#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 26 12:51:31 2021

@author: user
"""

# Program by Leah.

# Plots time dependence of concentrations averaged over cell-types from a series of pickled data structures (one for each time point).

import sys
sys.path.append('../')


import _pickle as pickle

from numpy import *
from pylab import *

import matplotlib
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
import matplotlib.pyplot as plt

from vroot.db_utilities import *
from openalea.celltissue import TissueDB



#db,=read('../Tissues/mature_root_section_radial.zip')

#PATH='output/Notsat_Pinf002_cwdiff_Ppass00222pK64_apopH5'
#PATH='output/Notsat_Pinf002_GA3params_Ppass00222pK4'
#PATH='output/mature_root_section_radial/GAtransport/GANoVacuole'
def Plot_Dynamic(db,N,dt,PATH,T,C):
    PATH=PATH
    cell_type = db.get_property('cell_type')
    
    #N=3
    #dt=1800
########################################################################################################
# Iterations: 
#########################################################################################################
    #time=[]
    #time[1:100+1]=T*arange(0,float(0.001)/3600*(100+0.5),float(0.001)/3600)
    #time[102:101+99]=T*arange(float(dt)*2/3600,float(dt)/3600*(100+0.5),float(dt)/3600)
    time=T*arange(0,float(dt)/3600*(N+0.5),float(dt)/3600)

    GA_epi=[]
    GA_cor=[]
    GA_endo=[]
    GA_peri=[]
    GA_ste=[]
    GA_vac_epi=[]
    GA_vac_cor=[]
    GA_vac_endo=[]
    GA_vac_peri=[]
    GA_vac_ste=[]
    
    ##########################################################################
    #fname='output/mature_root_section_radial/GAtransport_ss/GANoPlassteady/GAiter_00001.pickle'
    fname='output/mature_root_section_radial/GAtransport_ss/GAVacuole09NoPlasAllTransportersSyn0001Deg0000003/GAiter_00001.pickle'
    fdata=open(fname,'rb')
    GA=pickle.load(fdata)
    fdata.close()
    #fname='output/mature_root_section_radial/GAtransport_ss/GANoPlassteady/GA_vaciter_00001.pickle'
    fname='output/mature_root_section_radial/GAtransport_ss/GAVacuole09NoPlasAllTransportersSyn0001Deg0000003/GA_vaciter_00001.pickle'
    fdata=open(fname,'rb')
    GA_vac=pickle.load(fdata)
    fdata.close()
    GA_epi_list=[]
    GA_cor_list=[]
    GA_endo_list=[]
    GA_peri_list=[]
    GA_ste_list=[]
    GA_vac_epi_list=[]
    GA_vac_cor_list=[]
    GA_vac_endo_list=[]
    GA_vac_peri_list=[]
    GA_vac_ste_list=[]
    for cid in cell_type.keys():
        if cell_type[cid]==0:
            GA_epi_list.append(GA[cid])
            GA_vac_epi_list.append(GA_vac[cid])
        if cell_type[cid]==1:
            GA_cor_list.append(GA[cid])
            GA_vac_cor_list.append(GA_vac[cid])
        if cell_type[cid]==2:
            GA_endo_list.append(GA[cid])
            GA_vac_endo_list.append(GA_vac[cid])
        if cell_type[cid]==3:
            GA_peri_list.append(GA[cid])
            GA_vac_peri_list.append(GA_vac[cid])
        if cell_type[cid]==4:
            GA_ste_list.append(GA[cid])
            GA_vac_ste_list.append(GA_vac[cid])
    GA_epi.append(C*average(GA_epi_list))
    GA_cor.append(C*average(GA_cor_list))
    GA_endo.append(C*average(GA_endo_list))
    GA_peri.append(C*average(GA_peri_list))
    GA_ste.append(C*average(GA_ste_list))
    GA_vac_epi.append(C*average(GA_vac_epi_list))
    GA_vac_cor.append(C*average(GA_vac_cor_list))
    GA_vac_endo.append(C*average(GA_vac_endo_list))
    GA_vac_peri.append(C*average(GA_vac_peri_list))
    GA_vac_ste.append(C*average(GA_vac_ste_list))
    ##########################################################################

#    for iteration in range(1,100+1):
#        fname='output/Geometry_steady/GAtransport/GAVac09NoPlasNoNPF214SteadySyn00001Deg1/GAiter_%05d.pickle'%(iteration)
#        fdata=open(fname,'rb')
#        GA=pickle.load(fdata)
#        fdata.close()
#        fname='output/Geometry_steady/GAtransport/GAVac09NoPlasNoNPF214SteadySyn00001Deg1/GA_vaciter_%05d.pickle'%(iteration)
#        fdata=open(fname,'rb')
#        GA_vac=pickle.load(fdata)
#        fdata.close()
#        GA_epi_list=[]
#        GA_cor_list=[]
#        GA_endo_list=[]
#        GA_peri_list=[]
#        GA_ste_list=[]
#        GA_vac_epi_list=[]
#        GA_vac_cor_list=[]
#        GA_vac_endo_list=[]
#        GA_vac_peri_list=[]
#        GA_vac_ste_list=[]
#        for cid in cell_type.keys():
#            if cell_type[cid]==0:
#                GA_epi_list.append(GA[cid])
#                GA_vac_epi_list.append(GA_vac[cid])
#            if cell_type[cid]==1:
#                GA_cor_list.append(GA[cid])
#                GA_vac_cor_list.append(GA_vac[cid])
#            if cell_type[cid]==2:
#                GA_endo_list.append(GA[cid])
#                GA_vac_endo_list.append(GA_vac[cid])
#            if cell_type[cid]==3:
#                GA_peri_list.append(GA[cid])
#                GA_vac_peri_list.append(GA_vac[cid])
#            if cell_type[cid]==4:
#                GA_ste_list.append(GA[cid])
#                GA_vac_ste_list.append(GA_vac[cid])
#        GA_epi.append(C*average(GA_epi_list))
#        GA_cor.append(C*average(GA_cor_list))
#        GA_endo.append(C*average(GA_endo_list))
#        GA_peri.append(C*average(GA_peri_list))
#        GA_ste.append(C*average(GA_ste_list))
#        GA_vac_epi.append(C*average(GA_vac_epi_list))
#        GA_vac_cor.append(C*average(GA_vac_cor_list))
#        GA_vac_endo.append(C*average(GA_vac_endo_list))
#        GA_vac_peri.append(C*average(GA_vac_peri_list))
#        GA_vac_ste.append(C*average(GA_vac_ste_list))
    
    for iteration in range(1,N+1):
        fname='%s/GAiter_%05d.pickle'%(PATH,iteration)
        fdata=open(fname,'rb')
        GA=pickle.load(fdata)
        fdata.close()
        fname='%s/GA_vaciter_%05d.pickle'%(PATH,iteration)
        fdata=open(fname,'rb')
        GA_vac=pickle.load(fdata)
        fdata.close()
        GA_epi_list=[]
        GA_cor_list=[]
        GA_endo_list=[]
        GA_peri_list=[]
        GA_ste_list=[]
        GA_vac_epi_list=[]
        GA_vac_cor_list=[]
        GA_vac_endo_list=[]
        GA_vac_peri_list=[]
        GA_vac_ste_list=[]
        for cid in cell_type.keys():
            if cell_type[cid]==0:
                GA_epi_list.append(GA[cid])
                GA_vac_epi_list.append(GA_vac[cid])
            if cell_type[cid]==1:
                GA_cor_list.append(GA[cid])
                GA_vac_cor_list.append(GA_vac[cid])
            if cell_type[cid]==2:
                GA_endo_list.append(GA[cid])
                GA_vac_endo_list.append(GA_vac[cid])
            if cell_type[cid]==3:
                GA_peri_list.append(GA[cid])
                GA_vac_peri_list.append(GA_vac[cid])
            if cell_type[cid]==4:
                GA_ste_list.append(GA[cid])
                GA_vac_ste_list.append(GA_vac[cid])
        GA_epi.append(C*average(GA_epi_list))
        GA_cor.append(C*average(GA_cor_list))
        GA_endo.append(C*average(GA_endo_list))
        GA_peri.append(C*average(GA_peri_list))
        GA_ste.append(C*average(GA_ste_list))
        GA_vac_epi.append(C*average(GA_vac_epi_list))
        GA_vac_cor.append(C*average(GA_vac_cor_list))
        GA_vac_endo.append(C*average(GA_vac_endo_list))
        GA_vac_peri.append(C*average(GA_vac_peri_list))
        GA_vac_ste.append(C*average(GA_vac_ste_list))

    sidemargin=0.1   # Give the margins around the tissue as a proportion of the figure (i.e. in [0,1])
    bottommargin=0.15

    fig=figure(figsize=(12,6))
    fig.subplots_adjust(bottom=bottommargin)
    fig.subplots_adjust(top=1-bottommargin)
    fig.subplots_adjust(left=sidemargin)
    fig.subplots_adjust(right=1-0.1*sidemargin)

    ax=fig.add_subplot(111)
    ax.plot(time,GA_epi,'b-',linewidth=2)
    ax.plot(time,GA_cor,'r-',linewidth=2)
    ax.plot(time,GA_endo,'g-',linewidth=2)
    ax.plot(time,GA_peri,'m-',linewidth=2)
    ax.plot(time,GA_ste,'c-',linewidth=2)
    labels = ax.get_xticklabels()
    setp(labels,  fontsize=16)
    labels = ax.get_yticklabels()
    setp(labels, fontsize=16)
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
    ax.legend(('Epidermis','Cortex','Endodermis','Pericycle','Stele'),prop={'size':20}, loc=4) 
    ax.set_xlabel('Time (hours)',fontsize=20)
    ax.set_ylabel('GA concentration',fontsize=20)
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')

    fname = '%s/TimeDependence_N%d_dt%d'%(PATH,N,dt)
    savefig(fname)
    close()
    
    fig=figure(figsize=(12,6))
    fig.subplots_adjust(bottom=bottommargin)
    fig.subplots_adjust(top=1-bottommargin)
    fig.subplots_adjust(left=sidemargin)
    fig.subplots_adjust(right=1-0.1*sidemargin)

    ax=fig.add_subplot(111)
    ax.plot(time,GA_vac_epi,'b-',linewidth=2)
    ax.plot(time,GA_vac_cor,'r-',linewidth=2)
    ax.plot(time,GA_vac_endo,'g-',linewidth=2)
    ax.plot(time,GA_vac_peri,'m-',linewidth=2)
    ax.plot(time,GA_vac_ste,'c-',linewidth=2)
    labels = ax.get_xticklabels()
    setp(labels,  fontsize=16)
    labels = ax.get_yticklabels()
    setp(labels, fontsize=16)
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
    ax.legend(('Epidermis','Cortex','Endodermis','Pericycle','Stele'),prop={'size':20}, loc=4) 
    ax.set_xlabel('Time (hours)',fontsize=20)
    ax.set_ylabel('GA concentration in vacuole',fontsize=20)
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')

    fname = '%s/TimeDependenceVacuole_N%d_dt%d'%(PATH,N,dt)
    savefig(fname)
    close()


