# Program by Leah.

# Plots time dependence of concentrations averaged over cell-types from a series of pickled data structures (one for each time point).

import sys
sys.path.append('../')


import cPickle as pickle

from numpy import *
from pylab import *

import matplotlib
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
import matplotlib.pyplot as plt

from celltissue_util import *



db,=read('../Tissues/mature_root_section_radial.zip')
cell_type = db.get_property('cell_type')

#PATH='output/Notsat_Pinf002_cwdiff_Ppass00222pK64_apopH5'
#PATH='output/Notsat_Pinf002_GA3params_Ppass00222pK4'
PATH='output/Notsat_Pinf002_cwdiff_Ppass111pK42_cortexalpha001beta001'



N=120
dt=1800
########################################################################################################
# Iterations: 
#########################################################################################################
time=arange(0,float(dt)/3600*N+float(dt)/3600,float(dt)/3600)

GA_epi=[]
GA_cor=[]
GA_endo=[]
GA_peri=[]
GA_ste=[]

for iteration in range(0,len(time)):
	fname='%s/GA_N%d_dt%d_%03d'%(PATH,N,dt,iteration)
	fdata=open(fname,'r')
	GA=pickle.load(fdata)
	fdata.close()
	GA_epi_list=[]
	GA_cor_list=[]
	GA_endo_list=[]
	GA_peri_list=[]
	GA_ste_list=[]
	for cid in cell_type.keys():
		if cell_type[cid]==0:
			GA_epi_list.append(GA[cid])
		if cell_type[cid]==1:
			GA_cor_list.append(GA[cid])
		if cell_type[cid]==2:
			GA_endo_list.append(GA[cid])
		if cell_type[cid]==3:
			GA_peri_list.append(GA[cid])
		if cell_type[cid]==4:
			GA_ste_list.append(GA[cid])
	GA_epi.append(average(GA_epi_list))
	GA_cor.append(average(GA_cor_list))
	GA_endo.append(average(GA_endo_list))
	GA_peri.append(average(GA_peri_list))
	GA_ste.append(average(GA_ste_list))

sidemargin=0.1   # Give the margins around the tissue as a proportion of the figure (i.e. in [0,1])
bottommargin=0.15

fig=figure(figsize=(12,6))
fig.subplots_adjust(bottom=bottommargin)
fig.subplots_adjust(top=1-bottommargin)
fig.subplots_adjust(left=sidemargin)
fig.subplots_adjust(right=1-0.1*sidemargin)

ax=fig.add_subplot(111)
ax.plot(time,GA_epi,'b-',linewidth=2)
ax.plot(time,GA_cor,'r-',linewidth=2)
ax.plot(time,GA_endo,'g-',linewidth=2)
ax.plot(time,GA_peri,'m-',linewidth=2)
ax.plot(time,GA_ste,'c-',linewidth=2)
labels = ax.get_xticklabels()
setp(labels,  fontsize=16)
labels = ax.get_yticklabels()
setp(labels, fontsize=16)
box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
ax.legend(('Epidermis','Cortex','Endodermis','Pericycle','Stele'),prop={'size':20}, loc=4) 
ax.set_xlabel('Time (hours)',fontsize=20)
ax.set_ylabel('GA4 concentration',fontsize=20)
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.yaxis.set_ticks_position('left')
ax.xaxis.set_ticks_position('bottom')

fname = '%s/timedependence_N%d_dt%d'%(PATH,N,dt)
savefig(fname)
close()


