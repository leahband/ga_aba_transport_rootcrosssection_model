from vroot.import_tissue import import_tissue
from vroot.pylab_plot import plot_pylab_figure,plotCellIds

#open tissue and extract PIN and PIN2 dictionaries
db=import_tissue('Tissues/DIIV_mock.zip')
PIN=db.get_property('PIN')
PIN2=db.get_property('PIN2')

#make new empty dictionary
PIN2other={}
#iterate through existing PIN2
for eid,val in PIN2.items():
    #if PIN2 is present set value to 1
    if val==1.0:
        PIN2other[eid]=1
    else:
        #check if another PIN is present and if so set to 2
        if PIN[eid]>0.0:
            PIN2other[eid]=2
        #otherwise set to zero
        else:
            PIN2other[eid]=0
        
#add to tissue db
db.set_property('PIN2other',PIN2other)
db.set_description('PIN2other',' ')

#add to species_desc so it will plot
db.get_property('species_desc')['PIN2other']='edge'

#plot. last two arguments list 1) values to plot and what colour, and 2) Legend entries
plot_pylab_figure(db,'PIN2other',{'PIN2other':[0,2]},[1,1],False,{1:'g',2:'b'},['PIN2','other PIN'])

#plot cell ids
plotCellIds(db,'cellIds.png')


