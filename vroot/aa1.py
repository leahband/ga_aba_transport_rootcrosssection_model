#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 22 18:08:23 2022

@author: user
"""
import numpy as np
import scipy.sparse as sp
from scipy.integrate import solve_ivp
import matplotlib.pyplot as plt

def dq(t,C):
    return C

N = 10
t_span = [0, 20]
y0 = np.zeros(N)
y0[1::2] = 1
e1 = np.ones(N)
A = sp.spdiags([e1], [0], N, N, format="csc")

ans = solve_ivp(dq, t_span, y0,jac_sparsity=A)

plt.plot(np.arange(N), ans.y[:, 2])
plt.show()