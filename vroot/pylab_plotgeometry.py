#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 16 14:52:19 2022

@author: user
"""

### Nathan's most up to date pylab_plot.py file

#various plotting functions. 'plot_pylab_figure' is the most generalised.

import os, sys
import pickle
from openalea.tissueshape import centroid
from openalea.container import ordered_pids


from vroot.db_utilities import get_mesh,get_graph

from scipy.stats import sem
from numpy import *
from numpy import array,log2,mean
from pylab import *
from collections import defaultdict

import matplotlib
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection,PolyCollection,LineCollection
import matplotlib.pyplot as plt


from math import atan, cos,pi,sin,sqrt
from datetime import datetime
import time
a=datetime.now()

from openalea.ext.color import JetMap, GrayMap, GreenMap

import csv

def rescale_positions(db):
    
    db_position = db.get_property('position')
    db_cell_centres=db.get_property('cell_centres')
    #print(db_position._value[1])
    poslist=list(db_position.values())
    #poslist=[v[i] for i in range(len(v))]
    #poslist=[db_position._value[i] for i in range(len(db_position._value))]#original
    #poslist=list(db_position._value[1:len(db_position._value)][1])
    #print(dir(db_position))
    xvals,yvals = [[z[i] for z in poslist] for i in (0,1)]
    norm=max(max(xvals)-min(xvals),max(yvals)-min(yvals))
    position={}
    for pid,pos in db_position.items():
        position[pid]=((pos[0]-min(xvals))/norm,(pos[1]-min(yvals))/norm)
    #for i in range(len(db_position._value)):#original
    #for i in range(len(v)):
        #position[i]=((poslist[i][0]-min(xvals))/norm,(poslist[i][1]-min(yvals))/norm)#original
    cell_centres={}
    for cid,cc in db_cell_centres.items():
        cell_centres[cid]=((cc[0]-min(xvals))/norm,(cc[1]-min(yvals))/norm)
    return position,cell_centres,norm

def get_walls(mesh,position,rotate):
    
    wall_coordinates={}
    
    max_x=max([p[0] for p in position.values()])
        
    for wid in mesh.wisps(1):
        wall_coordinates[wid]=[]
        for pid in mesh.borders(1,wid):
            if rotate==True:
                wall_coordinates[wid].append([position[int(pid)][1],max_x-position[int(pid)][0]])
            else:
                wall_coordinates[wid].append([position[int(pid)][0],position[int(pid)][1]])
                   
    return [array([wall_coordinates[wid][0],wall_coordinates[wid][1]]) for wid in mesh.wisps(1)]
    
def get_cell_polygons(mesh,position,rotate):
    
    cell_coordinates={}
    
    max_x=max([p[0] for p in position.values()])

    for cid in mesh.wisps(2):
        cell_coordinates[cid]=[]
        for pid in ordered_pids(mesh,cid):
            if rotate==True:
                cell_coordinates[cid].append([position[int(pid)][1],max_x-position[int(pid)][0]])
            else:
                cell_coordinates[cid].append([position[int(pid)][0],position[int(pid)][1]])
                   
    return [Polygon(cell_coordinates[cid], True) for cid in mesh.wisps(2)]

def get_edges(mesh,position,offset_vectors,wall,graph,rotate):
    max_x=max([p[0] for p in position.values()])
    edge_coordinates={}
    for eid in graph.edges():
        cid=graph.source(eid)
        wid=wall[eid]
        new_coords=[array(position[pid])+0.001*offset_vectors[cid][pid][:2] for pid in list(mesh.borders(1,wid))]
        if rotate==True:
            edge_coordinates[eid]=[array(coord[1],max_x-coord[0]) for coord in new_coords]
        else:
            edge_coordinates[eid]=new_coords
    return [array([edge_coordinates[eid][0],edge_coordinates[eid][1]]) for eid in graph.edges()]
    
def get_edge_polygons(mesh,position,offset_vectors,wall,graph,rotate):
    offset=0.001
    max_x=max([p[0] for p in position.values()]) 
    edge_coordinates={}
    for eid in graph.edges():
        cid=graph.source(eid)
        wid=wall[eid]
        pids=list(mesh.borders(1,wid))
        new_coords=[array(position[pid]) for pid in pids]
        
        new_coords.append(array(position[pids[1]])+3*offset*offset_vectors[cid][pids[1]][:2])
        new_coords.append(array(position[pids[0]])+3*offset*offset_vectors[cid][pids[0]][:2])
        new_coords=[list(val) for val in new_coords]
        if rotate==True:
            edge_coordinates[eid]=[[coord[1],max_x-coord[0]] for coord in new_coords]
        else:
            edge_coordinates[eid]=new_coords
    return [Polygon(edge_coordinates[eid],True) for eid in graph.edges()]
    
    
def plot_pylab_figure(db,savename,prop_list,subplot_dims,rotate=False,clegend=None,plabels=None):
    #rotate=False
    #check what is on list and if we can plot it
    species_desc=db.get_property('species_desc')
    plottypes=[]
    for prop in prop_list.keys():
        if prop in species_desc:
            plottypes.append(species_desc[prop].lower())
        else:
            print(prop+" not in species_desc - can't plot")
            return
            
    #get coordinates
    position,cell_centres,norm = rescale_positions(db)
    mesh=get_mesh(db)
    graph=get_graph(db)
    if 'cell' in plottypes:
        cell_polygons=get_cell_polygons(mesh,position,rotate)
    if 'wall' in plottypes:
        walls=get_walls(mesh,position,rotate)
    if 'edge' in plottypes:
        edges=get_edges(mesh,position,db.get_property('offset_vectors'),db.get_property('wall'),graph,rotate)
        edge_polys=get_edge_polygons(mesh,position,db.get_property('offset_vectors'),db.get_property('wall'),graph,rotate)

    # dimension and subplots
    max_x=max([p[0] for p in position.values()])
    max_y=max([p[1] for p in position.values()])

    rowplots=subplot_dims[0]
    colplots=subplot_dims[1]
    
    if rotate==False:
        figysize=5
        figxsize=16#int(round((figysize/(max_y*colplots))*(max_x*rowplots)))
    else:
        figxsize=5
        figysize=5#int(round((figxsize/(max_y*colplots))*(max_x*rowplots)))

    fig=figure(figsize=(figxsize,figysize),dpi=160)
    subplots_adjust(left=0.025, bottom=0.025, right=0.975, top=0.975,wspace=0.025, hspace=0.025)

    lwidth=1

    #fig.patch.set_facecolor('grey')


    #make plots
    for i,prop in enumerate(prop_list):
        crange=prop_list[prop]
        cprop=db.get_property(prop)
        
        ax=fig.add_subplot(rowplots,colplots,i+1,aspect='equal')
        ax.set_xticks([])
        ax.set_yticks([])
        #ax.set_title(prop,{'fontsize': 20},'left')
        box(on=None)
        if rotate==False:
            xlim(0,max_x)
            ylim(0,max_y)
        else:
            xlim(0,max_y)
            ylim(0,max_x)
            
        if plottypes[i]=='cell':
            BDL=10.0
            for cid,val in cprop.items():
                if val<0:
                    cprop[cid]=BDL
            Ax_colors = [cprop[cid] for cid in mesh.wisps(2)]
            use_polygons = [cell_polygons[j] for j,val in enumerate(Ax_colors) if val>=0.0]
            #Ax_colors = [cprop[cid] for cid in mesh.wisps(2) if cprop[cid]>=0.0]
            
            p = PatchCollection(use_polygons, cmap=matplotlib.cm.jet,linewidths=(lwidth,),edgecolor=('black'))
        if plottypes[i]=='wall':
            Ax_colors = [cprop[cid] for cid in mesh.wisps(1)]
            p = LineCollection(walls, cmap=matplotlib.cm.jet,linewidths=(lwidth,))

        if plottypes[i]=='edge':
            max_x=max([p[0] for p in position.values()]) 
            for wid in mesh.wisps(1):
                coords=[position[pid] for pid in mesh.borders(1,wid)]
                if rotate==True:
                    gca().add_line(Line2D((coords[0][1],coords[1][1]),(max_x-coords[0][0],max_x-coords[1][0]), lw=1.5,color='gray'))
                else:
                    gca().add_line(Line2D((coords[0][0],coords[1][0]),(coords[0][1],coords[1][1]), lw=1.5,color='gray'))
            
            Ax_colors = [cprop[cid] for cid in graph.edges()]
            
            lpatches=[]

            
            for xval,col in clegend.items():
                use_polygons=[]
                for j,val in enumerate(Ax_colors):
                    if val==xval:
                        use_polygons.append(edge_polys[j])
                p = PatchCollection(use_polygons,linewidths=(0.5,),label=col)
                p.set_color(col)
                ax.add_collection(p)
                lpatches.append(matplotlib.patches.Patch(color=col, label=plabels[xval-1]))
            #plt.legend(handles=lpatches,loc=[0.71,0.0],fontsize='x-large')
            
        if plottypes[i]!='edge':
            p.set_array(array(Ax_colors))
            p.set_clim(crange[0],crange[1])
            #p.set_clim(crange[0],2.8)
            ax.add_collection(p)
            cb=colorbar(p,shrink=0.2,use_gridspec=False,anchor=(0.0,0.0))
            #cb=colorbar(p,shrink=0.8)
            #cb.set_ticks([crange[0],2.8/2,2.8])
            if crange[1]>=2.0:
                cb.set_ticks([crange[0],floor(crange[1])/2.0,floor(crange[1])])
            else:
                cb.set_ticks([crange[0],round(crange[1]/2.0,1),floor(crange[1]*10)/10.0])
            for t in cb.ax.get_yticklabels():
                 t.set_fontsize(20)
    
    #for cid,pos in cell_centres.items():
    #    ax.text(pos[0],pos[1],str(cid), horizontalalignment='center', verticalalignment='center',fontsize=6)
        
    #save and close
    savefig(savename,facecolor=fig.get_facecolor())
    clf()
    close(fig)

def plotsummarystats(db,fname,propnames,ylim=None):
    pos=db.get_property('position')
    centroid=db.get_property('cell_centres')
    mesh=get_mesh(db)
    border_type=db.get_property('border')
    cell_type=db.get_property('cell_type')
    def sort_pair(a, b):
        if a<=b:
                return (a,b)
        else:
                return (b,a)
                
    from collections import defaultdict
    wall_groups = defaultdict(list)
    for wid in mesh.wisps(1):
            if mesh.nb_regions(1, wid) == 2:
                    cid1, cid2 = mesh.regions(1, wid)
                    wall_groups[sort_pair(cid1, cid2)].append(wid)


    wall_groups_centre= {}
    for (cid1, cid2), wl in wall_groups.items():
        pid_set = set()
        for wid in wl:
            pid_set = pid_set ^ set(mesh.borders(1, wid))
        pid0, pid1 = pid_set
        p0 = array(pos[pid0])
        p1 = array(pos[pid1])
        wall_groups_centre[(cid1,cid2)]=0.5*p0+0.5*p1
    
    
    QC = 17
    QC_cells = [cid for cid, v in cell_type.items() if v == QC ]

    # Find QC position

    if len(QC_cells) == 1:
            QC_pos = centroid[QC_cells[0]]
            x_distalmeri = QC_pos[0]-40
            ycentreline = QC_pos[1]
    elif len(QC_cells) == 2: 
            QC_pos = wall_groups_centre[sort_pair(*QC_cells)]
            x_distalmeri = QC_pos[0]-40
            ycentreline = QC_pos[1]
            
    elif len(QC_cells) == 3: #should we ever have 3 QC cells? anyway this should pick the central one
        medy=median([centroid[cid][1] for cid in QC_cells])
        QC_pos=[centroid[cid] for cid in QC_cells if centroid[cid][1]==medy][0]
        x_distalmeri = QC_pos[0]-40
        ycentreline = QC_pos[1]
    
    
    most_shootward_upper = {}

    most_shootward_lower = {}
    for ct in (6, 7, 8, 9, 19):
            for cid in cell_type:
                    if cell_type[cid] == ct:
                            c = centroid[cid]
                            if c[1] < ycentreline and (ct not in most_shootward_lower or c[0] < centroid[most_shootward_lower[ct]][0]):
                                    most_shootward_lower[ct] = cid
                            if c[1] > ycentreline and (ct not in most_shootward_upper or c[0] < centroid[most_shootward_upper[ct]][0]):
                                    most_shootward_upper[ct] = cid
    
    xEZ=max([min([centroid[cid][0] for cid in most_shootward_upper.values()]),min([centroid[cid][0] for cid in most_shootward_lower.values()])])

    all_avs=[]
    all_errs=[]

    for propname in propnames:
        prop=db.get_property(propname)
        prop_epi=[]
        prop_epi_meri=[]
        prop_epi_EZ=[]
        prop_cor_meri=[]
        prop_cor_EZ=[]
        prop_cor=[]
        prop_endo=[]
        prop_vasc=[]
        prop_LRC=[]
        prop_colQC=[]
        prop_col=[]
        prop_initials=[]
        prop_QC=[]
        
        for cid in cell_type.keys():
            if cid not in border_type.keys() and prop[cid]>=0.0:
                    if cell_type[cid]==2:
                        prop_epi.append(prop[cid])
                        if centroid[cid][0]>xEZ:
                            prop_epi_meri.append(prop[cid])
                        else:
                            prop_epi_EZ.append(prop[cid])
                    if cell_type[cid]==4:
                        prop_cor.append(prop[cid])
                        if centroid[cid][0]>xEZ:
                            prop_cor_meri.append(prop[cid])
                        else:
                            prop_cor_EZ.append(prop[cid])
                    if cell_type[cid]==3:
                        prop_endo.append(prop[cid])
                    if cell_type[cid]==5:
                        prop_vasc.append(prop[cid])
                    if cell_type[cid]==6 or cell_type[cid]==7 or cell_type[cid]==8 or cell_type[cid]==9 or cell_type[cid]==19:
                        prop_LRC.append(prop[cid])
                    if cell_type[cid]==10:
                        prop_initials.append(prop[cid])
                    if cell_type[cid]==11 or cell_type[cid]==12 or cell_type[cid]==13 or cell_type[cid]==14 or cell_type[cid]==15:
                        prop_col.append(prop[cid])
                    if cell_type[cid]==17:
                        prop_QC.append(prop[cid])
                    if cell_type[cid]==17 or cell_type[cid]==10 or cell_type[cid]==11 or cell_type[cid]==12 or cell_type[cid]==13 or cell_type[cid]==14  or cell_type[cid]==15:                           
                        prop_colQC.append(prop[cid])

        yaverage=[average(prop_epi_meri),  average(prop_epi_EZ), average(prop_cor_meri), average(prop_cor_EZ),  average(prop_endo),\
                    average(prop_vasc), average(prop_LRC), average(prop_col),average(prop_initials),average(prop_QC)]
        yerror=[sem(prop_epi_meri),  sem(prop_epi_EZ), sem(prop_cor_meri),sem(prop_cor_EZ),  sem(prop_endo), sem(prop_vasc), sem(prop_LRC), sem(prop_col),sem(prop_initials),sem(prop_QC)]
        all_avs.append(yaverage)
        all_errs.append(yerror)
        
        
        N=len(yaverage)
        ylimmax=1.1*max(yaverage)
        fig=figure(figsize=(10,10),dpi=100) 
        ax=fig.add_subplot(1,1,1)
        ax.bar(range(N),yaverage,yerr=yerror,ecolor='black',facecolor='#777777',align='center')
        ax.set_ylabel(propname,size='30')
        ax.set_xticks(range(N))
        if ylim !=None:
            ax.set_ylim((0,ylim))
        else:
            ax.set_ylim((0,ylimmax))
            
        ax.set_xlim((-0.7,9.7))        
        group_labels=['Epi M','Epi EZ','Cor M','Cor EZ','End','Ste','LRC','Col','Init','QC']
        ax.set_xticklabels(group_labels,rotation=45,size='20')
        labels = ax.get_yticklabels()
        setp(labels, fontsize=30)
        savefig(fname+'_'+propname,dpi=100)
        clf()
        close(fig)


        
        
        spreadsheet = csv.writer(open(fname+'_'+propname+'.csv', 'wb'), delimiter=',',quotechar='|', quoting=csv.QUOTE_MINIMAL)
        spreadsheet.writerow(['cell type','mean','s.e.m.'])
        for i in range(N):
            spreadsheet.writerow([group_labels[i],yaverage[i],yerror[i]])

    N=len(all_avs[0])
    ylimmax=1.1*max(all_avs[0]+all_avs[1])
    fig=figure(figsize=(12,12),dpi=100) 
    ax=fig.add_subplot(1,1,1)
    bwidth=0.35
    ax.bar(arange(N)-bwidth/2,all_avs[0],bwidth,yerr=all_errs[0],color='SkyBlue',label='data')
    ax.bar(arange(N)+bwidth/2,all_avs[1],bwidth,yerr=all_errs[1],color='Green',label='model')
    ax.set_ylabel(propnames[1],size='30')
    ax.set_xticks(range(N))
    if ylim !=None:
        ax.set_ylim((0,ylim))
    else:
        ax.set_ylim((0,ylimmax))
        
    ax.set_xlim((-0.7,9.7))        
    group_labels=['Epi M','Epi EZ','Cor M','Cor EZ','End','Ste','LRC','Col','Init','QC']
    ax.set_xticklabels(group_labels,rotation=45,size='20')
    labels = ax.get_yticklabels()
    setp(labels, fontsize=30)
    ax.legend()
    plt.setp(plt.gca().get_legend().get_texts(), fontsize='30')
    savefig(fname+'_'+propnames[0]+'vs'+propnames[1],dpi=100)
    clf()
    close(fig)


def plotScatterByCentroidCellType(db,fname,propname,celltypes,key):
    centroid = db.get_property('cell_centres')
    cell_type = db.get_property('cell_type')
    prop = db.get_property(propname)
    print(set([val for val in cell_type.values()]))
    V = db.get_property('V')
    fig=figure(figsize=(10,10),dpi=100) 
    ax=fig.add_subplot(2,1,1,aspect='equal')
    colors='rgbkmcyy'
    for i,ctype in enumerate(celltypes):
        xcids=[cid for cid,val in cell_type.items() if val==ctype]
        #plt.scatter([centroid[cid][0] for cid in xcids],[log2(prop[cid]) for cid in xcids] , s=[V[cid] for cid in xcids], c=colors[i], alpha=0.5)
        plt.scatter([centroid[cid][0] for cid in xcids],[centroid[cid][1] for cid in xcids] , s=[V[cid] for cid in xcids], c=colors[i], alpha=0.5)
    plt.ylabel('y coordinate')
    ax=fig.add_subplot(2,1,2)

    for i,ctype in enumerate(celltypes):
        xcids=[cid for cid,val in cell_type.items() if val==ctype]
        plt.scatter([centroid[cid][0] for cid in xcids],[log2(prop[cid]) for cid in xcids] , s=[V[cid] for cid in xcids], c=colors[i], alpha=0.5)
        #plt.scatter([centroid[cid][0] for cid in xcids],[centroid[cid][1] for cid in xcids] , s=[V[cid] for cid in xcids], c=colors[i], alpha=0.5)
        
    plt.xlabel('x coordinate')
    plt.ylabel('%s (log2)' % propname)
    
    plt.legend(key)
    savefig(fname)
    
def getWallGroups(mesh):

    wall_groups = defaultdict(list)

    for wid in mesh.wisps(1):
        cids=list(mesh.regions(1,wid))
        wall_groups[(min(cids), max(cids))].append(wid)

    return wall_groups
    
def plotGAFlux(db,fname,J,scale,threshold,colour,rotate=False):

    mesh=get_mesh(db)

    position,cell_centres,norm=rescale_positions(db)
    
    #also rescale the scale(!)
    scale=scale/norm
    
    wall_groups=getWallGroups(mesh)

    xmin=min([val[0] for val in position.values()])
    xmax=max([val[0] for val in position.values()])
    ymin=min([val[1] for val in position.values()])
    ymax=max([val[1] for val in position.values()])
    
    if rotate==False:
        fig=figure(figsize=(16,5),dpi=160)
    else:
        fig=figure(figsize=(5,16),dpi=160)
    subplots_adjust(left=0.025, bottom=0.025, right=0.975, top=0.975,wspace=0.025, hspace=0.025)
    ax=fig.add_subplot(111,aspect='equal')
    ax.set_yticks([])
    ax.set_xticks([])
    box(on=None)
    if rotate==False:
        xlim(0,xmax )    # set the xlim to xmin, xmax
        ylim(0, ymax )
    else:
        ylim(0,xmax )    # set the xlim to xmin, xmax
        xlim(0, ymax )

    # Plot root outline:
    if rotate==False:
        for wid in mesh.wisps(1):
            coords=[position[pid] for pid in mesh.borders(1,wid)]
            gca().add_line(Line2D((coords[0][0],coords[1][0]),(coords[0][1],coords[1][1]), lw=1.0,color='gray'))
    else:
        for wid in mesh.wisps(1):
            coords=[position[pid] for pid in mesh.borders(1,wid)]
            gca().add_line(Line2D((coords[0][1],coords[1][1]),(xmax-coords[0][0],xmax-coords[1][0]), lw=1.0,color='gray'))
    S=db.get_property('S')
    alength=4/norm
    

    
    
    for (cid1, cid2), wl in wall_groups.items():
        J_tot = 0.0
        S_tot=0.0
        pid_set = set()
        for wid in wl:
            J_tot += J[wid]
            S_tot+=S[wid]
            pid_set = pid_set ^ set(mesh.borders(1, wid))
        J_tot=J_tot/S_tot
        pid_set=list(pid_set)
        p0 = position[pid_set[0]]
        p1 = position[pid_set[1]]

        wc = (0.5*(p0[0]+p1[0]), 0.5*(p0[1]+p1[1]))
        if p1[1]-p0[1]>0 or p1[1]-p0[1]<0:
            alpha=arctan(-(p1[0]-p0[0])/(p1[1]-p0[1]))
            xl=wc[0]-alength*cos(alpha)
            xu=wc[0]+alength*cos(alpha)
            yl=wc[1]-alength*sin(alpha)
            yu=wc[1]+alength*sin(alpha)
        if p1[1]-p0[1]==0:
            xl=wc[0]
            xu=wc[0]
            yl=wc[1]-alength
            yu=wc[1]+alength


        arrow_exists=False
    # Only plot fluxes of magnitude greater than a threshold
        if J_tot>threshold:
            arrow_exists=True
            if sqrt(pow(xl-cell_centres[cid1][0],2)+pow(yl-cell_centres[cid1][1],2))<sqrt(pow(xu-cell_centres[cid1][0],2)+pow(yu-cell_centres[cid1][1],2)):
                x0=xl
                y0=yl
                dx=xu-xl
                dy=yu-yl
            else:
                x0=xu
                y0=yu
                dx=xl-xu
                dy=yl-yu


        if J_tot<-threshold:
            arrow_exists=True
            if sqrt(pow(xl-cell_centres[cid1][0],2)+pow(yl-cell_centres[cid1][1],2))<sqrt(pow(xu-cell_centres[cid1][0],2)+pow(yu-cell_centres[cid1][1],2)):
                x0=xu
                y0=yu
                dx=xl-xu
                dy=yl-yu

            else:
                x0=xl
                y0=yl
                dx=xu-xl
                dy=yu-yl

        if arrow_exists:
            if rotate:
                arr = arrow(y0, xmax-x0, dy, -dx,width=sqrt(abs(J_tot))*scale, linewidth=0.5)
            else:
                arr = arrow(x0, y0, dx, dy,width=sqrt(abs(J_tot))*scale, linewidth=0.5)
                
            gca().add_patch(arr)
            arr.set_edgecolor('k')
            arr.set_facecolor(colour)


    gca().add_line(Line2D((xmax-60,xmax-10),(ymin+20,ymin+20), color='k', lw=5))

    arr = arrow(0.04*xmax, 0.13*ymax, -alength,0,width=sqrt(50)*scale, linewidth=0.5)
    gca().add_patch(arr)
    arr.set_edgecolor('k')
    arr.set_facecolor(colour)
    figtext(0.06*xmax, 0.1*ymax, 'J=50.0',size='30',rotation='horizontal')

    savefig(fname)
    close()


def plot_relative_property(db1,db2,savename,prop):
    rotate=True
    from numpy import log2
    #check what is on list and if we can plot it
    species_desc=db1.get_property('species_desc')


    if prop in species_desc:
        plottype=species_desc[prop].lower()
    else:
        print(prop+" not in species_desc - can't plot")
        return
            
    #get coordinates
    position,cell_centres,norm = rescale_positions(db1)
    mesh=get_mesh(db1)
    graph=get_graph(db1)
    if plottype=='cell':
        cell_polygons=get_cell_polygons(mesh,position,rotate)
    if plottype=='wall':
        walls=get_walls(mesh,position)
    if plottype=='edge':
        edges=get_edges(mesh,position,db1.get_property('offset_vectors'),db1.get_property('wall'),graph)


    # dimension and subplots
    # dimension and subplots
    max_x=max([p[0] for p in position.values()])
    max_y=max([p[1] for p in position.values()])

    if rotate==False:
        figysize=5
        figxsize=16#int(round((figysize/(max_y*colplots))*(max_x*rowplots)))
    else:
        figxsize=5
        figysize=16#int(round((figxsize/(max_y*colplots))*(max_x*rowplots)))

        
    fig=figure(figsize=(figxsize,figysize),dpi=160)
    subplots_adjust(left=0.025, bottom=0.025, right=0.975, top=0.975,wspace=0.025, hspace=0.025)

    lwidth=1



    #make plots


    cprop=db1.get_property(prop)
    cprop2=db2.get_property(prop)
    
    ax=fig.add_subplot(1,1,1,aspect='equal')
    ax.set_xticks([])
    ax.set_yticks([])
    #ax.set_title(prop,{'fontsize': 20},'left')
    box(on=None)
    
    if rotate==False:
        xlim(0,max_x)
        ylim(0,max_y)
    else:
        xlim(0,max_y)
        ylim(0,max_x)
    
    if plottype=='cell':
        Ax_colors=[]
        for cid in mesh.wisps(2):
            if cprop[cid]>0 and cprop2[cid]>0:
                Ax_colors.append(log2(cprop2[cid]/cprop[cid]))
                #Ax_colors.append(cprop2[cid]-cprop[cid])
            else:
                Ax_colors.append(0)
                #Ax_colors.append(cprop2[cid]-cprop[cid])
                
        #Ax_colors = [log2(cprop2[cid]/cprop[cid]) for cid in mesh.wisps(2)]
        p = PatchCollection(cell_polygons, cmap=matplotlib.cm.seismic,linewidths=(lwidth,),edgecolor=('black'))
    if plottype=='wall':
        Ax_colors = [log2(cprop2[cid]/cprop[cid]) for cid in mesh.wisps(1)]
        p = LineCollection(walls, cmap=matplotlib.cm.jet,linewidths=(lwidth,))
    if plottype=='edge':
        Ax_colors = [log2(cprop2[cid]/cprop[cid]) for cid in graph.edges()]
        p = LineCollection(edges, cmap=matplotlib.cm.jet,linewidths=(lwidth,))
        
    p.set_array(array(Ax_colors))
    
    crange=max([abs(min(Ax_colors)),abs(max(Ax_colors))])
    #crange=12
    p.set_clim(-crange,crange)
    ax.add_collection(p)
    #cb=colorbar(p,shrink=0.8)
    cb=colorbar(p,shrink=0.2,use_gridspec=False,anchor=(0.0,0.0))
    for t in cb.ax.get_yticklabels():
         t.set_fontsize(20)
    
    #for cid,pos in cell_centres.items():
    #    ax.text(pos[0],pos[1],str(cid), horizontalalignment='center', verticalalignment='center',fontsize=6)
        
    #save and close
    savefig(savename,facecolor=fig.get_facecolor())
    clf()

    close(fig)

def plot_absolute_difference(db1,db2,savename,prop):
    rotate=True
    from numpy import log2
    #check what is on list and if we can plot it
    species_desc=db1.get_property('species_desc')


    if prop in species_desc:
        plottype=species_desc[prop].lower()
    else:
        print(prop+" not in species_desc - can't plot")
        return
            
    #get coordinates
    position,cell_centres,norm = rescale_positions(db1)
    mesh=get_mesh(db1)
    graph=get_graph(db1)
    if plottype=='cell':
        cell_polygons=get_cell_polygons(mesh,position,rotate)
    if plottype=='wall':
        walls=get_walls(mesh,position)
    if plottype=='edge':
        edges=get_edges(mesh,position,db1.get_property('offset_vectors'),db1.get_property('wall'),graph)


    # dimension and subplots
    # dimension and subplots
    max_x=max([p[0] for p in position.values()])
    max_y=max([p[1] for p in position.values()])

    if rotate==False:
        figysize=5
        figxsize=16#int(round((figysize/(max_y*colplots))*(max_x*rowplots)))
    else:
        figxsize=5
        figysize=16#int(round((figxsize/(max_y*colplots))*(max_x*rowplots)))

        
    fig=figure(figsize=(figxsize,figysize),dpi=160)
    subplots_adjust(left=0.025, bottom=0.025, right=0.975, top=0.975,wspace=0.025, hspace=0.025)

    lwidth=1



    #make plots


    cprop=db1.get_property(prop)
    cprop2=db2.get_property(prop)
    
    ax=fig.add_subplot(1,1,1,aspect='equal')
    ax.set_xticks([])
    ax.set_yticks([])
    #ax.set_title(prop,{'fontsize': 20},'left')
    box(on=None)
    
    if rotate==False:
        xlim(0,max_x)
        ylim(0,max_y)
    else:
        xlim(0,max_y)
        ylim(0,max_x)
    
    if plottype=='cell':
        Ax_colors=[]
        for cid in mesh.wisps(2):
            if cprop[cid]>0 and cprop2[cid]>0:
                #Ax_colors.append(log2(cprop2[cid]/cprop[cid]))
                Ax_colors.append(cprop2[cid]-cprop[cid])
            else:
                #Ax_colors.append(0)
                Ax_colors.append(cprop2[cid]-cprop[cid])
                
        #Ax_colors = [log2(cprop2[cid]/cprop[cid]) for cid in mesh.wisps(2)]
        p = PatchCollection(cell_polygons, cmap=matplotlib.cm.seismic,linewidths=(lwidth,),edgecolor=('black'))
    if plottype=='wall':
        Ax_colors = [log2(cprop2[cid]/cprop[cid]) for cid in mesh.wisps(1)]
        p = LineCollection(walls, cmap=matplotlib.cm.jet,linewidths=(lwidth,))
    if plottype=='edge':
        Ax_colors = [log2(cprop2[cid]/cprop[cid]) for cid in graph.edges()]
        p = LineCollection(edges, cmap=matplotlib.cm.jet,linewidths=(lwidth,))
        
    p.set_array(array(Ax_colors))
    
    crange=max([abs(min(Ax_colors)),abs(max(Ax_colors))])
    #crange=12
    p.set_clim(-crange,crange)
    ax.add_collection(p)
    #cb=colorbar(p,shrink=0.8)
    cb=colorbar(p,shrink=0.2,use_gridspec=False,anchor=(0.0,0.0))
    for t in cb.ax.get_yticklabels():
         t.set_fontsize(20)
    
    #for cid,pos in cell_centres.items():
    #    ax.text(pos[0],pos[1],str(cid), horizontalalignment='center', verticalalignment='center',fontsize=6)
        
    #save and close
    savefig(savename,facecolor=fig.get_facecolor())
    clf()

    close(fig)


def plot_data_vs_model(db,dataprop,modelprop,savename,rotate=False,scale=None):
    from numpy import log2
    #check what is on list and if we can plot it
    species_desc=db.get_property('species_desc')


    if dataprop in species_desc:
        dataplottype=species_desc[dataprop].lower()
    else:
        print(prop+" not in species_desc - can't plot")
        return
        
    if modelprop in species_desc:
        modelplottype=species_desc[modelprop].lower()
    else:
        print(prop+" not in species_desc - can't plot")
        return
    
    assert modelplottype==dataplottype
    
    #get coordinates
    position,cell_centres,norm = rescale_positions(db)
    mesh=get_mesh(db)
    graph=get_graph(db)
    if modelplottype=='cell':
        cell_polygons=get_cell_polygons(mesh,position,rotate)
    if modelplottype=='wall':
        walls=get_walls(mesh,position)
    if modelplottype=='edge':
        edges=get_edges(mesh,position,db.get_property('offset_vectors'),db.get_property('wall'),graph)


    # dimension and subplots
    max_x=max([p[0] for p in position.values()])
    max_y=max([p[1] for p in position.values()])

    if rotate==False:
        figysize=5
        figxsize=16#int(round((figysize/(max_y*colplots))*(max_x*rowplots)))
    else:
        figxsize=5
        figysize=16#int(round((figxsize/(max_y*colplots))*(max_x*rowplots)))



        
    fig=figure(figsize=(figxsize,figysize),dpi=160)
    subplots_adjust(left=0.025, bottom=0.025, right=0.975, top=0.975,wspace=0.025, hspace=0.025)

    lwidth=1

    #fig.patch.set_facecolor('grey')


    #make plots

    from numpy import mean
    mprop=db.get_property(modelprop)
    mmean=min(mprop.values())
    dprop=db.get_property(dataprop)
    dmean=min(dprop.values())
    
    ax=fig.add_subplot(1,1,1,aspect='equal')
    ax.set_xticks([])
    ax.set_yticks([])
    #ax.set_title('%s model - data' % modelprop,{'fontsize': 20},'left')
    box(on=None)
    
    if rotate==False:
        xlim(0,max_x)
        ylim(0,max_y)
    else:
        xlim(0,max_y)
        ylim(0,max_x)
    
    if modelplottype=='cell':
        Ax_colors=[]
        for cid in mesh.wisps(2):
            if mprop[cid]>0 and dprop[cid]>0:
                Ax_colors.append(mprop[cid]/mmean-dprop[cid]/dmean)
                #Ax_colors.append(log2((mprop[cid]/mmean)/(dprop[cid]/dmean)))
            else:
                Ax_colors.append(0)
    
        p = PatchCollection(cell_polygons, cmap=matplotlib.cm.seismic,linewidths=(lwidth,),edgecolor=('black'))

        
    p.set_array(array(Ax_colors))
    
    crange=max([abs(min(Ax_colors)),abs(max(Ax_colors))])
    if scale!=None:
        crange=scale
    p.set_clim(-crange,crange)
    ax.add_collection(p)
    #cb=colorbar(p,shrink=0.8)
    cb=colorbar(p,shrink=0.2,use_gridspec=False,anchor=(-0.5,0.0))
    for t in cb.ax.get_yticklabels():
         t.set_fontsize(20)
    

        
    #save and close
    savefig(savename,facecolor=fig.get_facecolor())
    clf()

    close(fig)

def plot_cross_section(value,propname,db,savename,dimension='x'):

    if dimension=='x':
        dim=0
        oppdim=1
    elif dimension=='y':
        dim=1
        oppdim=0
    else:
        print('using x dimension for cross-section')
        dim=0
        oppdim=1
        
    mesh=get_mesh(db)
    position=db.get_property('position')
    prop=db.get_property(propname)
    
    toplot_dict={}
    for cid in mesh.wisps(2):
        vals=[position[pid][dim] for pid in ordered_pids(mesh,cid)]
        if min(vals)<=value<=max(vals):
            oppvals=[position[pid][oppdim] for pid in ordered_pids(mesh,cid)]
            toplot_dict[mean(oppvals)]=prop[cid]
            
    plotx=sorted(toplot_dict)
    ploty=[toplot_dict[val] for val in sorted(toplot_dict)]
    
    plt.plot(plotx,ploty,'x-')
    savefig('%s__%s_%03f.png' % (savename,propname,value))
    clf()
    #close(fig)
    return plotx,ploty
    
def plot_cross_section_zone(zone,propname,db,savename):

    position=db.get_property('position')
    prop=db.get_property(propname)
    zones=db.get_property('zones')
    cell_centres=db.get_property('cell_centres')
    toplot_dict={}
    count=0
    for cid,val in zones.items():
        if val==zone:
            if cell_centres[cid][1] not in toplot_dict:
                toplot_dict[cell_centres[cid][1]]=prop[cid]
            else:
                count+=1
                print('duplicate '+str(count))
                toplot_dict[cell_centres[cid][1]+0.000001]=prop[cid]
            
    plotx=sorted(toplot_dict)
    ploty=[toplot_dict[val] for val in sorted(toplot_dict)]
    
    plt.plot(plotx,ploty,'x-')
    savefig('%s__%s_%01d.png' % (savename,propname,zone))
    clf()
    #close(fig)
    return plotx,ploty

from scipy.cluster.vq import kmeans,vq
def plotPropByCelltypeVsXposition(celltypes,propnames,db):
    
    
    fig=figure()
    
    cell_centres=db.get_property('cell_centres')
    cell_type=db.get_property('cell_type')
    prop=db.get_property(propnames[0])
    maxprop=max(list(prop.values()))
    
    prop2=db.get_property(propnames[1])
    maxprop2=max(list(prop2.values()))
    titles=['Epi','Cor','End','Per']
    for i in range(len(celltypes)):

        ax=fig.add_subplot(len(celltypes),1,i+1)
        
        cids=[cid for cid,val in cell_type.items() if val==celltypes[i]]
        propvals=array([prop[cid]/maxprop for cid,val in cell_type.items() if val==celltypes[i]])
        propvals2=array([prop2[cid]/maxprop2 for cid,val in cell_type.items() if val==celltypes[i]])
        xvals=array([cell_centres[cid][0] for cid,val in cell_type.items() if val==celltypes[i]])
        yvals=array([cell_centres[cid][1] for cid,val in cell_type.items() if val==celltypes[i]])
        
        centroids,something = kmeans(array(yvals),2)
        print(centroids)
        print(something)
        highval = list(centroids).index(max(list(centroids)))
        lowval = list(centroids).index(min(list(centroids)))
        idx,_ = vq(array(yvals),centroids)
        #print(highval,lowval,idx)
        plt.title(titles[i])
        plt.plot(xvals[idx==highval],propvals[idx==highval],'rx-')
        plt.plot(xvals[idx==lowval],propvals[idx==lowval],'bx-')
        
        plt.plot(xvals[idx==highval],propvals2[idx==highval],'md-')
        plt.plot(xvals[idx==lowval],propvals2[idx==lowval],'cd-')
        plt.legend(['data+','data-','model+','model-'])
    plt.show()
        

def plotPropByCelltypeVsXposition_vsData(db,savename):
    
    
    fig=figure(figsize=(12,12),dpi=160)
    
    cell_centres=db.get_property('cell_centres')
    cell_type=db.get_property('cell_type')
    prop=db.get_property('DIIVdata')

    
    prop2=db.get_property('VENUS')
    celltypes=[[2],[4],[3],[16],[5,17,10,11,12,13,14,15],[6,7,8,9,18,19]]
    titles=['Epi','Cor','End','Per','Stele / QC / Columella','LRC']
    for i in range(len(celltypes)):

        ax=fig.add_subplot(3,2,i+1)
        
        cids=[cid for cid,val in cell_type.items() if val in celltypes[i]]
        propvals=array([prop[cid] for cid in cids])
        propvals2=array([prop2[cid] for cid in cids])
        xvals=array([cell_centres[cid][0] for cid in cids])
        yvals=array([cell_centres[cid][1] for cid in cids])

        plt.title(titles[i])
        plt.plot(sorted(xvals),[x for _,x in sorted(zip(xvals,propvals))],'rx-')
        plt.plot(sorted(xvals),[x for _,x in sorted(zip(xvals,propvals2))],'kd-')

        plt.legend(['data','model'])
        plt.xlim([0,500])
   
    savefig(savename)
    clf()

    close(fig)

def plotCellIds(db,fname,rotate=False):

    mesh=get_mesh(db)

    position,cell_centres,norm=rescale_positions(db)
    
    #also rescale the scale(!)
        
    wall_groups=getWallGroups(mesh)

    xmin=min([val[0] for val in position.values()])
    xmax=max([val[0] for val in position.values()])
    ymin=min([val[1] for val in position.values()])
    ymax=max([val[1] for val in position.values()])
    
    if rotate==False:
        fig=figure(figsize=(16,5),dpi=160)
    else:
        fig=figure(figsize=(5,16),dpi=160)
    subplots_adjust(left=0.025, bottom=0.025, right=0.975, top=0.975,wspace=0.025, hspace=0.025)
    ax=fig.add_subplot(111,aspect='equal')
    ax.set_yticks([])
    ax.set_xticks([])
    box(on=None)
    if rotate==False:
        xlim(0,xmax )    # set the xlim to xmin, xmax
        ylim(0, ymax )
    else:
        ylim(0,xmax )    # set the xlim to xmin, xmax
        xlim(0, ymax )

    # Plot root outline:
    if rotate==False:
        for wid in mesh.wisps(1):
            coords=[position[pid] for pid in mesh.borders(1,wid)]
            gca().add_line(Line2D((coords[0][0],coords[1][0]),(coords[0][1],coords[1][1]), lw=1.0,color='gray'))
    else:
        for wid in mesh.wisps(1):
            coords=[position[pid] for pid in mesh.borders(1,wid)]
            gca().add_line(Line2D((coords[0][1],coords[1][1]),(xmax-coords[0][0],xmax-coords[1][0]), lw=1.0,color='gray'))
    cell_type = db.get_property('cell_type')
    for cid,val in cell_centres.items():
        gca().text(val[0], val[1], cell_type[cid],size='12',rotation='horizontal')
        gca().text(val[0], val[1], cid,size='4',rotation='horizontal')
    savefig(fname)
    close()


def plot_celltypes_by_category(db,savename,rotate=False):
 
    #get coordinates
    position,cell_centres,norm = rescale_positions(db)
    mesh=get_mesh(db)
    graph=get_graph(db)
    cell_polygons=get_cell_polygons(mesh,position,rotate)


    # dimension and subplots
    max_x=max([p[0] for p in position.values()])
    max_y=max([p[1] for p in position.values()])

    rowplots=1
    colplots=1
    
    if rotate==False:
        figysize=11
        figxsize=16#int(round((figysize/(max_y*colplots))*(max_x*rowplots)))
    else:
        figxsize=11
        figysize=16#int(round((figxsize/(max_y*colplots))*(max_x*rowplots)))

    fig=figure(figsize=(figxsize,figysize),dpi=160)
    subplots_adjust(left=0.025, bottom=0.025, right=0.975, top=0.975,wspace=0.025, hspace=0.025)

    lwidth=1


    #make plots
    
    ax=fig.add_subplot(rowplots,colplots,1,aspect='equal')
    ax.set_xticks([])
    ax.set_yticks([])

    box(on=None)
    if rotate==False:
        xlim(0,max_x)
        ylim(0,max_y)
    else:
        xlim(0,max_y)
        ylim(0,max_x)
        

    #cell_types={ 0: 'Epidermis',#original
           #1: 'Cortex',
           #2: 'Endodermis',
           #3: 'Pericycle',
           #4: 'Stele'}
    cell_types={ 5: 'Stele',
           16: 'Pericycle',
           2: 'Epidermis',
           3: 'Endodermis',
           4: 'Cortex'}#,
           #11: 'Stele',
           #12: 'Stele',
           #13: 'Stele'}
           #6: 'LRC1 (Lateral Root Cap)',
           #7: 'LRC2',
           #8: 'LRC3',
           #9: 'LRC4',
           #10: 'Columella initials',
           #11: 'S1',
           #12: 'S2',
           #13: 'S3',
           #14: 'S4',
           #15: 'S5',
           #17: 'Quiescent centre (QC)',
           #18: 'Cortex/Endodermis Initials'}
    cell_type=db.get_property('cell_type')
    
    cols={2:'tomato',4:'cornflowerblue',3:'plum',16:'mediumseagreen',5:'greenyellow'}#7:'silver',8:'blueviolet',9:'salmon',
            #10:'darkorange',11:'khaki',12:'gold',13:'peachpuff',14:'sienna',15:'indianred',16:'yellow',17:'red',18:'lime'}

    
    lpatches=[]

    for type_code,type_name in cell_types.items():
    
        use_polygons = [cell_polygons[i] for i,j in enumerate(list(mesh.wisps(2))) if cell_type[j]==type_code]
        p = PatchCollection(use_polygons, cmap=matplotlib.cm.inferno,linewidths=(lwidth,),edgecolor=('black'))
        p.set_color(cols[type_code])
        ax.add_collection(p)
        lpatches.append(matplotlib.patches.Patch(color=cols[type_code], label=type_name))

    plt.legend(handles=lpatches,loc=[1,0.0],fontsize='x-large')
    max_x=max([p[0] for p in position.values()]) 
    for wid in mesh.wisps(1):
        coords=[position[pid] for pid in mesh.borders(1,wid)]
        if rotate==True:
            gca().add_line(Line2D((coords[0][1],coords[1][1]),(max_x-coords[0][0],max_x-coords[1][0]), lw=1.5,color='gray'))
        else:
            gca().add_line(Line2D((coords[0][0],coords[1][0]),(coords[0][1],coords[1][1]), lw=1.5,color='gray'))
    
    #maxx=max([val[0] for val in position.itervalues()])
    #for cid,val in cell_centres.items():
    #    gca().text(val[1],maxx-val[0],  cid,size='4',rotation='horizontal')
    
    
    #save and close
    savefig(savename,facecolor=fig.get_facecolor())
    clf()
    close(fig)
    
def plot_pylab_figure_time(db,savename,prop_list,subplot_dims,rotate=False,time=None):
    #rotate=False
    #check what is on list and if we can plot it
    species_desc=db.get_property('species_desc')
    plottypes=[]
    for prop in prop_list.keys():
        if prop in species_desc:
            plottypes.append(species_desc[prop].lower())
        else:
            print(prop+" not in species_desc - can't plot")
            return
            
    #get coordinates
    position,cell_centres,norm = rescale_positions(db)
    mesh=get_mesh(db)
    graph=get_graph(db)
    if 'cell' in plottypes:
        cell_polygons=get_cell_polygons(mesh,position,rotate)
    if 'wall' in plottypes:
        walls=get_walls(mesh,position,rotate)
    if 'edge' in plottypes:
        edges=get_edges(mesh,position,db.get_property('offset_vectors'),db.get_property('wall'),graph,rotate)
        edge_polys=get_edge_polygons(mesh,position,db.get_property('offset_vectors'),db.get_property('wall'),graph,rotate)

    # dimension and subplots
    max_x=max([p[0] for p in position.values()])
    max_y=max([p[1] for p in position.values()])

    rowplots=subplot_dims[0]
    colplots=subplot_dims[1]
    
    if rotate==False:
        figysize=5
        figxsize=16#int(round((figysize/(max_y*colplots))*(max_x*rowplots)))
    else:
        figxsize=5
        figysize=16#int(round((figxsize/(max_y*colplots))*(max_x*rowplots)))

    fig=figure(figsize=(figxsize,figysize),dpi=160)
    subplots_adjust(left=0.025, bottom=0.025, right=0.975, top=0.975,wspace=0.025, hspace=0.025)

    lwidth=1

    #fig.patch.set_facecolor('grey')


    #make plots
    for i,prop in enumerate(prop_list):
        crange=prop_list[prop]
        cprop=db.get_property(prop)
        
        ax=fig.add_subplot(rowplots,colplots,i+1,aspect='equal')
        ax.set_xticks([])
        ax.set_yticks([])
        #ax.set_title(prop,{'fontsize': 20},'left')
        box(on=None)
        if rotate==False:
            xlim(0,max_x)
            ylim(0,max_y)
        else:
            xlim(0,max_y)
            ylim(0,max_x)
            
        if plottypes[i]=='cell':
            BDL=10.0
            for cid,val in cprop.items():
                if val<0:
                    cprop[cid]=BDL
            Ax_colors = [cprop[cid] for cid in mesh.wisps(2)]
            use_polygons = [cell_polygons[j] for j,val in enumerate(Ax_colors) if val>=0.0]
            #Ax_colors = [cprop[cid] for cid in mesh.wisps(2) if cprop[cid]>=0.0]
            
            p = PatchCollection(use_polygons, cmap=matplotlib.cm.jet,linewidths=(lwidth,),edgecolor=('black'))
        if plottypes[i]=='wall':
            Ax_colors = [cprop[cid] for cid in mesh.wisps(1)]
            p = LineCollection(walls, cmap=matplotlib.cm.jet,linewidths=(lwidth,))

        if plottypes[i]=='edge':
            max_x=max([p[0] for p in position.values()]) 
            for wid in mesh.wisps(1):
                coords=[position[pid] for pid in mesh.borders(1,wid)]
                if rotate==True:
                    gca().add_line(Line2D((coords[0][1],coords[1][1]),(max_x-coords[0][0],max_x-coords[1][0]), lw=1.5,color='gray'))
                else:
                    gca().add_line(Line2D((coords[0][0],coords[1][0]),(coords[0][1],coords[1][1]), lw=1.5,color='gray'))
            
            Ax_colors = [cprop[cid] for cid in graph.edges()]
            
            lpatches=[]

            
            for xval,col in clegend.items():
                use_polygons=[]
                for j,val in enumerate(Ax_colors):
                    if val==xval:
                        use_polygons.append(edge_polys[j])
                p = PatchCollection(use_polygons,linewidths=(0.5,),label=col)
                p.set_color(col)
                ax.add_collection(p)
                lpatches.append(matplotlib.patches.Patch(color=col, label=plabels[xval-1]))
            #plt.legend(handles=lpatches,loc=[0.71,0.0],fontsize='x-large')
            
        if plottypes[i]!='edge':
            p.set_array(array(Ax_colors))
            p.set_clim(crange[0],crange[1])
            ax.add_collection(p)
            cb=colorbar(p,shrink=0.2,use_gridspec=False,anchor=(0.0,0.0))
            #cb=colorbar(p,shrink=0.8)
            if crange[1]>=2.0:
                cb.set_ticks([crange[0],floor(crange[1])/2.0,floor(crange[1])])
            else:
                cb.set_ticks([crange[0],round(crange[1]/2.0,1),floor(crange[1]*10)/10.0])
            for t in cb.ax.get_yticklabels():
                 t.set_fontsize(20)
    print(ylim())
    print(xlim())
    ax.text(0,0,'t=%d s' % time,fontsize=26)
        
    #save and close
    savefig(savename,facecolor=fig.get_facecolor())
    clf()
    close(fig)


#### Legacy code from Heather's work on trying to make projection plots that failed
def plot_data_vs_model_median(db,dataprop,modelprop,savename,rotate=False,scale=None):
	from numpy import log2
	#check what is on list and if we can plot it
	species_desc=db.get_property('species_desc')


	if dataprop in species_desc:
		dataplottype=species_desc[dataprop].lower()
	else:
		print(prop+" not in species_desc - can't plot")
		return
		
	if modelprop in species_desc:
		modelplottype=species_desc[modelprop].lower()
	else:
		print(prop+" not in species_desc - can't plot")
		return
	
	assert modelplottype==dataplottype
	
	#get coordinates
	position,cell_centres,norm = rescale_positions(db)
	mesh=get_mesh(db)
	graph=get_graph(db)
	if modelplottype=='cell':
		cell_polygons=get_cell_polygons(mesh,position,rotate)
	if modelplottype=='wall':
		walls=get_walls(mesh,position)
	if modelplottype=='edge':
		edges=get_edges(mesh,position,db.get_property('offset_vectors'),db.get_property('wall'),graph)


	# dimension and subplots
	max_x=max([p[0] for p in position.values()])
	max_y=max([p[1] for p in position.values()])

	if rotate==False:
		figysize=5
		figxsize=16#int(round((figysize/(max_y*colplots))*(max_x*rowplots)))
	else:
		figxsize=5
		figysize=16#int(round((figxsize/(max_y*colplots))*(max_x*rowplots)))



		
	fig=figure(figsize=(figxsize,figysize),dpi=160)
	subplots_adjust(left=0.025, bottom=0.025, right=0.975, top=0.975,wspace=0.025, hspace=0.025)

	lwidth=1

	#fig.patch.set_facecolor('grey')


	#make plots

	from numpy import median
	mprop=db.get_property(modelprop)
	mprop_list = [v for v in mprop.values()]
	mmean=median(mprop_list)
	dprop=db.get_property(dataprop)
	dprop_list = [v for v in dprop.values()]
	dmean=median(dprop_list)
	
	ax=fig.add_subplot(1,1,1,aspect='equal')
	ax.set_xticks([])
	ax.set_yticks([])
	#ax.set_title('%s model - data' % modelprop,{'fontsize': 20},'left')
	box(on=None)
	
	if rotate==False:
		xlim(0,max_x)
		ylim(0,max_y)
	else:
		xlim(0,max_y)
		ylim(0,max_x)
	
	if modelplottype=='cell':
		Ax_colors=[]
		for cid in mesh.wisps(2):
			if mprop[cid]>0 and dprop[cid]>0:
				Ax_colors.append(dprop[cid]/dmean-mprop[cid]/mmean) #mmean is module minimum
				#Ax_colors.append(mprop[cid]-dprop[cid])
				#Ax_colors.append(log2((mprop[cid]/mmean)/(dprop[cid]/dmean)))
			else:
				Ax_colors.append(0)
	
		p = PatchCollection(cell_polygons, cmap=matplotlib.cm.seismic,linewidths=(lwidth,),edgecolor=('black')) # PuOr swap back to seismic

		
	p.set_array(array(Ax_colors))
	
	crange=max([abs(min(Ax_colors)),abs(max(Ax_colors))])
	if scale!=None:
		crange=scale
	p.set_clim(-crange,crange)
	ax.add_collection(p)
	#cb=colorbar(p,shrink=0.8)
	cb=colorbar(p,shrink=0.2,use_gridspec=False,anchor=(-0.5,0.0))
	for t in cb.ax.get_yticklabels():
		 t.set_fontsize(20)
	

		
	#save and close
	savefig(savename,facecolor=fig.get_facecolor())
	clf()

	close(fig)
	
def plot_data_vs_model_projection(db,dataprop,modelprop,savename,rotate=False,scale=None):
	from numpy import log2
	#check what is on list and if we can plot it
	species_desc=db.get_property('species_desc')


	if dataprop in species_desc:
		dataplottype=species_desc[dataprop].lower()
	else:
		print(prop+" not in species_desc - can't plot")
		return
		
	if modelprop in species_desc:
		modelplottype=species_desc[modelprop].lower()
	else:
		print(prop+" not in species_desc - can't plot")
		return
	
	assert modelplottype==dataplottype
	
	#get coordinates
	position,cell_centres,norm = rescale_positions(db)
	mesh=get_mesh(db)
	graph=get_graph(db)
	if modelplottype=='cell':
		cell_polygons=get_cell_polygons(mesh,position,rotate)
	if modelplottype=='wall':
		walls=get_walls(mesh,position)
	if modelplottype=='edge':
		edges=get_edges(mesh,position,db.get_property('offset_vectors'),db.get_property('wall'),graph)


	# dimension and subplots
	max_x=max([p[0] for p in position.values()])
	max_y=max([p[1] for p in position.values()])

	if rotate==False:
		figysize=5
		figxsize=16#int(round((figysize/(max_y*colplots))*(max_x*rowplots)))
	else:
		figxsize=5
		figysize=16#int(round((figxsize/(max_y*colplots))*(max_x*rowplots)))



		
	fig=figure(figsize=(figxsize,figysize),dpi=160)
	subplots_adjust(left=0.025, bottom=0.025, right=0.975, top=0.975,wspace=0.025, hspace=0.025)

	lwidth=1

	#fig.patch.set_facecolor('grey')


	#make plots

	from numpy import mean
	mprop=db.get_property(modelprop)
	mmean=min(mprop.values())
	dprop=db.get_property(dataprop)
	dmean=min(dprop.values())
	
	ax=fig.add_subplot(1,1,1,aspect='equal')
	ax.set_xticks([])
	ax.set_yticks([])
	#ax.set_title('%s model - data' % modelprop,{'fontsize': 20},'left')
	box(on=None)
	
	if rotate==False:
		xlim(0,max_x)
		ylim(0,max_y)
	else:
		xlim(0,max_y)
		ylim(0,max_x)
	
	if modelplottype=='cell':
		Ax_colors=[]
		for cid in mesh.wisps(2):
			Ax_colors.append(mprop[cid]-dprop[cid])
			#if mprop[cid]>0 and dprop[cid]>0:
				#Ax_colors.append(mprop[cid]/mmean-dprop[cid]/dmean) #mmean is module minimum
				#Ax_colors.append(dprop[cid]-mprop[cid])
				#Ax_colors.append(log2((mprop[cid]/mmean)/(dprop[cid]/dmean)))
			#else:
				#Ax_colors.append(0)
	
		p = PatchCollection(cell_polygons, cmap=matplotlib.cm.seismic,linewidths=(lwidth,),edgecolor=('black')) # PuOr swap back to seismic

		
	p.set_array(array(Ax_colors))
	
	crange=max([abs(min(Ax_colors)),abs(max(Ax_colors))])
	if scale!=None:
		crange=scale
	p.set_clim(-crange,crange)
	ax.add_collection(p)
	#cb=colorbar(p,shrink=0.8)
	cb=colorbar(p,shrink=0.2,use_gridspec=False,anchor=(-0.5,0.0))
	for t in cb.ax.get_yticklabels():
		 t.set_fontsize(20)
	

		
	#save and close
	savefig(savename,facecolor=fig.get_facecolor())
	clf()

	close(fig)
	
def plot_data_vs_model_projection2(db,dataprop,modelprop,savename,rotate=False,scale=None):
	from numpy import log2
	#check what is on list and if we can plot it
	species_desc=db.get_property('species_desc')


	if dataprop in species_desc:
		dataplottype=species_desc[dataprop].lower()
	else:
		print(prop+" not in species_desc - can't plot")
		return
		
	if modelprop in species_desc:
		modelplottype=species_desc[modelprop].lower()
	else:
		print(prop+" not in species_desc - can't plot")
		return
	
	assert modelplottype==dataplottype
	
	#get coordinates
	position,cell_centres,norm = rescale_positions(db)
	mesh=get_mesh(db)
	graph=get_graph(db)
	if modelplottype=='cell':
		cell_polygons=get_cell_polygons(mesh,position,rotate)
	if modelplottype=='wall':
		walls=get_walls(mesh,position)
	if modelplottype=='edge':
		edges=get_edges(mesh,position,db.get_property('offset_vectors'),db.get_property('wall'),graph)


	# dimension and subplots
	max_x=max([p[0] for p in position.values()])
	max_y=max([p[1] for p in position.values()])

	if rotate==False:
		figysize=5
		figxsize=16#int(round((figysize/(max_y*colplots))*(max_x*rowplots)))
	else:
		figxsize=5
		figysize=16#int(round((figxsize/(max_y*colplots))*(max_x*rowplots)))



		
	fig=figure(figsize=(figxsize,figysize),dpi=160)
	subplots_adjust(left=0.025, bottom=0.025, right=0.975, top=0.975,wspace=0.025, hspace=0.025)

	lwidth=1

	#fig.patch.set_facecolor('grey')


	#make plots

	from numpy import mean
	mprop=db.get_property(modelprop)
	mmean=min(mprop.values())
	dprop=db.get_property(dataprop)
	dmean=min(dprop.values())
	
	ax=fig.add_subplot(1,1,1,aspect='equal')
	ax.set_xticks([])
	ax.set_yticks([])
	#ax.set_title('%s model - data' % modelprop,{'fontsize': 20},'left')
	box(on=None)
	
	if rotate==False:
		xlim(0,max_x)
		ylim(0,max_y)
	else:
		xlim(0,max_y)
		ylim(0,max_x)
	
	if modelplottype=='cell':
		Ax_colors=[]
		for cid in mesh.wisps(2):
			Ax_colors.append(log2(mprop[cid]/dprop[cid]))
			#if mprop[cid]>dprop[cid]:
				#Ax_colors.append(mprop[cid]/mmean-dprop[cid]/dmean) #mmean is module minimum
			#   Ax_colors.append(mprop[cid]/dprop[cid])
				#Ax_colors.append(log2((mprop[cid]/mmean)/(dprop[cid]/dmean)))
			#else:
			#    Ax_colors.append(-dprop[cid]/mprop[cid])
	
		p = PatchCollection(cell_polygons, cmap=matplotlib.cm.seismic,linewidths=(lwidth,),edgecolor=('black')) # PuOr swap back to seismic

		
	p.set_array(array(Ax_colors))
	
	crange=max([abs(min(Ax_colors)),abs(max(Ax_colors))])
	if scale!=None:
		crange=scale
	#p.set_clim(2-crange,crange)
	#p.set_clim(0,crange)
	p.set_clim(-crange,crange)
	ax.add_collection(p)
	#cb=colorbar(p,shrink=0.8)
	cb=colorbar(p,shrink=0.2,use_gridspec=False,anchor=(-0.5,0.0))
	for t in cb.ax.get_yticklabels():
		 t.set_fontsize(20)
	

		
	#save and close
	savefig(savename,facecolor=fig.get_facecolor())
	clf()

	close(fig)
	
def plot_relative_property_old(db1,db2,savename,prop,scale):
	rotate=True
	from numpy import log2
	#check what is on list and if we can plot it
	species_desc=db1.get_property('species_desc')


	if prop in species_desc:
		plottype=species_desc[prop].lower()
	else:
		print(prop+" not in species_desc - can't plot")
		return
			
	#get coordinates
	position,cell_centres,norm = rescale_positions(db1)
	mesh=get_mesh(db1)
	graph=get_graph(db1)
	if plottype=='cell':
		cell_polygons=get_cell_polygons(mesh,position,rotate)
	if plottype=='wall':
		walls=get_walls(mesh,position)
	if plottype=='edge':
		edges=get_edges(mesh,position,db1.get_property('offset_vectors'),db1.get_property('wall'),graph)


	# dimension and subplots
	# dimension and subplots
	max_x=max([p[0] for p in position.values()])
	max_y=max([p[1] for p in position.values()])

	if rotate==False:
		figysize=5
		figxsize=16#int(round((figysize/(max_y*colplots))*(max_x*rowplots)))
	else:
		figxsize=5
		figysize=16#int(round((figxsize/(max_y*colplots))*(max_x*rowplots)))

		
	fig=figure(figsize=(figxsize,figysize),dpi=160)
	subplots_adjust(left=0.025, bottom=0.025, right=0.975, top=0.975,wspace=0.025, hspace=0.025)

	lwidth=1



	#make plots


	cprop=db1.get_property(prop)
	cprop2=db2.get_property(prop)
	
	ax=fig.add_subplot(1,1,1,aspect='equal')
	ax.set_xticks([])
	ax.set_yticks([])
	ax.set_title(prop,{'fontsize': 20},'left')
	box(on=None)
	
	if rotate==False:
		xlim(0,max_x)
		ylim(0,max_y)
	else:
		xlim(0,max_y)
		ylim(0,max_x)
	
	if plottype=='cell':
		Ax_colors=[]
		for cid in mesh.wisps(2):
			if cprop[cid]>0 and cprop2[cid]>0:
				#Ax_colors.append(log2(cprop2[cid]/cprop[cid]))
				Ax_colors.append(cprop2[cid]-cprop[cid])
			else:
				#Ax_colors.append(0)
				Ax_colors.append(cprop2[cid]-cprop[cid])
							  
		#Ax_colors = [log2(cprop2[cid]/cprop[cid]) for cid in mesh.wisps(2)]
		p = PatchCollection(cell_polygons, cmap=matplotlib.cm.seismic,linewidths=(lwidth,),edgecolor=('black'))
	if plottype=='wall':
		Ax_colors = [log2(cprop2[cid]/cprop[cid]) for cid in mesh.wisps(1)]
		p = LineCollection(walls, cmap=matplotlib.cm.jet,linewidths=(lwidth,))
	if plottype=='edge':
		Ax_colors = [log2(cprop2[cid]/cprop[cid]) for cid in graph.edges()]
		p = LineCollection(edges, cmap=matplotlib.cm.jet,linewidths=(lwidth,))
		
	p.set_array(array(Ax_colors))
	
	crange=max([abs(min(Ax_colors)),abs(max(Ax_colors))])
	if scale!=None:
		crange=scale
	p.set_clim(-crange,crange)
	ax.add_collection(p)
	#cb=colorbar(p,shrink=0.8)
	cb=colorbar(p,shrink=0.2,use_gridspec=False,anchor=(-0.5,0.0))
	for t in cb.ax.get_yticklabels():
		 t.set_fontsize(20) 
  
	#for cid,pos in cell_centres.items():
	#    ax.text(pos[0],pos[1],str(cid), horizontalalignment='center', verticalalignment='center',fontsize=6)
		
	#save and close
	savefig(savename,facecolor=fig.get_facecolor())
	clf()

	close(fig)
	
def plot_relative_property2_old(db1,db2,savename,prop,scale):
	rotate=True
	from numpy import log2
	#check what is on list and if we can plot it
	species_desc=db1.get_property('species_desc')


	if prop in species_desc:
		plottype=species_desc[prop].lower()
	else:
		print(prop+" not in species_desc - can't plot")
		return
			
	#get coordinates
	position,cell_centres,norm = rescale_positions(db1)
	mesh=get_mesh(db1)
	graph=get_graph(db1)
	if plottype=='cell':
		cell_polygons=get_cell_polygons(mesh,position,rotate)
	if plottype=='wall':
		walls=get_walls(mesh,position)
	if plottype=='edge':
		edges=get_edges(mesh,position,db1.get_property('offset_vectors'),db1.get_property('wall'),graph)


	# dimension and subplots
	# dimension and subplots
	max_x=max([p[0] for p in position.values()])
	max_y=max([p[1] for p in position.values()])

	if rotate==False:
		figysize=5
		figxsize=16#int(round((figysize/(max_y*colplots))*(max_x*rowplots)))
	else:
		figxsize=5
		figysize=16#int(round((figxsize/(max_y*colplots))*(max_x*rowplots)))

		
	fig=figure(figsize=(figxsize,figysize),dpi=160)
	subplots_adjust(left=0.025, bottom=0.025, right=0.975, top=0.975,wspace=0.025, hspace=0.025)

	lwidth=1



	#make plots


	cprop=db1.get_property(prop)
	cprop2=db2.get_property(prop)
	
	ax=fig.add_subplot(1,1,1,aspect='equal')
	ax.set_xticks([])
	ax.set_yticks([])
	ax.set_title(prop,{'fontsize': 20},'left')
	box(on=None)
	
	if rotate==False:
		xlim(0,max_x)
		ylim(0,max_y)
	else:
		xlim(0,max_y)
		ylim(0,max_x)
	
	if plottype=='cell':
		Ax_colors=[]
		for cid in mesh.wisps(2):
			Ax_colors.append(cprop[cid]/cprop2[cid])
			#if cprop[cid]>cprop2[cid]:
				#if cprop[cid]/cprop2[cid]==1:
					#Ax_colors.append(0)
				#else:
					#Ax_colors.append(cprop[cid]/cprop2[cid])
				#Ax_colors.append(cprop[cid]/cprop2[cid])
			#else:
				#if -cprop2[cid]/cprop[cid]==-1:
					#Ax_colors.append(0)
				#else:
					#Ax_colors.append(-cprop2[cid]/cprop[cid])
				#Ax_colors.append(-cprop2[cid]/cprop[cid])
							  
		#Ax_colors = [log2(cprop2[cid]/cprop[cid]) for cid in mesh.wisps(2)]
		p = PatchCollection(cell_polygons, cmap=matplotlib.cm.seismic,linewidths=(lwidth,),edgecolor=('black'))
	if plottype=='wall':
		Ax_colors = [log2(cprop2[cid]/cprop[cid]) for cid in mesh.wisps(1)]
		p = LineCollection(walls, cmap=matplotlib.cm.jet,linewidths=(lwidth,))
	if plottype=='edge':
		Ax_colors = [log2(cprop2[cid]/cprop[cid]) for cid in graph.edges()]
		p = LineCollection(edges, cmap=matplotlib.cm.jet,linewidths=(lwidth,))
		
	p.set_array(array(Ax_colors))
	
	crange=max([abs(min(Ax_colors)),abs(max(Ax_colors))])
	if scale!=None:
		crange=scale
	p.set_clim(2-crange,crange)
	ax.add_collection(p)
	#cb=colorbar(p,shrink=0.8)
	cb=colorbar(p,shrink=0.2,use_gridspec=False,anchor=(-0.5,0.0))
	for t in cb.ax.get_yticklabels():
		 t.set_fontsize(20) 
  
	#for cid,pos in cell_centres.items():
	#    ax.text(pos[0],pos[1],str(cid), horizontalalignment='center', verticalalignment='center',fontsize=6)
		
	#save and close
	savefig(savename,facecolor=fig.get_facecolor())
	clf()

	close(fig)

def plot_data_vs_model_min(db,dataprop,modelprop,savename,rotate=False,scale=None):
	from numpy import log2
	#check what is on list and if we can plot it
	species_desc=db.get_property('species_desc')


	if dataprop in species_desc:
		dataplottype=species_desc[dataprop].lower()
	else:
		print(prop+" not in species_desc - can't plot")
		return
		
	if modelprop in species_desc:
		modelplottype=species_desc[modelprop].lower()
	else:
		print(prop+" not in species_desc - can't plot")
		return
	
	assert modelplottype==dataplottype
	
	#get coordinates
	position,cell_centres,norm = rescale_positions(db)
	mesh=get_mesh(db)
	graph=get_graph(db)
	if modelplottype=='cell':
		cell_polygons=get_cell_polygons(mesh,position,rotate)
	if modelplottype=='wall':
		walls=get_walls(mesh,position)
	if modelplottype=='edge':
		edges=get_edges(mesh,position,db.get_property('offset_vectors'),db.get_property('wall'),graph)


	# dimension and subplots
	max_x=max([p[0] for p in position.values()])
	max_y=max([p[1] for p in position.values()])

	if rotate==False:
		figysize=5
		figxsize=16#int(round((figysize/(max_y*colplots))*(max_x*rowplots)))
	else:
		figxsize=5
		figysize=16#int(round((figxsize/(max_y*colplots))*(max_x*rowplots)))



		
	fig=figure(figsize=(figxsize,figysize),dpi=160)
	subplots_adjust(left=0.025, bottom=0.025, right=0.975, top=0.975,wspace=0.025, hspace=0.025)

	lwidth=1

	#fig.patch.set_facecolor('grey')


	#make plots

	from numpy import mean
	mprop=db.get_property(modelprop)
	mmean=min(mprop.values())
	dprop=db.get_property(dataprop)
	dmean=min(dprop.values())
	
	ax=fig.add_subplot(1,1,1,aspect='equal')
	ax.set_xticks([])
	ax.set_yticks([])
	#ax.set_title('%s model - data' % modelprop,{'fontsize': 20},'left')
	box(on=None)
	
	if rotate==False:
		xlim(0,max_x)
		ylim(0,max_y)
	else:
		xlim(0,max_y)
		ylim(0,max_x)
	
	if modelplottype=='cell':
		Ax_colors=[]
		for cid in mesh.wisps(2):
			if mprop[cid]>0 and dprop[cid]>0:
				Ax_colors.append(dprop[cid]/dmean-mprop[cid]/mmean) #mmean is module minimum
				#Ax_colors.append(mprop[cid]-dprop[cid])
				#Ax_colors.append(log2((mprop[cid]/mmean)/(dprop[cid]/dmean)))
			else:
				Ax_colors.append(0)
	
		p = PatchCollection(cell_polygons, cmap=matplotlib.cm.seismic,linewidths=(lwidth,),edgecolor=('black')) # PuOr swap back to seismic

		
	p.set_array(array(Ax_colors))
	
	crange=max([abs(min(Ax_colors)),abs(max(Ax_colors))])
	if scale!=None:
		crange=scale
	p.set_clim(-crange,crange)
	ax.add_collection(p)
	#cb=colorbar(p,shrink=0.8)
	cb=colorbar(p,shrink=0.2,use_gridspec=False,anchor=(-0.5,0.0))
	for t in cb.ax.get_yticklabels():
		 t.set_fontsize(20)
	

		
	#save and close
	savefig(savename,facecolor=fig.get_facecolor())
	clf()

	close(fig)
