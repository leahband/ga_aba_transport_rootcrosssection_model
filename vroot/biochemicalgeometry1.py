#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 22 19:26:17 2022

@author: user
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 17 02:29:31 2022

@author: user
"""


from vroot.db_utilities import refresh_property, get_wall_decomp, get_tissue_maps,set_parameters,get_parameters,get_graph,get_mesh,def_property,updateVS


from openalea.tissueshape import edge_length, face_surface_2D

from models.set_carriers_wt_GAgeometry import set_carriers
from math import exp
from models.set_plasmodesmataradial1geometry import set_plasmodesmata

from scipy.sparse import lil_matrix
from numpy import zeros, concatenate
from scipy.integrate import odeint, solve_ivp

from functools import partial



class CombinedModel(object):
    """
    Additive combination of GRN models
    """

    name = 'CombinedModel'

    def __init__(self, models):
        """
        :param models: List of models to combine (each implementing
                       the AbstractModel interface)
        """
        self.models=models

    def set_default_parameters(self, db):
        for model in self.models:
            model.set_default_parameters(db)

    def get_diluted_props(self):
        return set.union(*[set(m.diluted_props) for m in self.models])


    def get_species(self):
        """
        :returns: Set of names of species in the combined model
        """
        return set.union(*[set(m.get_species()) for m in self.models])

    def get_steady_species(self):
        return set.union(*[set(m.get_steady_species()) for m in self.models])

    def get_model_names(self):
        names = []
        for model in self.models:
            names.append(model.__class__.__name__)
        return names
    
    def deriv(self, t, y, *args):
        """
        Derivative function - sum of the derivatives for 
        each component model
        :param y: Current state vector
        :type y: numpy.array (or list)
        :param t: Current time
        :type t: float
        :param *args: additional arguments for derivative function
        """
        return sum((m.deriv(t, y, *args) for m in self.models),zeros(y.shape))



    def set_ss(self, db, tissue_maps, wall_decomposition, species_slices):
        for model in self.models:
            model.set_ss(db, tissue_maps, wall_decomposition, species_slices)


class Biochemical(object):
    """
    Class to handle integration of GRN and cell-cell transport
    """
    def __init__(self, db, model):
        """ 
        Initialise the networks 
        :param db: Tissue database
        :type db: TissueDB
        :param model: implementation of AbstractModel
        """
        self.db = db
        # Construct a model for each of the model classes
        self.model=model
        self.model_species=model.get_species()

        self.steady_species=model.get_steady_species()

        species_desc=self.db.get_property('species_desc')


        # Initialise all of the species needed which are not already
        # contained in the TissueDB

        for species_name, species_type  in self.model_species:
            species_desc[species_name]=species_type
            if species_name not in db.properties():
                self.db,prop = def_property(db,species_name,0.0,species_type.upper(), "config","")


        for species_name, species_type  in self.steady_species:
            species_desc[species_name]=species_type
            if species_name not in db.properties():
                self.db,prop = def_property(db, species_name, 0.0,species_type.upper(),"config","")

        model.set_default_parameters(db)

        set_fixed_properties(db)
    
        self.update_tissue()

    def update_tissue(self):
        """
        update various geometric properties of the tissue
        """
        self.wall_decomposition = get_wall_decomp(self.db)
        updateVS(self.db)
        self.tissue_maps = get_tissue_maps(self.db)
        self.offset=0 # current offset into the y[] array
        self.species_slices={} # map from species name to the slice of y[]
        for species_name, species_type in self.model_species:
            # 'size' is how many values are needed to store a property
            # of that type 
            size = len(self.tissue_maps[species_type])
            self.species_slices[species_name] = slice(self.offset, self.offset+size)
            self.offset+=size
        self.Jpat=self.get_Jpat(self.species_slices,self.offset,self.db,self.model)
        #fixed_idx,fixed_steady=self.get_fixed_idx(self.tissue_maps, self.species_slices)
        #self.J=self.set_ss1(self.db,self.tissue_maps,fixed_idx,self.species_slices)
        
    def set_ss1(self, db, tissue_maps, fixed_idx,ssl):
        p={}
        #fixed_idx,fixed_steady=self.get_fixed_idx(self.tissue_maps, self.species_slices)
        #db=self.db
        #tissue_maps = get_tissue_maps(self.db)
        #ssl=self.species_slices
        # Parameters governing GA transport
        p['pHapo']=5.3
        p['pHcyt']=7.0
        p['pK']=4.2
        p['pHvac']=5.5
        p['Voltage']=-0.120
        p['Voltagevac']=-0.03
        p['Temp']=300
        p['Rconst']=8.31
        p['Fdconst']=96500
        p['phi']=p['Fdconst']*p['Voltage']/(p['Rconst']*p['Temp'])
        p['phivac']=p['Fdconst']*p['Voltagevac']/(p['Rconst']*p['Temp'])
        # Parameters governing GA transport
        p['A1']=1/(1+pow(10,p['pHapo']-p['pK'])) # A1, A2, A3, B1, B2, B3 are dimensionless
        p['A2']=(1-p['A1'])*p['phi']/(exp(p['phi'])-1)
        p['A3']=-(1-p['A1'])*p['phi']/(exp(-p['phi'])-1)

        p['B1']=1/(1+pow(10,p['pHcyt']-p['pK']))
        p['B2']=-(1-p['B1'])*p['phi']/(exp(-p['phi'])-1)
        p['B3']=(1-p['B1'])*p['phivac']/(exp(p['phivac'])-1)
        p['B31']=(1-p['B1'])*p['phi']/(exp(p['phi'])-1)
        p['B32']=-(1-p['B1'])*p['phivac']/(exp(-p['phivac'])-1)
        
        p['C1']=1/(1+pow(10,p['pHvac']-p['pK']))
        p['C3']=-(1-p['C1'])*p['phivac']/(exp(-p['phivac'])-1)
        p['C31']=(1-p['C1'])*p['phivac']/(exp(p['phivac'])-1)
        #print(p['A1'])
        #print(p['A2'])
        #print(p['B1'])
        #print(p['B2'])
        #print(p['C1'])
        #print(p['C3'])
        #print(p['B3'])
        #print(p['B32'])
        #print(p['C31'])

        #scale-factor for time during computation (scales out degradation rate)
        p['T']=1/0.000003
        
        p['vac_percentage']=0.9


        p['D_cw']= 32.0    # Diffusion rate in cell wall (microns^2 sec^(-1)) from Kramer 2007.
        p['D_k']= 0 #Diffusivity in the Casparian strip


        # The permeability coefficients for GA (and ABA, you need to change them when you want to run the simulations for the Supplementary figures)
        p['PIAAH']=  0.333#0.028 # Diffusion coefficient of IAAH (micrometers.sec-1)
        p['PInf1']= 0.139#0.0125 # Permeability of anionic GA due to Inf1 active influx carriers (for an Inf1 concentration equal to 1). (micrometers.sec-1)
        p['PInf2']= 0.0097#0.0069 # Permeability of anionic GA due to Inf2 active influx carriers (for an Inf2 concentration equal to 1). (micrometers.sec-1)
        p['PExpvac']=  0.556#0*0.028  # Permeability of anionic GA due to active efflux carriers on vacuole (for a Expvac concentration equal to 1).   (micrometers.sec-1)    
        p['Pback']= 0*0.1 # Background efflux permeability
        p['Pplas'] = 0*8.0/9.92 # Permeability of Plasmodesmata (micrometers.sec-1) 8 in stele (Rustchow et al, 2011) / Plasmodesmata density in transvere wall of Stele
        p['PImpvac'] = 0
        
        '''
        # Expvac type specific permeabilities
        p['PExpvac1'] = 0.304
        p['PExpvac2'] = 0.123
        p['PExpvac3'] = 2.006
        p['PExpvac4'] = 2.006
        p['PExpvac7'] = 2.006
        '''
        
        p['scalefactor']=50/470 # original mesh in pixels, 1 pixel = 0.04 microns; comes from the number of pixels in the scale bar on the experimental image. 

        GA_vacinit= 0#  Initially set GA vacuolar concentrations to be small.

        # GA production and degradation: 
        p['dGA']=1 #because we have scaled this using T
        p['pGA']=('cell_type',{5:0,11:0,12:0,13:0,'else':0})#('cell_type',{1:0.00000009,0:0,'else':0.00000018})
        
        #exogenous GA
        p['outer_concentration']=0.0
        
        
        A1PI=p['A1']*p['PIAAH']
        A2PI1=p['A2']*p['PInf1']
        A2PI2=p['A2']*p['PInf2']
        A3Pb=p['A3']*p['Pback']
        B1PI=p['B1']*p['PIAAH']
        B2PI1=p['B2']*p['PInf1']
        B2PI2=p['B2']*p['PInf2']
        B3Pb=p['B31']*p['Pback']
        Ppla=p['Pplas']
        C1PI=p['C1']*p['PIAAH']
        B3PEv=p['B3']*p['PExpvac']
        B3Piv=p['B32']*p['PImpvac']
        C3PEv=p['C3']*p['PExpvac']
        C3Piv=p['C31']*p['PImpvac']
        
        pGA=db.get_property('pGA')

        Sp = db.get_property('S')
        Vp = db.get_property('V')
        wall_width=db.get_property('wall_width')
        graph=get_graph(db)
        mesh = get_mesh(db)
        
        inner_walls=[]
        outer_walls={}
        for wid in mesh.wisps(1):
            wcids=list(mesh.regions(1,wid))
            if len(wcids)==2:
                inner_walls.append(wid)
            else:
                outer_walls[wid]=wcids[0]
        points_to_walls={pid:list(mesh.regions(0, pid)) for pid in mesh.wisps(0)}
        
        S={}
        V={}
        Sv={}
        Vv={}
        
        cell_to_walls = dict((cid,tuple(mesh.borders(2,cid))) for cid in mesh.wisps(2))
        
        for cid in mesh.wisps(2):
            V[cid]=p['scalefactor']*p['scalefactor']*Vp[cid]
            Vv[cid]=p['vac_percentage']*p['scalefactor']*p['scalefactor']*Vp[cid]
            Sv[cid]=0
            for a in cell_to_walls[cid]:
                Sv[cid] += pow(p['vac_percentage'],2/3)*p['scalefactor']*Sp[a]

        for wid in mesh.wisps(1):
	        S[wid]=p['scalefactor']*Sp[wid]

        GA=db.get_property('GA')
        GA_wall=db.get_property('GA_wall')
        GA_vac=db.get_property('GA_vac')
        


        Expvac = db.get_property("Expvac")
        Inf1 = db.get_property("Inf1")
        Inf2 = db.get_property("Inf2")
        plasmod=db.get_property("plasmodesmata")
        cell_type=db.get_property('cell_type')

            

        cell_map = tissue_maps['cell']
        wall_map = tissue_maps['wall']
        point_map = tissue_maps['point']
        wall_decomposition=get_wall_decomp(db)
        
        
        c_off = len(list(cell_map.keys()))
        w_off = len(list(wall_map.keys()))
        v_off = len(list(point_map.keys()))

        if p['D_cw']>0.0:
            N = 2*c_off+w_off+v_off
        else:
            N = 2*c_off+w_off
            
        J = lil_matrix((self.offset,self.offset))
        r = zeros((N,))
        
        #set fluxes
        for wid in inner_walls:
            widx = ssl['GA_wall'].start + wall_map[wid]

            eid1 = wall_decomposition[wid][0]
            eid2 = wall_decomposition[wid][1]
            sid = graph.source(eid1)
            tid = graph.target(eid1)
            sidx = ssl['GA'].start+cell_map[sid]
            tidx = ssl['GA'].start+cell_map[tid]
            
            #carrier mediated transport and diffusion

            
            Jsw = B1PI+B2PI1*Inf1[eid1]+B2PI2*Inf2[eid1]+ B3Pb
            Jtw = B1PI+B2PI1*Inf1[eid2]+B2PI2*Inf2[eid2]+ B3Pb
            Jws = A1PI+A2PI1*Inf1[eid1]+A2PI2*Inf2[eid1]+ A3Pb
            Jwt = A1PI+A2PI1*Inf1[eid2]+A2PI2*Inf2[eid2]+ A3Pb
            
            T=p['T']
            
            J[sidx, sidx] += -(S[wid]/V[sid])*Jsw*T
            J[sidx, widx] += (S[wid]/V[sid])*Jws*T
            J[widx, sidx] += (1.0/wall_width[wid])*Jsw*T
            J[widx, widx] += -(1.0/wall_width[wid])*Jws*T

            J[tidx, tidx] += -(S[wid]/V[tid])*Jtw*T
            J[tidx, widx] += (S[wid]/V[tid])*Jwt*T
            J[widx, tidx] += (1.0/wall_width[wid])*Jtw*T
            J[widx, widx] += -(1.0/wall_width[wid])*Jwt*T
            
            #inter-cellular diffusion via Plasmodesmata
            Jpla = Ppla*plasmod[wid]

            J[sidx, sidx] += -(S[wid]/V[sid])*Jpla*T
            J[sidx, tidx] += (S[wid]/V[sid])*Jpla*T
            J[tidx, sidx] += (S[wid]/V[tid])*Jpla*T
            J[tidx, tidx] += -(S[wid]/V[tid])*Jpla*T
            
        for wid,cid in outer_walls.items():
        
            widx = ssl['GA_wall'].start + wall_map[wid]
            sidx = ssl['GA'].start + cell_map[cid]
            
            #carrier mediated transport and diffusion
            Jsw= B1PI#+B2PI1+B3Pb
            Jws= A1PI#+A2PI1+A3Pb
            
            J[sidx, sidx] += -(S[wid]/V[cid])*Jsw*T
            J[sidx, widx] += (S[wid]/V[cid])*Jws*T
            J[widx, sidx] += (1.0/wall_width[wid])*Jsw*T
            J[widx, widx] += -(1.0/wall_width[wid])*Jws*T
        
        #production / degradation
        for cid in mesh.wisps(mesh.degree()):
            cidx = ssl['GA'].start + cell_map[cid]
            #J[cidx, cidx] -= p['dGA']#/V[cid]
            r[cidx] += pGA[cid]#/V[cid]
            cidv=ssl['GA_vac'].start+cell_map[cid]
            J[cidx,cidx]+=-p['dGA']-Sv[cid]/V[cid] * (B1PI+B3PEv*Expvac[cid]+B3Piv*Expvac[cid])*T#/V[cid]
            J[cidx,cidv]+=Sv[cid]/V[cid] * (C1PI+C3PEv*Expvac[cid]+C3Piv*Expvac[cid])*T
            J[cidv,cidv]+=-Sv[cid]/Vv[cid] * (C1PI+C3PEv*Expvac[cid]+C3Piv*Expvac[cid])*T
            J[cidv,cidx]+=Sv[cid]/Vv[cid] * (B1PI+B3PEv*Expvac[cid]+B3Piv*Expvac[cid])*T
        
        # prescribe fluxes between adjoining cell wall compartments
        if p['D_cw']>0.0:
            for pid,wids in points_to_walls.items():
                bottom=0.0
                for wid in wids:
                    index_wall=ssl['GA_wall'].start+wall_map[wid]
                    bottom += 1/S[wid]
                GA_vertex = bottom
                for wid in wids:
                    index_wall=ssl['GA_wall'].start+wall_map[wid]
                    J[index_wall,index_wall]+=1/S[wid]*(2*p['D_cw']/S[wid]*((1/S[wid])/GA_vertex-1)) * T
                    for wid1 in wids:
                        if wid1!=wid:
                            index_wall1=ssl['GA_wall'].start+wall_map[wid1]
                            J[index_wall,index_wall1]+=1/S[wid]*(2*p['D_cw']/S[wid]*(1/S[wid1])/GA_vertex) * T
                #for wid in wids:
                    #widx=ssl['GA_wall'].start+wall_map[wid]
                    #J[pidx,pidx] += -(1.0/wall_width[wid])*(2*p['D_cw']/S[wid])
                    #J[pidx,widx] += (1.0/wall_width[wid])*(2*p['D_cw']/S[wid])
                    #J[widx,widx] += -(1.0/S[wid])*(2*p['D_cw']/S[wid])
                    #J[widx,pidx] += (1.0/S[wid])*(2*p['D_cw']/S[wid])


        # fixed properties    
        reverse_map={}
        reverse_ids={}
        for cid,val in GA.items():
            reverse_map[ssl['GA'].start+cell_map[cid]]=val
            reverse_ids[ssl['GA'].start+cell_map[cid]]=cid
        for wid,val in GA_wall.items():
            reverse_map[ssl['GA_wall'].start+wall_map[wid]]=val
            reverse_ids[ssl['GA_wall'].start+wall_map[wid]]=wid
        for cid,val in GA_vac.items():
            reverse_map[ssl['GA_vac'].start+cell_map[cid]]=val
            reverse_ids[ssl['GA_vac'].start+cell_map[cid]]=cid
            
        
        for i in fixed_idx:
            J[i, :] *=0
            J[i, i] = 1
            r[i]=0
            
            #r[i] = -reverse_map[i] ### only needed if section below is commented out
            
            ########## Possibly delete here to not have the graident to zero in top row of cells
            
            if cell_type[reverse_ids[i]] in [2,3,4,16]:
                ncids=list(mesh.border_neighbors(2,reverse_ids[i]))
                found=False
                for xcid in ncids:
                    if cell_type[reverse_ids[i]]==cell_type[xcid]:
                        nid=xcid
                        found=True
                if found==False:
                    print('!!!boundary neighbour not found!!!')

                J[i, ssl['GA'].start+cell_map[nid]] = -1
            else:
                r[i] = -reverse_map[i]
                
			###########
			
        #fixed outer boundary
        if p['outer_concentration']>0.0:
            for wid in self.outer_walls.keys():
                widx = ssl['GA_wall'].start + wall_map[wid]
                J[widx, :] *=0
                J[widx,widx]=1
                r[widx]=-p['outer_concentration']
        return J
    
    def step(self):
        """
        Evolve the gene network for one timestep
        """

        #set fixed properties, may use updated dictionaries from previous step
        set_fixed_properties(self.db)   
   
        # Call routine to find fixed property values
        fixed_idx,fixed_steady=self.get_fixed_idx(self.tissue_maps, self.species_slices)

        # assign steady state values before integration step
        self.model.set_ss(self.db, self.tissue_maps, fixed_idx, self.species_slices)

        #initialise with zero
        y0=zeros((self.offset,))

        ### SETUP INITIAL VALUES
        # Loop over all species referred to in these models
        for species_name, species_type in self.model_species:
            # extract property dict from TissueDB
            prop=self.db.get_property(species_name)
            # obtain appropriate section of y0[]
            y0_slice=y0[self.species_slices[species_name]]
            for tid, idx in self.tissue_maps[species_type].items() :
                y0_slice[idx]=prop[tid]

        ###INTEGRATE OVER TIMESTEP()
        dt=get_parameters(self.db, 'timestep')

        args=(self.db, self.tissue_maps, self.species_slices,self.wall_decomposition, fixed_idx)
        #aras=(self.db, self.tissue_maps, fixed_idx, self.species_slices)
        self.J=self.set_ss1(self.db, self.tissue_maps, fixed_idx, self.species_slices)
        #integrated = odeint(self.model.deriv, y0, [0., dt], args= (self.db, self.tissue_maps, self.species_slices,self.wall_decomposition, fixed_idx)) #, Dfun=self.Jpat)
        integrated = solve_ivp(lambda t, y: self.model.deriv(t, y, *args), [0., dt], y0, method='BDF', t_eval=None, dense_output=False, events=None, vectorized=False)#,jac=self.J)#,jac=None,jac_sparsity=self.Jpat)#, args= (self.db, self.tissue_maps, self.species_slices,self.wall_decomposition, fixed_idx))#,jac_sparsity=self.Jpat)
        
        ###SET SPECIES TO NEW VALUE (AFTER TIMESTEP)
        yend=integrated.y[:,-1]
        
        for species_name, species_type in self.model_species: 
            prop=self.db.get_property(species_name)
            yend_slice=yend[self.species_slices[species_name]]
            for tid, idx in self.tissue_maps[species_type].items() :
                prop[tid]=yend_slice[idx]

        mesh=get_mesh(self.db)
        iwalls=[]
        for wid in mesh.wisps(1):
            if len(list(mesh.regions(1,wid)))==2:
                iwalls.append(self.tissue_maps['wall'][wid])



    def get_fixed_idx(self, tissue_maps, species_slices):
        """
        Obtain a list of those elements in the ODE state vector y
        which are constant during the simulation
        (From the property 'fixed' in the tissue database.)
        This currently only works for cell-based properties
        
        The dictionary fixed maps a property name to a list
        of (expression, value) rules; for each cell, if the expression
        is satisfied, then the property is fixed
        This is a Python expression, and may depend on the cell
        id number and the values of all the properties of the tissue.

        e.g. fixed = { 'GA':[ ('cell_type[cid]=='pericycle', 2) ] }
        would fix the GA concentration in the pericycle.

        :param tissue_maps:
        :param species_slices:
        :returns: list of the offsets into the state vector y[] which
                  are to be held constant.

        """
        fixed = get_parameters(self.db, 'fixed')
        
        cell_map = tissue_maps['cell']
        edge_map = tissue_maps['edge']
        wall_map = tissue_maps['wall']
        fixed_idx = [] # list of fixed indices
        fixed_steady={} # dist of steady props and cids
        # Extract all the properties from the tissue data,
        # so these can be used in the eval statement
        all_properties=dict((name, self.db.get_property(name)) \
                                for name in self.db.properties())
        # Loop over all properties with rules

        for prop_name, rules in fixed.items():      
            # Ignore properties not in model
            if prop_name in species_slices:     
                s=species_slices[prop_name]
                prop=self.db.get_property(prop_name)
                for (expr, value) in rules:
                    # Parse expression and convert it to python
                    # bytecode
                    code=compile(expr, '', 'eval')
                    if dict(self.model_species)[prop_name]=='cell':
                            # Loop over all cells
                        for cid, i in cell_map.items():
                                # Test the expression for this cid
                                if eval(code, all_properties, { 'cid':cid }):
                                    #check cid 
                                    if cid in prop.keys():
                                        idx=s.start+i
                                        fixed_idx.append(idx)
                    if dict(self.model_species)[prop_name]=='wall':
                            # Loop over all walls
                        for cid, i in wall_map.items():
                                # Test the expression for this cid
                                if eval(code, all_properties, { 'cid':cid }):
                                    #check cid
                                    if cid in prop.keys():
                                        idx=s.start+i
                                        fixed_idx.append(idx)
                    if dict(self.model_species)[prop_name]=='edge':
                            # Loop over all walls
                        for cid, i in edge_map.items():
                                # Test the expression for this cid
                                if eval(code, all_properties, { 'cid':cid }):
                                    #check cid
                                    if cid in prop.keys():
                                        idx=s.start+i
                                        fixed_idx.append(idx)
        
            elif prop_name in dict(self.steady_species):
                prop=self.db.get_property(prop_name)
                fixed_steady[prop_name]=[]
                for (expr, value) in rules:
                    # Parse expression and convert it to python
                    # bytecode
                    code=compile(expr, '', 'eval')
                    if dict(self.steady_species)[prop_name]=='cell':
                            # Loop over all cells
                        for cid, i in cell_map.items():
                                # Test the expression for this cid
                                if eval(code, all_properties, { 'cid':cid }):
                                    #check cid 
                                    if cid in prop.keys():
                                        fixed_steady[prop_name].append(cid)
                    if dict(self.steady_species)[prop_name]=='wall':
                            # Loop over all walls
                        for cid, i in wall_map.items():
                                # Test the expression for this cid
                                if eval(code, all_properties, { 'cid':cid }):
                                    #check cid
                                    if cid in prop.keys():
                                        fixed_steady[prop_name].append(cid)
                    if dict(self.steady_species)[prop_name]=='edge':
                            # Loop over all walls
                        for cid, i in edge_map.items():
                                # Test the expression for this cid
                                if eval(code, all_properties, { 'cid':cid }):
                                    #check cid
                                    if cid in prop.keys():
                                        fixed_steady[prop_name].append(cid)
        return fixed_idx,fixed_steady

    def get_Jpat(self,species_slices,offset,db,model):
    #def get_Jpat(self,y,t, *args):#
        JPat=lil_matrix((offset,offset))
        tissue_maps = get_tissue_maps(db)
        cell_map = tissue_maps['cell']
        wall_map = tissue_maps['wall']
        point_map = tissue_maps['point']#
        c_off = len(list(cell_map.keys()))#
        w_off = len(list(wall_map.keys()))#
        v_off = len(list(point_map.keys()))#
        #p['D_cw']=32.0
        #if p['D_cw']>0.0:#
        #    N = 2*c_off+w_off+v_off#
        #else:#
        #    N = 2*c_off+w_off#
        #JPat=lil_matrix((N,N))#
        graph=get_graph(db)
        mesh=get_mesh(db)
        wall_deg=mesh.degree()-1
        wall_decomposition=get_wall_decomp(db)
        
        #TODO: GA is hardcoded - if there is another mobile species
        #will have to update jacobian
        #species_slices=self.species_slices#
        csl = species_slices['GA']
        wsl = species_slices['GA_wall']
        ccsl = species_slices['GA_vac']

        for wid in mesh.wisps(1):
            if len(list(mesh.regions(1,wid)))==2:
                (eid1,eid2)=wall_decomposition[wid]
                cell1 = graph.source(eid1)
                cell2 = graph.target(eid1)
                index_cell1 = csl.start + cell_map[cell1]
                index_cell2 = csl.start + cell_map[cell2]
                index_wall= wsl.start + wall_map[wid]
                
                JPat[index_cell1,index_wall]=1.0
                JPat[index_wall,index_cell1]=1.0
                JPat[index_cell2,index_wall]=1.0
                JPat[index_wall,index_cell2]=1.0
                       #update JPat to incorperate inter-auxi diffusion
                JPat[index_cell1,index_cell2]=1.0
                JPat[index_cell2,index_cell1]=1.0
            else:
                (cidlist)=tuple(mesh.regions(1,wid))
                cid=cidlist[0]  
                index_cell = csl.start+cell_map[cid]
                index_wall= wsl.start+wall_map[wid]
                
                JPat[index_cell,index_wall]=1.0
                JPat[index_wall,index_cell]=1.0
        for cid in mesh.wisps(2):
            cidx=csl.start+cell_map[cid]
            cidv=ccsl.start+cell_map[cid]
            
            JPat[cidx,cidv]=1.0
            JPat[cidv,cidx]=1.0
                
        #wall diffusion
        try:
        # version with ODE points
            point_map=tissue_maps['point']
            psl=species_slices['GA_point']
            for pid in mesh.wisps(0):
                pidx=psl.start+point_map[pid]
                for wid in mesh.regions(0,pid):
                    widx=wsl.start+wall_map[wid]                    
                    JPat[pidx,widx]=1.0
                    JPat[widx,pidx]=1.0

        
        except KeyError:
            # original
            for pid in mesh.wisps(0):
                idx = [wsl.start+wall_map[wid] for wid in mesh.regions(0,pid)]
                for i in idx:
                    JPat[idx,i]=1.0
        
        
        for cid in mesh.wisps(2):
            idx=csl.start+cell_map[cid]
            JPat[idx,idx]=1.0

        return JPat




def set_fixed_properties(db):
    """
    The dictionary fixed maps a property name to a list
    of (expression, value) rules; for each cell, if the expression
    is satisfied, then the property is set to the value.
    This is a Python expression, and may depend on the cell
    id number and the values of all the properties of the tissue.
    If multiple expressions are satisfied, then all of these
    are applied, so the last in the list is used.

    e.g. fixed = { 'GA':[ ('cell_type[cid]=='pericycle', 2) ] }
    would set the GA concentration in the pericycle to 2.

    :param db:  tissue database
    :type db: TissueDB
    """
    # Extract mesh from TissueDB

    mesh = get_mesh(db)
    graph = get_graph(db)
    # Extract all the properties from the tissue data,
    # so these can be used in the eval statement
    all_properties=dict((name, db.get_property(name)) \
                            for name in db.properties())
    # Loop over all properties with rules 
    for prop_name, rules in get_parameters(db, 'fixed').items():

        # Check if property currently in tissue database
        if prop_name in db.properties():
            prop = db.get_property(prop_name)
            for (expr, value) in rules:
                # compile expression to python bytecode
                code=compile(expr, '', 'eval')

                # loop over all cells
        try:
            for cid in mesh.wisps(mesh.degree()):
                # test whether expr is satisfied for this cell
                if eval(code, all_properties, {'cid':cid}):
                #check cid is already a key of property
                    if cid in prop.keys():
                        prop[cid] = value

        #if rule does not apply to cells pass
        except KeyError:
            pass
        try:
            for cid in mesh.wisps(mesh.degree()-1):
                # test whether expr is satisfied for this wall
                if eval(code, all_properties, {'cid':cid}):
                #check cid is already a key of property
                    if cid in prop.keys():
                        prop[cid] = value
        #if rule does not apply to walls pass
        except KeyError:
            pass
        try:
            for cid in graph.edges():
                # test whether expr is satisfied for this edge
                if eval(code, all_properties, {'cid':cid}):
                #check cid is already a key of property
                    if cid in prop.keys():
                        prop[cid] = value
        #if rule does not apply to walls pass           
        except KeyError:
            pass

def model_setup(model,db):

    set_parameters(db, model.__class__.__name__, model.p)
    model.p = get_parameters(db, model.__class__.__name__)
    set_parameters(db, 'fixed', model.fixed)

    model.inner_walls=[]
    model.outer_walls={}
    mesh=get_mesh(db)
    for wid in mesh.wisps(1):
        wcids=list(mesh.regions(1,wid))
        if len(wcids)==2:
            model.inner_walls.append(wid)
        else:
            model.outer_walls[wid]=wcids[0]
    model.points_to_walls={pid:list(mesh.regions(0, pid)) for pid in mesh.wisps(0)}
    

