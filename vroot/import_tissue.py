
from vroot.vtk_output import set_vtk_strings
from vroot.db_utilities import get_empty_db,get_mesh,get_graph,def_property,get_2d_offset_vectors,centroid

import lxml.etree as ET
import xlrd

from openalea.tissueshape import face_surface_2D,edge_length
from openalea.celltissue import TissueDB
from openalea.container import ordered_pids


def import_tissue(fname):
    #it we have an .xml file try and open it using cell set conversion
    if fname.split('.')[-1]=='xml':
        db = import_xml(fname)
    #else assume we have a valid .zip file and (try and) open it as a TissueDB
    else:
        db=TissueDB()    
        db.read(fname)
    
    db=check_tissue(db)
    
    return db


def check_tissue(db):

    if 'scale_px_to_micron' not in db.properties():
        db.set_property('scale_px_to_micron',(1.0,'microns'))
        db.set_description('scale_px_to_micron','scale: pixels to microns')

    scale = db.get_property('scale_px_to_micron')


    if 'border' not in db.properties():
        def_property(db,'border',0,'CELL',"config","")

    if 'time' not in db.properties():
        db.set_property('time', 0)
        db.set_description('time', 'current simulation time')
        itime=0
    else:
        itime=db.get_property('time')

    if 'iteration' not in db.properties():
        db.set_property('iteration', 0)
        db.set_description('iteration', 'current iteration')

    if 'parameters' in db.properties():
        parameters = db.get_property('parameters')
        parameters.clear()

    if 'wall_width' not in db.properties():
        db,wall_width = def_property(db,'wall_width',0.14,'WALL',"config","") #TODO hardcoded wall width


    if 'species_desc' not in db.properties():
        species_desc={'V':'cell','S':'wall','cell_type':'cell','wall_width':'wall'}
        db.set_property('species_desc', species_desc)
        db.set_description('species_desc', 'type of property, i.e. cell,edge or wall')


    if 'offset_vectors' not in db.properties():
        mesh = get_mesh(db)
        offset_vectors=get_2d_offset_vectors(mesh,db.get_property('position'))
        db.set_property('offset_vectors', offset_vectors)
        db.set_description('offset_vectors', "horizontal offset vectors to shrink cells etc.")

    if 'vtk_strings' not in db.properties():
        set_vtk_strings(db)

    #if 'cell_centres' not in db.properties(): #removed the IF! to make sure this is the method used for cell centres
    position=db.get_property('position')
    db,centres = def_property(db,'cell_centres',0,'CELL',"config","")
    mesh = get_mesh(db)
    if mesh.degree()==2:
        for cid in centres.keys():
            centres[cid] = centroid([position[pid] for pid in ordered_pids(mesh,cid)])
            

    return db

def import_xml(filename,scale=0.5078125):

    #read in xml
    tree=ET.parse(filename)
    root=tree.getroot()

    #find cell types
    cellgroups=tree.find('groups')
    cell_types={}
    for gp in cellgroups.iter('group'):
        cell_types[int(gp.get('id'))]=gp.get('name')

    #initialise db, graph, mesh
    db=get_empty_db(cell_types)
    mesh=get_mesh(db)
    graph=get_graph(db)

    #find cells
    cells=tree.find('cells')

    cell_data=[]
    cell_list = []
    cell_group_list = []
    old_cell_id_list = []
    truncated_list = []
    for c in cells.iter('cell'):
        walls=c.find('walls')
        cell_walls=[]
        for w in walls.iter('wall'):
            cell_walls.append(int(w.get('id')))
        cell_data.append(cell_walls)
        cell_group_list.append(int(c.get('group')))
        old_cell_id_list.append(int(c.get('id')))
        truncated_list.append(c.get('truncated') == 'true')
        
    #find walls and point data
    walls=tree.find('walls')
    wc = 0
    wall_data={}
    for w in walls.iter('wall'):
        pts=[]
        for point in w.find('points').iter('point'):
            pts.append((float(point.get('x')), float(point.get('y'))))
        wall_data[int(w.get('id'))]=pts
        wc+=1

    #add walls to mesh, add points to mesh and link
    pt_map={}
    wall_map={}
    pos={}
    for i, w in wall_data.items():
        wall_map[i]=[]
        for pp in zip(w[0:-1], w[1:]):
            wid=mesh.add_wisp(1)
            for p in pp:
                if p not in pt_map:
                    pid=mesh.add_wisp(0)
                    pos[pid]=p
                    pt_map[p]=pid
                mesh.link(1, wid, pt_map[p])
            wall_map[i].append(wid)

    #add cells to mesh and link with walls
    cid_list=[]
    for c in cell_data:
        cid=mesh.add_wisp(2)
        cid_list.append(cid)
        for w in c:
            for wid in wall_map.get(w,[]):
                mesh.link(2, cid, wid)

    #make graph
    wall = {}
    for wid in mesh.wisps(1) :
        if len(list(mesh.regions(1,wid))) == 2 :
            cid1,cid2 = mesh.regions(1,wid)
            eid1 = graph.add_edge(cid1,cid2)
            wall[eid1] = wid
            eid2 = graph.add_edge(cid2,cid1)
            wall[eid2] = wid

    #scale and flip
    y_min = min(v[1] for pid, v in pos.items())
    y_max = max(v[1] for pid, v in pos.items())
    for pid, v in pos.items():
        pos[pid]=(v[0]*scale, (y_min+(y_max-v[1]))*scale)

    # add cell type, border (cells at boundary) and old cell id (reference to original xml)
    cell_type = dict((cid, cell_group_list[i]) 
                      for i, cid in enumerate(cid_list))

    old_cell_id = dict((cid, old_cell_id_list[i]) 
                      for i, cid in enumerate(cid_list))

    border_dict = dict((cid_list[i], 1) 
                  for i,t in enumerate(truncated_list) if t)

    #calculate 'volume' (2D area) of cells and 'surface' (2D length) of walls
    V = {}
    for cid in mesh.wisps(2):
        V[cid] = face_surface_2D(mesh, pos, cid)
   
    S = {}
    for wid in mesh.wisps(1):
        S[wid] = edge_length(mesh, pos, wid)


    species_desc={'V':'cell','S':'wall','cell_type':'cell'}

    #add required properties
    db.set_property('position', pos)
    db.set_description('position', "position")
    db.set_property('old_cell_id', old_cell_id)
    db.set_description('old_cell_id', "original cell id numbers in xml file")
    db.set_property('cell_type', cell_type)
    db.set_description('cell_type', "type of each cell")
    db.set_property('border', border_dict)
    db.set_description('border',"")
    db.set_property('wall', wall)
    db.set_description('wall', "wall corresponding to a given edge")
    db.set_property('V', V)
    db.set_description('V', "Volume of each cell")
    db.set_property('S', S)
    db.set_description('S', "Surface of each wall in m")
    db.set_property("species_desc",species_desc)
    db.set_description("species_desc","association of properties with mesh degree")
    
    db.set_property("tissuename",filename.split('/')[-1].split('.')[0])
    db.set_description("tissuename","tissuename")

    db=check_tissue(db)
    
    return db


#import nuclei intensity from cellset output spreadsheet (given as string of path). BDL is 'below detection limit' and defaults as 10 in cellset

def import_nuclei_intensity_xls(db,spreadsheet,BDL=10.0):

    xml_cids=db.get_property('old_cell_id')
    rev_xcids={ocid:ncid for ncid,ocid in xml_cids.items()}

    xl_workbook = xlrd.open_workbook(spreadsheet)

    xl_sheet = xl_workbook.sheet_by_index(0)
    nuclei_data={cid:BDL for cid in xml_cids.keys()}
    
    for colix,cell in enumerate(xl_sheet.row(0)):
        if cell.value=='Nucleus Intensity':
            NI_index=colix
        if cell.value=='ID':
            ID_index=colix
    
    for row_index in range(1,xl_sheet.nrows):
        if xl_sheet.cell(row_index,NI_index).value=='BDL':
            nuclei_data[rev_xcids[int(xl_sheet.cell(row_index,ID_index).value)]]=BDL
        else:
            nuclei_data[rev_xcids[int(xl_sheet.cell(row_index,ID_index).value)]]=float(xl_sheet.cell(row_index,NI_index).value)

    db.set_property('nuclei_data',nuclei_data)
    db.set_description('nuclei_data','Imported nuclei intensity data')

    db.get_property('species_desc')['nuclei_data']='cell'

    return db

def get_tissue_dimensions(db):
    position=db.get_property('position')
    xspan=scale[0]*(max([pos[0] for pos in position.values()])-min([pos[0] for pos in position.values()]))
    yspan=scale[0]*(max([pos[1] for pos in position.values()])-min([pos[1] for pos in position.values()]))
    print('x span = '+str(xspan)+' um')
    print('y span = '+str(yspan)+' um')
    return xspan,yspan
