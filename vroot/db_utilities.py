from openalea.celltissue import Tissue, ConfigFormat, TissueDB
from openalea.container import ordered_pids
from numpy import array,cross,dot
from scipy.linalg import norm
from openalea.tissueshape import face_surface_3D, edge_length,cell_volume,face_surface_2D

def get_default_cell_types():
    return { 2: 'epiderm',
           4: 'cortex',
           3: 'endoderm',
           5: 'vasculature',
           6: 'LRC1',
           7: 'LRC2',
           8: 'LRC3',
           9: 'LRC4',
           17: 'QC',
           10: 'initials',
           11: 'S1', # S1,2,3,4,5 are the columella?
           12: 'S2',
           13: 'S3',
           14: 'S4',
           15: 'S5',
           16: 'pericycle',
           19: 'protoxylem',
           20: 'metaxylem',
           21: 'xp_pericycle',
           22: 'procambium',
           23: 'phloem'}

def set_parameters(db, name, value):
    """
    Set parameter set with name 'name' from TissueDB
    :param db: The tissue database
    :type db: TissueDB
    :param name: Name of parameter set
    :param value: Parameter values (dictionary of name-value pairs)
    :type value: dict
    :type name: str
    """
    
    if 'parameters' in db.properties():
        parameters = db.get_property('parameters')
    else:
        parameters = {}
        db.set_property('parameters', parameters)
        db.set_description('parameters', "Model parameters")
    parameters[name]=value

    if type(value)==dict:
        for pname,ptypes in value.items():
            if type(ptypes)==tuple:
                db,par_dict=def_property(db,pname,ptypes[1]['else'],'CELL',"config","")
                prop=db.get_property(ptypes[0])
                for ctype,val in ptypes[1].items():
                    for cid in par_dict.keys():
                        if prop[cid]==ctype:
                            par_dict[cid]=val


def set_cell_dependent_parameter(db,pname,default_value,exceptions):
        # Extract mesh from TissueDB

    db,prop=def_property(db,pname,default_value,'CELL',"config","")

    mesh = get_mesh(db)
    graph = get_graph(db)
    
    # Extract all the properties from the tissue data,
    # so these can be used in the eval statement
    all_properties=dict((name, db.get_property(name)) for name in db.properties())
    # Loop over all properties with rules 
    for (expr, value) in exceptions:
        # compile expression to python bytecode
        code=compile(expr, '', 'eval')

        for cid in mesh.wisps(mesh.degree()):
            # test whether expr is satisfied for this cell
            if eval(code, all_properties, {'cid':cid}):
                
            #check cid is already a key of property
                if cid in prop.keys():
                    prop[cid] = value

    set_parameters(db, 'cell_dependent_%s' % pname, [default_value]+exceptions)

def get_parameters(db, name):
    """
    Extract parameter set with name 'name' from TissueDB
    :param db: The tissue database
    :type db: TissueDB
    :param name: Name of parameter
    :type name: str
    :returns: Parameter set dictionary
    """
    return db.get_property('parameters').get(name)

def writeParameters(db,directory):
    # Write parameters of db to filename
    fname='%s/parameters.dat' % directory
    f = open(fname,"w")
    params = db.get_property('parameters')
    for param_name, prop in params.items():
        f.write(param_name + '\n')
        if isinstance(prop, dict) and len(prop)>0:
            for key, value in prop.items():
                f.write('    ' + str(key) + ' : ' + str(value) + '\n')
        else:
            f.write('    '+ str(prop) + '\n')
    f.write('\n')

def get_empty_db(cell_types=None):
    
    t=Tissue()
    POINT = t.add_type('POINT')
    WALL = t.add_type('WALL')
    CELL = t.add_type('CELL')
    mesh_id = t.add_relation('mesh', (POINT,WALL,CELL) )
    mesh = t.relation(mesh_id)

    EDGE = t.add_type('EDGE')
    graph_id = t.add_relation("graph",(CELL,EDGE) )
    graph = t.relation(graph_id)
    
    cfg = ConfigFormat(locals())
    cfg.add('POINT')
    cfg.add('WALL')
    cfg.add('CELL')
    cfg.add('EDGE')
    cfg.add('mesh_id')
    cfg.add('graph_id')
    cfg.add('cell_types')

    db=TissueDB()
    db.set_config('config', cfg.config())
    db.set_tissue(t)
    
    return db
    

def get_mesh(db):
    """
    Extract graph from TissueDB
    :param db: The tissue database
    :type db: TissueDB
    :returns: Mesh
    """
    t = db.tissue()
    cfg = db.get_config('config')
    return t.relation(cfg.mesh_id)

def get_graph(db):
    """
    Extract graph from TissueDB
    :param db: The tissue database
    :type db: TissueDB
    :returns: graph
    """
    t = db.tissue()
    cfg = db.get_config('config')
    return t.relation(cfg.graph_id)


def refresh_property(db, prop_name):
    """ 
    Look in the db - if there is a property called prop_name, clear it.
    Otherwise, create an empty property of that name
    We don't want to call db.set_property with a new dictionary, as
    other references to that property (particularly in the viewer)
    will still point to the old dictionary
    :param db: The tissue database
    :type db: TissueDB
    :param prop_name: Name of the property
    :type prop_name: str
    :returns: Empty property
    """
    try:
        prop = db.get_property(prop_name)
        prop.clear() # Empty the dictionary
    except KeyError:
        prop = {}
        db.set_property(prop_name, prop) # Create an empty property of that name
    return prop

def get_wall_decomp(db):
    """ 
    Calculate the 'wall decomposition', which maps each wall in the mesh
    seperating a pair of cells to its corresponding pair of edges in the
    graph
    :param db: Tissue Database
    :type db: TissueDB
    :returns dict: Wall decomposition. Map from wall ids to the
                   corresponding pair of edge ids.
    """
    wall_decomposition = {}
    wall=db.get_property('wall')
    for eid in wall:
        if wall[eid] not in wall_decomposition:
            wall_decomposition[wall[eid]] = [eid]
        else:
            wall_decomposition[wall[eid]].append(eid)
    return wall_decomposition


def get_tissue_maps(db):
    """
    Calculate the 'cell_map' and 'edge_map':
    maps between the id numbers in the graph and their
    position in the list of the cells and edges
    :param db: Tissue Database (modified in place)
    :type db: TissueDB
    :returns: {'cell': cell_map, 'edge': edge_map}
              cell_map is a dictionary mapping cell ids
              to offsets. Similarly for edge_map.
    """
    t = db.tissue()
    cfg = db.get_config('config')
    graph = t.relation(cfg.graph_id) # directed cell to cell graph
    mesh = t.relation(cfg.mesh_id)

    cell_map={}  
    for ind, cid in enumerate(mesh.wisps(mesh.degree())) :
        cell_map[cid] = ind
    edge_map={}
    for ind, eid in enumerate(graph.edges()) :
        edge_map[eid] = ind
    wall_map={}
    for ind, wid in enumerate(mesh.wisps(mesh.degree()-1)) :
        wall_map[wid] = ind

    #return {'cell': cell_map, 'edge': edge_map, 'wall': wall_map }
    point_map={}
    for ind, pid in enumerate(mesh.wisps(0)) :
        point_map[pid] = ind
    
    return {'cell': cell_map, 'edge': edge_map, 'wall': wall_map ,'point': point_map}

   
# function to remove a cell from tissue db and tidy up walls, edges, properties etc.
def remove_cell2d(cid,db,allocation_rules,V,S,wall_width):
    species_desc=db.get_property('species_desc')

    point_props=['position']

    mesh=get_mesh(db)
    orig_vals={}
    #note values of properties to be reallocated e.g. auxin to auxin_wall

    Vo=V[cid]
    for source in allocation_rules.keys():
        if type(source)==str:
            prop=db.get_property(source) #assume this a cell property
            orig_vals[source]=prop[cid]
        else: #assume its a number
            orig_vals[source]=source

    #delete cell from mesh (after noting border walls)
    
    wids=list(mesh.borders(2,cid))
    mesh.remove_wisp(2,cid)
    
    #delete refs to properties of that cell
    for pname,ptype in species_desc.items():
        if ptype=='cell':
            prop=db.get_property(pname)
            del prop[cid]
            
    remaining_walls=[]
    for wid in wids:
        #find 'orphan' wids
        if mesh.nb_regions(1,wid)==0:
            #remove wall (after noting points)
            pids=list(mesh.borders(1,wid))
            mesh.remove_wisp(1,wid)
            #remove properties associated with this wall
            for pname,ptype in species_desc.items():
                if ptype=='wall':
                    prop=db.get_property(pname)
                    del prop[wid]
            #remove 'orphan' points
            for pid in pids:
                if mesh.nb_regions(0,pid)==0:
                    mesh.remove_wisp(0,pid)
                    for pname in point_props:
                        prop=db.get_property(pname)
                        del prop[pid]
        else:
            remaining_walls.append(wid)
            
    #reallocate properties based on area/volume
    
    total_wall_area=sum([wall_width[wid]*S[wid] for wid in remaining_walls])
    
    for source,target in allocation_rules.items():
        prop=db.get_property(target)
        for wid in remaining_walls:
            prop[wid]+=Vo*orig_vals[source]/total_wall_area

    
    #resolve edge properties
    graph=get_graph(db)
    etd=[]
    wall=db.get_property('wall')
    for eid,wid in wall.items():
        try:
            test=graph.source(eid)
        except:
            etd.append(eid)
    for eid in etd:
        del wall[eid]
        graph.remove_vertex(eid)
    for pname,ptype in species_desc.items():
        if ptype=='edge':
            prop=db.get_property(pname)
            for eid in etd:
                del prop[eid]
    return remaining_walls
    

class DummyFunc (object) :
        def __init__ (self, val) :
                self._val = val

        def __call__ (self) :
                return self._val
     
def def_property (db, name, val, elm_type, cfg, descr) :
    cfg = db.get_config(cfg)
    prop = {}

    if callable(val) :
        func = val
    else :
        func = DummyFunc(val)

    for elmid in db._tissue.elements(cfg[elm_type]) :
        prop[elmid] = func()

        db.set_property(name,prop)
        db.set_description(name,descr)


    return db,prop



def CellToCoordinates(mesh,cell_to_walls,border_to_coordinates):

    ptc=[]
    for ncid,cid in enumerate(mesh.wisps(2)):
        ptc.append([])
        patches=[]
  
        for i in range(len(cell_to_walls[cid])):
            patches.append(border_to_coordinates[cell_to_walls[cid][i]])

        ptc[ncid]=[]
        ptc[ncid].append(patches[0][0])
        ptc[ncid].append(patches[0][1])
        patches.remove(patches[0])
        i=0
        while i <len(patches):
            if patches[i].count(ptc[ncid][-1])==1:
                id1=patches[i].index(ptc[ncid][-1])
                id2=abs(-1+id1)
                ptc[ncid].append(patches[i][id2])
                patches.remove(patches[i])
                i=-1
            i=i+1

    cell_to_coordinates = dict((cid,ptc[ncid]) for ncid,cid in enumerate(mesh.wisps(2)))

    return cell_to_coordinates


def updateVS(db):
    """ 
    Calculate cell volumes and surface area between cells
    (updating these in the TissueDB properties V and S). 
    :param db: Tissue Database (modified in place)
    :type db: TissueDB
    """
#    graph = get_graph(db)
    mesh = get_mesh(db)
    pos = db.get_property('position')
    wall = db.get_property('wall')
    scale = db.get_property('scale_px_to_micron')

    if mesh.degree()==3:
        # Area of each cell
        V = refresh_property(db, 'V')
        scale3=scale[0]*scale[0]*scale[0]
        for cid in mesh.wisps(3):
            V[cid]=scale3*cell_volume(mesh, pos, cid)
    
        # Lengths of each wall
        scale2=scale[0]*scale[0]
        S = refresh_property(db, 'S')
        for wid in mesh.wisps(2):
            S[wid] = scale2*face_surface_3D(mesh, pos, wid)

    if mesh.degree()==2:
        # Area of each cell
        V = refresh_property(db, 'V')
        for cid in mesh.wisps(2):
            V[cid]=scale[0]*scale[0]*face_surface_2D(mesh, pos, cid)

        
        # Lengths of each wall
        S = refresh_property(db, 'S')
        for wid in mesh.wisps(1):
            S[wid] = scale[0]*edge_length(mesh, pos, wid)

def get_offset_vec2(pvals):
    #points: pt, pt+1, pt-1, centroid

    dp = array(pvals[1]) - array(pvals[0]) 
    dp = dp / norm(dp) # unit pi->p1
    dm = array(pvals[0]) - array(pvals[2])
    dm = dm / norm(dm) # unit pl->pi

    to_centre=array(pvals[3])-array(pvals[0])
    k = cross(to_centre,dm)
    if norm(k)!=0:
        k = k/norm(k)

    ni = cross((dp + dm),k) # normal to k, pt 1 to -1

    if norm(ni)!=0:
        ni = ni / norm(ni)
    normal = cross(dp,k) # normal to k, pt 0 to 1
    dot_p = dot(normal, ni)

    if dot_p!=0:
        vec = ni/dot_p
    else:
        vec = ni
    
    # check angle between point to cell centre and vec is < pi/2
    if dot(to_centre/norm(to_centre),ni)<=0:
        vec=vec*-1
    # assumes z direction is vertical
    #vec = vec - array([0.0,0.0,1.0])
    
    return vec

def get_2d_offset_vectors(mesh,position):

    offset_vectors={}
    for cid in mesh.wisps(2):
        offset_vectors[cid]={}
        
        pids = ordered_pids(mesh,cid)
        cell_pts = [position[i] for i in pids]
        xvals=[i[0] for i in cell_pts]
        yvals=[i[1] for i in cell_pts]
        xlen=len(cell_pts)
        xy_centre=[sum(xvals)/xlen,sum(yvals)/xlen,0.0]
        N=len(pids)
        for i,pid in enumerate(pids):
            
            pvals = [list(position[pid]),list(position[pids[(i+1)%N]]),list(position[pids[i-1]])]
            for val in pvals:
                if len(val)==2:
                    val.append(0.0)
            pvals.append(xy_centre) 
            offset_vectors[cid][pid]=get_offset_vec2(pvals)
    
    #look for parallel offset vectors - if so need to reverse one, so pick one that opposes its neighbouring points in that cell
    checked_pids={}
    pid_other_cell={}
    for cid,pidvecs in offset_vectors.items():
        for pid,vec in pidvecs.items():
            if len(list(mesh.regions(0,pid)))==2:
                if pid not in checked_pids:
                    checked_pids[pid]=vec
                    pid_other_cell[pid]=cid
                else:
                    if norm(checked_pids[pid]-vec)<1.0: #
                        negcids=[]
                        for xid in mesh.region_neighbors(0,pid):
                            if dot(offset_vectors[cid][xid],offset_vectors[cid][pid])<0:
                                negcids.append(cid)
                            if dot(offset_vectors[pid_other_cell[pid]][xid],offset_vectors[pid_other_cell[pid]][pid])<0:
                                negcids.append(pid_other_cell[pid])
                        negcids=list(set(negcids))
                        if len(negcids)==0:
                            print("no cell picked")# ?should something else happen
                        elif len(negcids)>1:
                            print("both cells picked")# ?should something else happen
                        else:
                            offset_vectors[negcids[0]][pid]=offset_vectors[negcids[0]][pid]*-1

    return offset_vectors


def pairs(lst):
    """Generaterator which returns all the sequential pairs of lst,
    treating lst as a circular list
    
    >>> list(pairs([1,2,3]))
    [(1, 2), (2, 3), (3, 1)]
    """     

    i = iter(lst)
    first = prev = next(i)
    for item in i:
        yield prev, item
        prev = item
    yield item, first



def area(poly):
    """ 
    Calculates the area of a polygon
    
    Args:
        poly: list of the vertices of a polygon in counterclockwise order

    Returns:
        float. Area of the polygon
    """
    return sum((0.5*(pp[0][0]*pp[1][1]-pp[1][0]*pp[0][1]) 
                for pp in pairs(poly)))

     
def centroid(poly):
    """ 
    Calculates the centroid of a polygon, defined by
    a list of points

    Args:
        poly: list of the vertices of a polygon in counterclockwise order

    Returns:
        numpy.ndarray. Centroid of the polygon
    
    >>> centroid([(0,0),(1,0),(1,1),(0,1)])
    array([ 0.5,  0.5])
    """
    A = area(poly)
    cx = sum(((pp[0][0]+pp[1][0])*(pp[0][0]*pp[1][1]-pp[1][0]*pp[0][1]) 
            for pp in pairs(poly)))/(6.0*A)
    cy = sum(((pp[0][1]+pp[1][1])*(pp[0][0]*pp[1][1]-pp[1][0]*pp[0][1]) 
            for pp in pairs(poly)))/(6.0*A)
    return (cx, cy)
