#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 17 02:53:49 2022

@author: user
"""


from vroot.biochemicalgeometry1 import CombinedModel,Biochemical,model_setup

from vroot.vtk_output import db_to_vtu,vtu_series
from vroot.pylab_plotgeometry import plot_pylab_figure,plotsummarystats,plotGAFlux


from vroot.import_tissue import import_tissue
from vroot.vtk_output import set_vtk_strings

from vroot.db_utilities import get_parameters, set_parameters, writeParameters, remove_cell2d,get_mesh,def_property
from openalea.scheduler import Scheduler,Task

import os,sys
import pickle  # To save data.
from datetime import datetime
from scipy.stats import scoreatpercentile
import time

class Simulation(object):
    def __init__(self,*args,**kwargs):

        # import tissue (if given string)
        if type(args[0])==str:
            self.db=import_tissue(args[0])
            self.tissuename=args[0].split('/')[-1].split('.')[0] # name of tissue file minus path and extension
        else:
            self.db=args[0]
            if 'tissuename' in self.db.properties():
                self.tissuename=self.db.get_property('tissuename')
            else:
                self.tissuename='unknown'

        # set default timestep and container for time points
        set_parameters(self.db, 'timestep', 1)

        # initialise scheduler and default no division or growth
        self.sch = Scheduler()
        self.division=False
        self.growth=False

        # initialise models
        self.combined_model=None
        self.give_output=True
        model_initialisation_functions={'biochemical':self.init_biochemical,'division':self.init_division,'growth':self.init_growth}
        for mtype,model in kwargs.items():
            if mtype == 'output':
                if model==False:
                    self.give_output=False
            else:
                model_initialisation_functions[mtype](model)

        # set and make output directory. if second input argument is a string use that, otherwise use current timecode
        
        
        if self.combined_model!=None:
            self.modelname='_'.join(self.combined_model.get_model_names())
        else:
            self.modelname='no_biochemical_model'
            
        if len(args)>1:
            self.simname=args[1]
            if sys.platform=='win32':
                self.dir_name= 'output\%s\%s\%s' % (self.tissuename,self.modelname,self.simname)
            else:
                self.dir_name= 'output/%s/%s/%s' % (self.tissuename,self.modelname,self.simname)
        else:
            self.simname=int(time.time())
            if sys.platform=='win32':
                self.dir_name= 'output\%s\%s\t%10d' % (self.tissuename,self.modelname,self.simname)
            else:
                self.dir_name= 'output/%s/%s/t%10d' % (self.tissuename,self.modelname,self.simname)

        if self.give_output==True:
            if sys.platform=='win32':
                os.system('mkdir %s' % self.dir_name)
            else:
                os.system('mkdir -p %s' % self.dir_name)
            if len(os.listdir(self.dir_name))>0:
                if sys.platform=='win32':
                    os.system('del %s\* /Q' % self.dir_name)
                else:
                    os.system('rm %s/*' % self.dir_name)


        # define and schedule update of iteration

        def update_iteration():
            t=self.db.get_property('time')
            t+=get_parameters(self.db, 'timestep')
            self.db.set_property('time', t)
            iteration=self.db.get_property('iteration')
            iteration+=1
            self.db.set_property('iteration', iteration)

        self.sch.register(Task(update_iteration, 1, 10, "add iteration"))


        # schedule update of vtk strings if division or growth is active
        self.vtk_output_types=['cell','wall','edge']
        def update_vtk():
            set_vtk_strings(self.db)
        if self.division or self.growth:
            self.sch.register(Task(update_vtk, 1, 3, "set vtk strings"))


    #######run simulations#############
    #run this database for last_step - first_step iterations, setting initial iteration to be first_step
    #tissue db and paraview output at each step
    def run_simulation(self,last_step):

        first_step=self.db.get_property('iteration')
        #self.output()
        with open('%s/GAiter_00000.pickle' % (self.dir_name), 'wb') as handle:
            pickle.dump(self.get_db().get_property('GA'), handle, protocol=pickle.HIGHEST_PROTOCOL)
        with open('%s/GA_vaciter_00000.pickle' % (self.dir_name), 'wb') as handle:
            pickle.dump(self.get_db().get_property('GA_vac'), handle, protocol=pickle.HIGHEST_PROTOCOL)
        
        start = datetime.now()
        print(self.dir_name[7:]+', start time: '+str(start.strftime('%H'))+':'+str(start.strftime('%M')))
        for i in range(first_step,last_step):
            now=datetime.now()
            tstr=str(now.strftime('%H'))+':'+str(now.strftime('%M'))
            sys.stdout.write("\r"+tstr+' iteration '+str(self.db.get_property('iteration')+1)+' of '+str(last_step)+', model time='+str(self.db.get_property('time')))
            sys.stdout.flush()
            next ( self.sch . run ())
            #self.output()
            with open('%s/GAiter_%05d.pickle' % (self.dir_name,i+1), 'wb') as handle:
                pickle.dump(self.get_db().get_property('GA'), handle, protocol=pickle.HIGHEST_PROTOCOL)
            with open('%s/GA_vaciter_%05d.pickle' % (self.dir_name,i+1), 'wb') as handle:
                pickle.dump(self.get_db().get_property('GA_vac'), handle, protocol=pickle.HIGHEST_PROTOCOL)
            with open('%s/GAwalliter_%05d.pickle' % (self.dir_name,i+1), 'wb') as handle:
                pickle.dump(self.get_db().get_property('GA_wall'), handle, protocol=pickle.HIGHEST_PROTOCOL)
            
        tdelta=datetime.now()-start
        print('\n')
        print('total time: '+str(round(tdelta.total_seconds()))+' seconds')
        if self.give_output==True:
            #vtu_series(self.dir_name,self.vtk_output_types)
            writeParameters(self.db,self.dir_name)

    #run this database for last_step - first_step iterations, setting initial iteration to be first_step
    #tissue db and paraview output at each step, time stored in final tissue db
    def run_simulation_tcdb(self,last_step):

        first_step=self.db.get_property('iteration')
        
        for propname,ptype in self.db.get_property('species_desc').items():
            if '%s_tc' % propname not in self.db.properties():
                self.db.set_property('%s_tc' % propname,{})
                self.db.set_description('%s_tc' % propname,'timecourse of %s' % propname)
            
                xprop=self.db.get_property(propname)
                tcprop=self.db.get_property('%s_tc' % propname)

                for xid,val in xprop.items():
                    tcprop[xid]=[val]
                    
        start = datetime.now()
        
        t=self.db.get_property('time')
        
        if 'timepoints' not in self.db.properties():
            self.db.set_property('timepoints',[t])
            self.db.set_description('timepoints','list of all simulated timepoints')
        
     

        print(self.dir_name[7:]+', start time: '+str(start.strftime('%H'))+':'+str(start.strftime('%M')))
        for i in range(first_step,last_step):
            now=datetime.now()
            tstr=str(now.strftime('%H'))+':'+str(now.strftime('%M'))
            sys.stdout.write("\r"+tstr+' iteration '+str(self.db.get_property('iteration')+1)+' of '+str(last_step)+', model time='+str(self.db.get_property('time')))
            sys.stdout.flush()
            next ( self.sch . run ())

            t=self.db.get_property('time')
            self.db.get_property('timepoints').append(t)
            for propname,ptype in self.db.get_property('species_desc').items():
                xprop=self.db.get_property(propname)
                tcprop=self.db.get_property('%s_tc' % propname)
                for xid,val in xprop.items():
                    tcprop[xid].append(val)
            print('\n')
            print('writing')
            self.db.write('%s/tissuedb.zip' % self.dir_name)
            db_to_vtu(self.db,self.dir_name,'iter_%05d.vtu' % i,self.vtk_output_types)
        tdelta=datetime.now()-start
        print('total time: '+str(round(tdelta.total_seconds()))+' seconds')


    #run model until maximum absolute difference between this and previous iteration is less than tolerance for all model species given
    def run_to_steady_state(self,propnames,tolerance):

        maxdifference=1000000000000000.0
        if type(propnames) == str:
            propnames=[propnames]
        lastvals={pname:self.db.get_property(pname).copy() for pname in propnames}

        with open('%s/GAiter_00000.pickle' % (self.dir_name), 'wb') as handle:
            pickle.dump(self.get_db().get_property('GA'), handle, protocol=pickle.HIGHEST_PROTOCOL)
        with open('%s/GA_vaciter_00000.pickle' % (self.dir_name), 'wb') as handle:
            pickle.dump(self.get_db().get_property('GA_vac'), handle, protocol=pickle.HIGHEST_PROTOCOL)
        start = datetime.now()
        print(self.dir_name[7:]+', start time: '+str(start.strftime('%H'))+':'+str(start.strftime('%M'))+', theshold: '+str(tolerance))
        while maxdifference>tolerance:
            #self.output()
            i=self.db.get_property('iteration')
            with open('%s/GAiter_%05d.pickle' % (self.dir_name,i+1), 'wb') as handle:
                pickle.dump(self.get_db().get_property('GA'), handle, protocol=pickle.HIGHEST_PROTOCOL)
            with open('%s/GA_vaciter_%05d.pickle' % (self.dir_name,i+1), 'wb') as handle:
                pickle.dump(self.get_db().get_property('GA_vac'), handle, protocol=pickle.HIGHEST_PROTOCOL)
            now=datetime.now()
            tstr=str(now.strftime('%H'))+':'+str(now.strftime('%M'))
            sys.stdout.write("\r"+tstr+' iteration '+str(self.db.get_property('iteration')+1)+', maxdifference='+str(maxdifference)+', model time='+str(self.db.get_property('time')))
            sys.stdout.flush()
            next ( self.sch.run() )
            alldiffs=[]
            for pname in propnames:
                prop=self.db.get_property(pname)
                pdiffs=[abs(prop[cid]-val) for cid,val in lastvals[pname].items()]
                if len(pdiffs)>0:
                    alldiffs.append(max(pdiffs))
            if len(alldiffs)>0:
                maxdifference=min(maxdifference,max(alldiffs))
            lastvals={pname:self.db.get_property(pname).copy() for pname in propnames}
        tdelta=datetime.now()-start
        print('\n')
        print('total time: '+str(round(tdelta.total_seconds())),' seconds')
        #self.output()
        #vtu_series(self.dir_name,self.vtk_output_types)
        writeParameters(self.db,self.dir_name)
        return self.db.get_property('iteration')


    #########change settings ###############

    def set_sim_name(self,name):
        old_name = self.dir_name
        self.dir_name= 'output/%s/%s/%s' % (self.tissuename,self.modelname,name)
        os.system('mv %s %s' % (old_name,self.dir_name))

    def set_initial_values(self,ivs):
        for pname,val in ivs.items():
            prop=self.db.get_property(pname)
            for cid in prop.keys():
                prop[cid]=val
        set_parameters(self.db, 'initial_values', ivs)
        self.output()

    def set_timestep(self,tstep):
        set_parameters(self.db, 'timestep', tstep)

    def set_fixed_dict(self,fixed_dict):
        set_parameters(self.db, 'fixed', fixed_dict)

    def set_param_values(self,param_values):
        names = self.combined_model.get_model_names()
        overwritten={}
        for name in names:
            p = get_parameters(self.db, name)
            for par,value in param_values.items():
                if par in p.keys():
                    previous=p[par]
                    p[par]=value
                    overwritten[par]=previous
            set_parameters(self.db, name, p)
        if self.growth:
            p = get_parameters(self.db, self.gr.name)
            for par,value in param_values.items():
                if par in p.keys():
                    previous=p[par]
                    p[par]=value
                    overwritten[par]=previous
            set_parameters(self.db, self.gr.name, p)
        if self.division:
            p = get_parameters(self.db, self.dv.name)
            for par,value in param_values.items():
                if par in p.keys():
                    previous=p[par]
                    p[par]=value
                    overwritten[par]=previous
            set_parameters(self.db, self.dv.name, p)
        set_parameters(self.db,'overwritten parameters',overwritten)
        print('overwritten:')
        print(param_values)

        writeParameters(self.db,self.dir_name)
        for model in self.combined_model.models:
            model_setup(model,self.db)

    def set_scale(self,px_to_micron):
        self.db.set_property('scale_px_to_micron',(px_to_micron,'microns'))
        self.db.set_description('scale_px_to_micron','scale: pixels to microns')

    def set_cell_wall_width(self,cell_wall_width):
        self.db.set_property('cell_wall_width',cell_wall_width)
        set_vtk_strings(self.db)

    def set_vtk_output_types(self,types):
        self.vtk_output_types=types
        
    def reset_time(self):
        self.db.set_property('time',0)
        self.db.set_property('iteration',0)


    #########get values / objects ###############

    def get_db(self):
        return self.db
        
    def get_mesh(self):
        return get_mesh(self.db)
        
    def get_max(self,pname):
        return max(list(self.get_db().get_property(pname).values()))
        
    def get_min(self,pname):
        return min(list(self.get_db().get_property(pname).values()))

    def get_95pc(self,pname):
        return scoreatpercentile(list(self.get_db().get_property(pname).values()),95)


    ############output methods#############
    def plot_figure(self,fname,prop_dict,sub_plots,rotate=True,clegend=None,plabels=None):
        plot_pylab_figure(self.get_db(),'%s/%s' % (self.dir_name,fname),prop_dict,sub_plots,rotate,clegend,plabels)

    def plot_summary(self,fname,props,ylim=None):
        if type(props)==str:
            props=[props]
        plotsummarystats(self.db,'%s/%s' % (self.dir_name,fname),props,ylim)
        
    def plot_flux(self,fname,FluxProp,scale,threshold,colour='r',rotate=True):
        plotGAFlux(self.get_db(),'%s/%s' % (self.dir_name,fname),self.get_db().get_property(FluxProp),scale,threshold,colour,rotate)

    def output(self):
        if self.give_output==True:
            i=self.db.get_property('iteration')
            self.db.write('%s/iter_%05d.zip' % (self.dir_name,i))
            db_to_vtu(self.db,self.dir_name,'iter_%05d.vtu' % i,self.vtk_output_types)
            
    def configure_output(self,*args,**kwargs):
        if 'zips' in kwargs:
            self.output_zips=kwargs['zips']
        if 'vtus' in kwargs:
            self.output_vtus=kwargs['vtus']
        if 'steps' in kwargs:
            self.output_steps=kwargs['steps']
        if 'timecourses' in kwargs:
            self.output_timecourses=kwargs['timecourses']



    ##############initialise models##############

    def init_biochemical(self,model_list):
        if type(model_list)==list:
            mlist=[model() for model in model_list]
        else:
            mlist=[model_list()]
        self.combined_model=CombinedModel(mlist) # calls the CombinedModel class from biochemical.py

        self.biochemical=Biochemical(self.db, self.combined_model) # calls the Biochemical class from biochemical.py
        self.sch.register(Task(self.biochemical.step, 1, 6, "biochemical") )

    def init_growth(self,model):
        self.gr=model(self.db)
        self.sch.register(Task(self.gr.step, 1, 8, "growth") )
        self.growth=True

    def init_division(self,model):
        self.dv=model(self.db)
        self.sch.register(Task(self.dv.step, 1, 7, "division") )
        self.division=True

    ##############events#################
    
    def remove_cell(self,cid,allocation_rules):

        walls = remove_cell2d(cid,self.db,allocation_rules,self.db.get_property('V'),self.db.get_property('S'),self.db.get_property('wall_width'))
        set_vtk_strings(self.db)
        for model in self.combined_model.models:
            model_setup(model,self.db)
        self.biochemical.update_tissue()

        return walls
    def remove_cell_3Daxi(self,cid,allocation_rules):

        walls = remove_cell2d(cid,self.db,allocation_rules,self.db.get_property('V3D'),self.db.get_property('S3D'),self.db.get_property('wall_width'))
        set_vtk_strings(self.db)
        for model in self.combined_model.models:
            model_setup(model,self.db)
        self.biochemical.update_tissue()

        return walls
