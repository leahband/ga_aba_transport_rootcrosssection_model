#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 15 14:16:38 2022

@author: user
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr  3 17:17:47 2022

@author: user
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr  3 12:46:27 2022

@author: user
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 17 03:29:51 2022

@author: user
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 26 12:51:31 2021

@author: user
"""

# Program by Kristian.

# Plots time dependence of concentrations averaged over cell-types from a series of pickled data structures (one for each time point).

import sys
sys.path.append('../')


import _pickle as pickle

from numpy import *
from pylab import *

import matplotlib
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
import matplotlib.pyplot as plt

from vroot.db_utilities import *
from openalea.celltissue import TissueDB


def Plot_Dynamic1(db,N,dt,T,C):
    PATH='output/Geometry_steady1/GAtransport/Plots'
    cell_type = db.get_property('cell_type')
    dt=0.1
    N=130
    M=42
########################################################################################################
# Iterations: 
#########################################################################################################
    time1=T*arange(0,float(0.01)/3600*(N+0.5),float(0.01)/3600)
    time2=T*arange(float(0.01)/3600*N+float(dt)/3600,float(0.01)/3600*N+float(dt)/3600*(M+0.5),float(dt)/3600)
    time=concatenate((time1,time2))

    GA_endo1=[]
    GA_vac_endo1=[]
    GA_endo2=[]
    GA_vac_endo2=[]
    GA_endo3=[]
    GA_vac_endo3=[]
    GA_endo4=[]
    GA_vac_endo4=[]

    for iteration in range(0,N+1):
        fname='output/crosssection_withcelltypes_nodespacing20/GAtransport/GAVacuole09NoPlasAllTransportersDiffusionSyn0001Deg0000003DynamicPaper5days/GAiter_%05d.pickle'%(iteration)
        fdata=open(fname,'rb')
        GA=pickle.load(fdata)
        fdata.close()
        fname='output/crosssection_withcelltypes_nodespacing20/GAtransport/GAVacuole09NoPlasAllTransportersDiffusionSyn0001Deg0000003DynamicPaper5days/GA_vaciter_%05d.pickle'%(iteration)
        fdata=open(fname,'rb')
        GA_vac=pickle.load(fdata)
        fdata.close()
        GA_endo_list1=[]
        GA_vac_endo_list1=[]
        for cid in cell_type.keys():
            if cell_type[cid]==3:
                GA_endo_list1.append(GA[cid])
                GA_vac_endo_list1.append(GA_vac[cid])
        GA_endo1.append(C*average(GA_endo_list1))
        GA_vac_endo1.append(C*average(GA_vac_endo_list1))
        
    ##########################################################################

    for iteration in range(1,M+1):
        fname='output/Geometry_steady1/GAtransport/GAVac09NoPlasAllTransportersDiffusionSyn0001Deg0000003Paper5daysSynthesis0/GAiter_%05d.pickle'%(iteration)
        fdata=open(fname,'rb')
        GA=pickle.load(fdata)
        fdata.close()
        fname='output/Geometry_steady1/GAtransport/GAVac09NoPlasAllTransportersDiffusionSyn0001Deg0000003Paper5daysSynthesis0/GA_vaciter_%05d.pickle'%(iteration)
        fdata=open(fname,'rb')
        GA_vac=pickle.load(fdata)
        fdata.close()
        GA_endo_list1=[]
        GA_vac_endo_list1=[]
        for cid in cell_type.keys():
            if cell_type[cid]==3:
                GA_endo_list1.append(GA[cid])
                GA_vac_endo_list1.append(GA_vac[cid])
        GA_endo1.append(C*average(GA_endo_list1))
        GA_vac_endo1.append(C*average(GA_vac_endo_list1))
        
    for iteration in range(0,N+1):
        fname='output/crosssection_withcelltypes_nodespacing20/GAtransport/GAVacuole09NoPlasNoNPF214DiffusionSyn0001Deg0000003DynamicPaper5days/GAiter_%05d.pickle'%(iteration)
        fdata=open(fname,'rb')
        GA=pickle.load(fdata)
        fdata.close()
        fname='output/crosssection_withcelltypes_nodespacing20/GAtransport/GAVacuole09NoPlasNoNPF214DiffusionSyn0001Deg0000003DynamicPaper5days/GA_vaciter_%05d.pickle'%(iteration)
        fdata=open(fname,'rb')
        GA_vac=pickle.load(fdata)
        fdata.close()
        GA_endo_list2=[]
        GA_vac_endo_list2=[]
        for cid in cell_type.keys():
            if cell_type[cid]==3:
                GA_endo_list2.append(GA[cid])
                GA_vac_endo_list2.append(GA_vac[cid])
        GA_endo2.append(C*average(GA_endo_list2))
        GA_vac_endo2.append(C*average(GA_vac_endo_list2))
        
    ##########################################################################

    for iteration in range(1,M+1):
        fname='output/Geometry_steady1/GAtransport/GAVac09NoPlasNoNPF214DiffusionSyn0001Deg0000003Paper5days/GAiter_%05d.pickle'%(iteration)
        fdata=open(fname,'rb')
        GA=pickle.load(fdata)
        fdata.close()
        fname='output/Geometry_steady1/GAtransport/GAVac09NoPlasNoNPF214DiffusionSyn0001Deg0000003Paper5days/GA_vaciter_%05d.pickle'%(iteration)
        fdata=open(fname,'rb')
        GA_vac=pickle.load(fdata)
        fdata.close()
        GA_endo_list2=[]
        GA_vac_endo_list2=[]
        for cid in cell_type.keys():
            if cell_type[cid]==3:
                GA_endo_list2.append(GA[cid])
                GA_vac_endo_list2.append(GA_vac[cid])
        GA_endo2.append(C*average(GA_endo_list2))
        GA_vac_endo2.append(C*average(GA_vac_endo_list2))

    sidemargin=0.1   # Give the margins around the tissue as a proportion of the figure (i.e. in [0,1])
    bottommargin=0.15

    fig=figure(figsize=(12,6))
    fig.subplots_adjust(bottom=bottommargin)
    fig.subplots_adjust(top=1-bottommargin)
    fig.subplots_adjust(left=sidemargin)
    fig.subplots_adjust(right=1-0.1*sidemargin)

    ax=fig.add_subplot(111)
    ax.plot(time,GA_endo1,'b-',linewidth=2)
    ax.plot(time,GA_endo2,'r-',linewidth=2)
    labels = ax.get_xticklabels()
    setp(labels,  fontsize=16)
    labels = ax.get_yticklabels()
    setp(labels, fontsize=16)
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
    ax.legend(('Wild type','npf 2.14'),prop={'size':20}, loc=1) 
    ax.set_xlabel('Maturation (hours)',fontsize=20)
    ax.set_ylabel('GA concentration',fontsize=20)
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')

    fname = '%s/SynthesisTonoplastPaper5daysGA_N%d_dt%d'%(PATH,N,dt)
    savefig(fname)
    close()
    
    fig=figure(figsize=(12,6))
    fig.subplots_adjust(bottom=bottommargin)
    fig.subplots_adjust(top=1-bottommargin)
    fig.subplots_adjust(left=sidemargin)
    fig.subplots_adjust(right=1-0.1*sidemargin)

    ax=fig.add_subplot(111)
    ax.plot(time,GA_vac_endo1,'b-',linewidth=2)
    ax.plot(time,GA_vac_endo2,'r-',linewidth=2)
    labels = ax.get_xticklabels()
    setp(labels,  fontsize=16)
    labels = ax.get_yticklabels()
    setp(labels, fontsize=16)
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
    ax.legend(('Wild type','npf 2.14'),prop={'size':20}, loc=1) 
    ax.set_xlabel('Maturation (hours)',fontsize=20)
    ax.set_ylabel('GA concentration in vacuole',fontsize=20)
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')

    fname = '%s/SynthesisTonoplastVacuolePaper5daysGA_N%d_dt%d'%(PATH,N,dt)
    savefig(fname)
    close()


