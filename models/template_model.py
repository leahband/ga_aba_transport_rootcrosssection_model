class AbstractModel(object):
    """
    Abstract object used to document Model interface -
    implement this interface rather than subclass
    Here for documentation purposes
    """



    def get_species(self):
        """
        Species used in model
        
        :returns: list of (species_name, type) pairs.
                  e.g. [('auxin', 'cell'), ('PIN', 'edge')]
        """
        pass

    def set_default_parameters(self, db):
        """
        Set default parameters in TissueDB
        :param db: Tissue database
        :type db: TissueDB
        """
        pass

        
    def deriv(y, t, db, tissue_maps, species_slices, wall_decomposition, 
              fixed_idx):
        """
        Right hand side for the system of equations.
        :param y: current state vector
        :type y: numpy.ndarray
        :param t: current simulation time 
        :type t: float
        :param db:  tissue database
        :type db: TissueDB
        :param tissue_maps: dictionary mapping property types ('cell', 'wall',
                            'edge') to a dictionary which takes id numbers to
                            positions in slices of the y for properties of 
                            that type.
                            For example, tissue_maps['cell'] gives a dictionary
                            of the form { 101: 0, 123: 1, 121: 2, ...}
                            so the cell with cid 101 maps to the first element
                            of the slice.
                            y[species_slice['auxin']][tissue_maps['cell'][28]]
                            is the auxin concentration in cell with cid 28
        :type tissue_maps: dict
        :param species_slices: dictionary mapping property names to slices of
                               y. e.g. auxin = y[species_slice['auxin']]
        :type species_slices: dict
        :param wall_decomposition: dictionary which takes a wall id to the
                                   ids of the two corresponding edges.
              
        :param fixed_idx: indices of the elements of y[] which are to be kept
                          constant
        :type fixed_idx: list
        :returns: - numpy.array which is the right hand side of
                        the system of ODEs              
        """
        pass
