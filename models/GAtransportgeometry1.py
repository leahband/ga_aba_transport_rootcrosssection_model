#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 22 20:07:24 2022

@author: user
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 17 03:05:28 2022

@author: user
"""

from numpy import zeros
from math import exp, sqrt
from vroot.db_utilities import get_mesh, get_graph, get_parameters, set_parameters, get_wall_decomp, get_tissue_maps,def_property
from numpy import size, pi
from models.set_plasmodesmataradial1geometry import set_plasmodesmata
from vroot.biochemicalgeometry1 import model_setup

class GAtransport(object):
    """

    Implements AbstractModel
    """
    jacobian_links = {'cells_to_walls':[('GA','GA_wall')],'cells_to_cells':[],'intracellular':{}}

    diluted_props = ['GA','GA_wall']

    def get_species(self):
        return [('GA_vac','cell'),('GA', 'cell'),('GA_wall','wall')]

    def get_steady_species(self):
        return [('Expvac','cell'),('Inf1','edge'),('Inf2','edge'),('plas_flux','wall'),('carrier_flux','wall'),('combined_flux','wall'),
                ('Inf1Inf2_flux','wall'),('Expvac_flux','wall'),('passive_flux','wall')]

    def set_default_parameters(self, db):
        p={}

        # Parameters governing GA transport
        p['pHapo']=5.3
        p['pHcyt']=7.0
        p['pK']=4.2
        p['pHvac']=5.5
        p['Voltage']=-0.120
        p['Voltagevac']=-0.03
        p['Temp']=300
        p['Rconst']=8.31
        p['Fdconst']=96500
        p['phi']=p['Fdconst']*p['Voltage']/(p['Rconst']*p['Temp'])
        p['phivac']=p['Fdconst']*p['Voltagevac']/(p['Rconst']*p['Temp'])
        # Parameters governing GA transport
        p['A1']=1/(1+pow(10,p['pHapo']-p['pK'])) # A1, A2, A3, B1, B2, B3 are dimensionless
        p['A2']=(1-p['A1'])*p['phi']/(exp(p['phi'])-1)
        p['A3']=-(1-p['A1'])*p['phi']/(exp(-p['phi'])-1)

        p['B1']=1/(1+pow(10,p['pHcyt']-p['pK']))
        p['B2']=-(1-p['B1'])*p['phi']/(exp(-p['phi'])-1)
        p['B3']=(1-p['B1'])*p['phivac']/(exp(p['phivac'])-1)
        p['B31']=(1-p['B1'])*p['phi']/(exp(p['phi'])-1)
        p['B32']=-(1-p['B1'])*p['phivac']/(exp(-p['phivac'])-1)
        
        p['C1']=1/(1+pow(10,p['pHvac']-p['pK']))
        p['C3']=-(1-p['C1'])*p['phivac']/(exp(-p['phivac'])-1)
        p['C31']=(1-p['C1'])*p['phivac']/(exp(p['phivac'])-1)
        print(p['A1'])
        print(p['A2'])
        print(p['B1'])
        print(p['B2'])
        print(p['C1'])
        print(p['C3'])
        print(p['B3'])


        p['D_cw']= 32.0    # Diffusion rate in cell wall (microns^2 sec^(-1)) from Kramer 2007.
        p['D_k']= 0 #diffusivity in the Casparian strip


        # The permeability coefficients 
        p['PIAAH']= 0.333#0.028#0.333#0.0222 #0.33 # Diffusion coefficient of IAAH (micrometers.sec-1)
        p['PInf1']=0.139#0.0125#0.139#0.017#0.01 # Permeability of anionic GA due to Inf1 active influx carriers (for an Inf1 concentration equal to 1). (micrometers.sec-1)
        p['PInf2']= 0.0097#0.0069#0.0097 # Permeability of anionic GA due to Inf2 active influx carriers (for an Inf2 concentration equal to 1). (micrometers.sec-1)
        p['PExpvac']= 0.556#0*0.028#0.556  # Permeability of anionic GA due to active efflux carriers on vacuole (for a Expvac concentration equal to 1).   (micrometers.sec-1)    
        p['Pback']= 0 # Background efflux permeability
        p['Pplas'] = 0*8.0/9.92 # Permeability of Plasmodesmata (micrometers.sec-1) 8 in stele (Rustchow et al, 2011) / Plasmodesmata density in transvere wall of Stele
        #scale-factor for time during computation
        p['T']=1/0.000003
        p['C']=1
        p['PImpvac'] = 0

        GA_vacinit=0  # Initially set vacuolar concentration to be small.

        # GA production and degradation:
        #because it is scaled out by T
        p['dGA']=1
        p['pGA']=('cell_type',{5:0.001/0.000003,'else':0})#('cell_type',{1:0.00000009,'else':0.00000018})
        
        #exogenous GA
        p['OuterFluxRate']=100.0
        p['outer_concentration']=0.0
        
        p['vac_percentage']=0.9

        self.fixed={'GA':[('cid in border.keys() and cell_type[cid] in [2,3,4,16]',0.0)]}
        
        set_plasmodesmata(db)
        self.p=p
        model_setup(self,db)


    def deriv(self, t, y, db, tissue_maps, ssl, wall_decomposition, fixed_idx):
        p=self.p

        ret = zeros((len(y),))

        cell_map = tissue_maps['cell']
        wall_map = tissue_maps['wall']
        
        A1PI=p['A1']*p['PIAAH']
        A2PI1=p['A2']*p['PInf1']
        A2PI2=p['A2']*p['PInf2']
        A3Pb=p['A3']*p['Pback']
        B1PI=p['B1']*p['PIAAH']
        B2PI1=p['B2']*p['PInf1']
        B2PI2=p['B2']*p['PInf2']

        B3Pb=p['B31']*p['Pback']
        Ppla=p['Pplas']
        p['scalefactor']=50/470 # original mesh in pixels, 1 pixel = 0.04 microns; comes from the number of pixels in the scale bar on the experimental image. 
        C1PI=p['C1']*p['PIAAH']
        B3PEv=p['B3']*p['PExpvac']
        B3Piv=p['B32']*p['PImpvac']
        C3PEv=p['C3']*p['PExpvac']
        C3Piv=p['C31']*p['PImpvac']

        Expvac = db.get_property("Expvac")
        
        Inf1 = db.get_property("Inf1")
        Inf2 = db.get_property("Inf2")
        plasmod=db.get_property("plasmodesmata")

        # model equations
        Vp = db.get_property('V')
        Sp = db.get_property('S')
        wall_width=db.get_property('wall_width')
        pGA=db.get_property('pGA')
        
        S={}
        V={}
        Sv={}
        Vv={}

        mesh=get_mesh(db)
        graph=get_graph(db)
        
        cell_to_walls = dict((cid,tuple(mesh.borders(2,cid))) for cid in mesh.wisps(2))
        
        for cid in mesh.wisps(2):
            V[cid]=(1-p['vac_percentage'])*p['scalefactor']*p['scalefactor']*Vp[cid]
            Vv[cid]=p['vac_percentage']*p['scalefactor']*p['scalefactor']*Vp[cid]
            Sv[cid]=0
            for a in cell_to_walls[cid]:
                Sv[cid] += pow(p['vac_percentage'],2/3)*p['scalefactor']*Sp[a]

        for wid in mesh.wisps(1):
	        S[wid]=p['scalefactor']*Sp[wid]

        T=p['T']

        for wid in self.inner_walls:
                
            (eid1,eid2)=wall_decomposition[wid]
            cell1 = graph.source(eid1)
            cell2 = graph.target(eid1)
            index_cell1 = ssl['GA'].start+cell_map[cell1]
            index_cell2 = ssl['GA'].start+cell_map[cell2]
            index_wall = ssl['GA_wall'].start+wall_map[wid]

            Swall=S[wid]
            Vcell1=V[cell1]
            Vcell2=V[cell2]
            Vwall=S[wid]*wall_width[wid]

            #inter-cellular diffusion via Plasmodesmata                
            J1PLA = Ppla*plasmod[wid]*(y[index_cell1]-y[index_cell2]) 
            J2PLA = Ppla*plasmod[wid]*(y[index_cell2]-y[index_cell1])
            
            J1 = (B1PI+B2PI1*Inf1[eid1]+B2PI2*Inf2[eid1]+ B3Pb)*y[index_cell1]-(A1PI+A2PI1*Inf1[eid1]+A2PI2*Inf2[eid1]+ A3Pb)*y[index_wall]
            J2 = (B1PI+B2PI1*Inf1[eid2]+B2PI2*Inf2[eid2]+ B3Pb)*y[index_cell2]-(A1PI+A2PI1*Inf1[eid2]+A2PI2*Inf2[eid2]+ A3Pb)*y[index_wall]

            ret[index_cell1] += -Swall/Vcell1 * (J1+J1PLA) * T
            ret[index_cell2] += -Swall/Vcell2 * (J2+J2PLA) * T
            ret[index_wall]  += Swall/Vwall*(J1+J2) * T
            
        for wid,cid in self.outer_walls.items():

            index_cell = ssl['GA'].start+cell_map[cid]
            index_wall= ssl['GA_wall'].start+wall_map[wid]
            Swall=S[wid]
            Vcell=V[cid]
            Vwall=S[wid]*wall_width[wid]
            #J1=(B1PI+B2PI1+B3Pb)* y[index_cell]-(A1PI+A2PI1+A3Pb)*y[index_wall]
            J1=(B1PI)* y[index_cell]-(A1PI)*y[index_wall]

            ret[index_cell] += -Swall/Vcell * J1 * T                                     
            ret[index_wall] += Swall/Vwall*J1 * T

            ret[index_wall] += max(0.0,p['OuterFluxRate']*(p['outer_concentration']-y[index_wall])) * T

        # Calculate effective GA concentration at each vertex (in limit as vertex volume -> 0)
        # and prescribe fluxes between adjoining cell wall compartments.
        cell_type=db.get_property('cell_type')
        
        for pid,wids in self.points_to_walls.items():
            top = 0.0
            bottom = 0.0
            for wid in wids:
                index_wall=ssl['GA_wall'].start+wall_map[wid]
                top += y[index_wall]/S[wid]
                bottom += 1/S[wid]
            GA_vertex = top/bottom
            for wid in wids:
                index_wall=ssl['GA_wall'].start+wall_map[wid]
                eid1 = wall_decomposition[wid][0]
                eid2 = wall_decomposition[wid][1]
                sid = graph.source(eid1)
                tid = graph.target(eid1)
                if (cell_type[sid]==3 or cell_type[tid]==3):
                    ret[index_wall]+=1/S[wid]*(2*p['D_k']/S[wid]*(GA_vertex-y[index_wall])) * T
                else:
                    ret[index_wall]+=1/S[wid]*(2*p['D_cw']/S[wid]*(GA_vertex-y[index_wall])) * T
        #production and degradation

        for cid in mesh.wisps(2):
            cidx=ssl['GA'].start+cell_map[cid]
            cidv=ssl['GA_vac'].start+cell_map[cid]
            ret[cidx]+=(pGA[cid]-p['dGA']*y[cidx])-Sv[cid]/V[cid] * ((B1PI+B3PEv*Expvac[cid]+B3Piv*Expvac[cid])*y[cidx]-(C1PI+C3PEv*Expvac[cid]+C3Piv*Expvac[cid])*y[cidv]) * T#/V[cid]
            if Vv[cid]>0:
                ret[cidv]+=-Sv[cid]/Vv[cid] * ((C1PI+C3PEv*Expvac[cid]+C3Piv*Expvac[cid])*y[cidv]-(B1PI+B3PEv*Expvac[cid]+B3Piv*Expvac[cid])*y[cidx]) * T
            else:
                ret[cidv]+=0

        for i in fixed_idx:
            ret[i] = 0.0
        return ret

    def set_ss(self, db, tissue_maps, fixed_steady,ssl):
        p=self.p
        plas_flux=db.get_property('plas_flux')
        carrier_flux=db.get_property('carrier_flux')
        combined_flux=db.get_property('combined_flux')
        Inf1Inf2_flux=db.get_property('Inf1Inf2_flux')
        Expvac_flux=db.get_property('Expvac_flux')
        passive_flux=db.get_property('passive_flux')

        A1PI=p['A1']*p['PIAAH']
        A2PI1=p['A2']*p['PInf1']
        A2PI2=p['A2']*p['PInf2']
        A3Pb=p['A3']*p['Pback']
        B1PI=p['B1']*p['PIAAH']
        B2PI1=p['B2']*p['PInf1']
        B2PI2=p['B2']*p['PInf2']
        B3Pb=p['B3']*p['Pback']
        Ppla=p['Pplas']
        C1PI=p['C1']*p['PIAAH']
        B3PEv=p['B3']*p['PExpvac']
        C3PEv=p['C3']*p['PExpvac']

        Expvac = db.get_property("Expvac")
        
        Inf1 = db.get_property("Inf1")
        Inf2 = db.get_property("Inf2")
        plasmod=db.get_property("plasmodesmata")
        
        GA = db.get_property("GA")
        GA_wall = db.get_property("GA_wall")


        
        V = db.get_property('V')
        S = db.get_property('S')
        wall_width=db.get_property('wall_width')
        pGA=db.get_property('pGA')

        mesh=get_mesh(db)
        graph=get_graph(db)
        wall_decomposition=get_wall_decomp(db)

        for wid in self.inner_walls:
            
                
            (eid1,eid2)=wall_decomposition[wid]
            sid = graph.source(eid1)
            tid = graph.target(eid1)

            Swall=S[wid]
            Vcell1=V[sid]
            Vcell2=V[tid]
            Vwall=S[wid]*wall_width[wid]


            Jpla = Ppla*plasmod[wid]
            J1PLA = Ppla*plasmod[wid]*(GA[sid]-GA[tid]) 
            plas_flux[wid]=J1PLA/S[wid]
            
            J1 = (B1PI+B2PI1*Inf1[eid1]+B2PI2*Inf2[eid1]+ B3Pb)*GA[sid]-(A1PI+A2PI1*Inf1[eid1]+A2PI2*Inf2[eid1]+ A3Pb)*GA_wall[wid]
            J2 = (B1PI+B2PI1*Inf1[eid2]+B2PI2*Inf2[eid2]+ B3Pb)*GA[tid]-(A1PI+A2PI1*Inf1[eid2]+A2PI2*Inf2[eid2]+ A3Pb)*GA_wall[wid]
            carrier_flux[wid] = (J1-J2)/2*S[wid]

            J1 = (B1PI+B2PI1*Inf1[eid1]+B2PI2*Inf2[eid1])*GA[sid]-(A1PI+A2PI1*Inf1[eid1]+A2PI2*Inf2[eid1])*GA_wall[wid]
            J2 = (B1PI+B2PI1*Inf1[eid2]+B2PI2*Inf2[eid2])*GA[tid]-(A1PI+A2PI1*Inf1[eid2]+A2PI2*Inf2[eid2])*GA_wall[wid]
            Inf1Inf2_flux[wid] = (J1-J2)/2*S[wid]

            J1 = 0
            J2 = 0
            Expvac_flux[wid] = (J1-J2)/2*S[wid]
            
            J1 = (B3Pb)*GA[sid]-(A3Pb)*GA_wall[wid]
            J2 = (B3Pb)*GA[tid]-(A3Pb)*GA_wall[wid]
            passive_flux[wid] = (J1-J2)/2*S[wid]
            
            combined_flux[wid]=carrier_flux[wid]+plas_flux[wid]

        