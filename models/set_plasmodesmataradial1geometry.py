#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 17 03:22:04 2022

@author: user
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 16 15:42:38 2021

@author: user
"""

from vroot.db_utilities import get_mesh,get_graph,get_wall_decomp


def set_plasmodesmata(db):

    mesh=get_mesh(db)
    graph=get_graph(db)
    wall_decomposition=get_wall_decomp(db)
    cell_type=db.get_property('cell_type')
    position=db.get_property('position')
    border_to_coordinates = dict((wid,\
                        [[position[tuple(mesh.borders(1,wid))[0]][0],\
                          position[tuple(mesh.borders(1,wid))[0]][1]],\
                         [position[tuple(mesh.borders(1,wid))[1]][0],\
                          position[tuple(mesh.borders(1,wid))[1]][1]]]\
                        ) for wid in mesh.wisps(1)) 
    plasmod={}
    for wid in mesh.wisps(1):
        plasmod[wid]=0

    #define plasmodesmata permeabilities (tangential and radial, n.b. longtitudinal seems to be used synonomously with tangental in Zhu)
    #Primary Tissue (immature)
    peri_endo_tang = 0.3#3.08 
    epi_rad = 0.1*3.33
    endo_rad = 0.1*5.17 #n.b. in Zhu the endodermis is referred to as the inner cortex
    endo_cort_tang = 0.26#3
    cort_rad = 0.1*2.25 #n.b. in Zhu the model 'cortex' is refered to as the outer cortex
    cort_epi_tang = 0.13#2.33
    peri_rad = 0.1*2.42
    ste_rad = 0.1*2.42
    peri_ste_tang = 0.1*2.42
    
    for wid,(eid1,eid2) in wall_decomposition.items() :
        cid1=graph.source(eid1)
        cid2=graph.source(eid2)
        
        #Transverse walls
        if cell_type[cid1] == 2 and cell_type[cid2] == 2: #Epidermis radial walls
            plasmod[wid] = epi_rad
        if cell_type[cid1] == 3 and cell_type[cid2] == 3: #Endodermis radial walls
            plasmod[wid] = endo_rad
        if cell_type[cid1] == 4 and cell_type[cid2] == 4: #Cortex radial walls
            plasmod[wid] = cort_rad
        if cell_type[cid1] == 16 and cell_type[cid2] == 16: #Pericycle radial walls
            plasmod[wid] = peri_rad
        if cell_type[cid1] == 5 and cell_type[cid2] == 5: #Stele radial walls
            plasmod[wid] = ste_rad
            
        #Tangental walls
        if cell_type[cid1] == 4 and cell_type[cid2] == 3: #walls between endodermis/inner cortex and cortex/outer cortex
            plasmod[wid] = endo_cort_tang
        if cell_type[cid1] == 3 and cell_type[cid2] == 4: #walls between endodermis/inner cortex and cortex/outer cortex
            plasmod[wid] = endo_cort_tang
        if cell_type[cid1] == 2 and cell_type[cid2] == 4: #walls between (outer) cortex and epidermis
            plasmod[wid] = cort_epi_tang
        if cell_type[cid1] == 4 and cell_type[cid2] == 2: #walls between (outer) cortex and epidermis
            plasmod[wid] = cort_epi_tang
        if cell_type[cid1] == 3 and cell_type[cid2] == 16: #walls between endodermis and pericycle
            plasmod[wid] = peri_endo_tang
        if cell_type[cid1] == 16 and cell_type[cid2] == 3: #walls between endodermis and pericycle
            plasmod[wid] = peri_endo_tang
        if cell_type[cid1] == 16 and cell_type[cid2] == 5: #walls between pericycle and stele
            plasmod[wid] = peri_ste_tang
        if cell_type[cid1] == 5 and cell_type[cid2] == 16: #walls between pericycle and stele
            plasmod[wid] = peri_ste_tang  

    db.set_property('plasmodesmata', plasmod)
    db.set_description('plasmodesmata', '')
    db.get_property('species_desc')['plasmodesmata']='wall'
