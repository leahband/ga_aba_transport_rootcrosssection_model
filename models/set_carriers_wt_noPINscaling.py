from vroot.db_utilities import get_mesh,get_graph,get_wall_decomp,def_property,CellToCoordinates
from numpy import mean,array,median

def set_carriers(db):
    
    mesh=get_mesh(db)
    graph=get_graph(db)
    wall_decomposition=get_wall_decomp(db)
    cell_type=db.get_property('cell_type')
    pos=db.get_property('position')

    border_type = db.get_property('border')

    db,PIN1 = def_property(db, "PIN1", 0.0, "EDGE", "config", "")
    db,PIN2 = def_property(db, "PIN2", 0.0, "EDGE", "config", "")
    db,PIN3 = def_property(db, "PIN3", 0.0, "EDGE", "config", "")
    db,PIN4 = def_property(db, "PIN4", 0.0, "EDGE", "config", "")
    db,PIN7 = def_property(db, "PIN7", 0.0, "EDGE", "config", "")

    db,AUX1 = def_property(db, "AUX1", 0.0, "EDGE", "config", "")
    db,LAX = def_property(db, "LAX", 0.0, "EDGE", "config", "")

    for cid in mesh.wisps(2):
        for eid in graph.out_edges(cid):
            AUX1[eid]=0
            LAX[eid]=0

    V = db.get_property("V")
    S = db.get_property("S")
    wall = db.get_property("wall")

    # cell -> walls
    cell_to_walls = dict((cid,tuple(mesh.borders(2,cid))) for cid in mesh.wisps(2))
    # border -> coordinates dictionary
    border_to_coordinates = dict((wid,\
                            [[pos[tuple(mesh.borders(1,wid))[0]][0],\
                              pos[tuple(mesh.borders(1,wid))[0]][1]],\
                             [pos[tuple(mesh.borders(1,wid))[1]][0],\
                              pos[tuple(mesh.borders(1,wid))[1]][1]]]\
                            ) for wid in mesh.wisps(1))
    # cell -> coordinates           
    cell_to_coordinates = CellToCoordinates(mesh,cell_to_walls,border_to_coordinates)

    centroid=db.get_property('cell_centres')


    def sort_pair(a, b):
            if a<=b:
                    return (a,b)
            else:
                    return (b,a)

    from collections import defaultdict
    wall_groups = defaultdict(list)
    for wid in mesh.wisps(1):
            if mesh.nb_regions(1, wid) == 2:
                    cid1, cid2 = mesh.regions(1, wid)
                    wall_groups[sort_pair(cid1, cid2)].append(wid)


    wall_groups_centre= {}
    for (cid1, cid2), wl in wall_groups.items():
        pid_set = set()
        for wid in wl:
            pid_set = pid_set ^ set(mesh.borders(1, wid))
        pid0, pid1 = pid_set
        p0 = array(pos[pid0])
        p1 = array(pos[pid1])
        wall_groups_centre[(cid1,cid2)]=0.5*p0+0.5*p1


    LRC_TYPES = (6,7,8,9)
    QC = 17

    ND = 2



    QC_cells = [cid for cid, v in cell_type.items() if v == QC ]

    # Find QC position

    if len(QC_cells) == 1:
            
            QC_pos = centroid[QC_cells[0]]
            x_distalmeri = QC_pos[0]-40
            ycentreline = QC_pos[1]
    elif len(QC_cells) == 2:
            
            QC_pos = wall_groups_centre[sort_pair(*QC_cells)]
            x_distalmeri = QC_pos[0]-40
            ycentreline = QC_pos[1]
            
    elif len(QC_cells) == 3: #should we ever have 3 QC cells? anyway this should pick the central one
        medy=median([centroid[cid][1] for cid in QC_cells])
        QC_pos=[centroid[cid] for cid in QC_cells if centroid[cid][1]==medy][0]
        x_distalmeri = QC_pos[0]-40
        ycentreline = QC_pos[1]
    elif len(QC_cells) == 4: #should we ever have 4 QC cells?
        ypos={centroid[cid][1]:cid for cid in QC_cells}
        QC_cells.remove(ypos[min(ypos.keys())])
        QC_cells.remove(ypos[max(ypos.keys())])
        QC_pos = wall_groups_centre[sort_pair(*QC_cells)]
        x_distalmeri = QC_pos[0]-40
        ycentreline = QC_pos[1]
    # QC_pos = position of the centre of the QC
    # x_distalmeri = x-position of the boundary between the basal and  distal meristem
    # ycentreline = y-position of the QC centre - midline of the root

    # find most shootward LRC cell of each type
    most_shootward_upper = {}

    most_shootward_lower = {}
    for ct in (6, 7, 8, 9, 19):
            for cid in cell_type:
                    if cell_type[cid] == ct:
                            c = centroid[cid]
                            if c[1] < ycentreline and (ct not in most_shootward_lower or c[0] < centroid[most_shootward_lower[ct]][0]):
                                    most_shootward_lower[ct] = cid
                            if c[1] > ycentreline and (ct not in most_shootward_upper or c[0] < centroid[most_shootward_upper[ct]][0]):
                                    most_shootward_upper[ct] = cid

    most_rootward_upper = {}

    most_rootward_lower = {}
    for ct in (2, 9, 19):
            for cid in cell_type:
                    if cell_type[cid] == ct:
                            c = centroid[cid]
                            #if c[1] < ycentreline and (ct not in most_shootward_lower or c[0] > centroid[most_shootward_lower[ct]][0]):
                            #        most_rootward_lower[ct] = cid
                            #if c[1] > ycentreline and (ct not in most_shootward_upper or c[0] > centroid[most_shootward_upper[ct]][0]):
                            #        most_rootward_upper[ct] = cid


    def assign_LRC_end(carrier_dict, cid1, dest_types):
            for eid in graph.out_edges(cid1):
                    cid2 = graph.target(eid)
                    if cell_type[cid2] in dest_types:
                            wid = wall[eid]
                            pid1, pid2 = mesh.borders(1, wid)
                            if (0.5*(pos[pid1][0]+pos[pid2][0]))<centroid[cid1][0]:
                                    carrier_dict[eid] = 1.0

    #this is BAD - what if 'd[6] etc is empty???????
    #for d in (most_shootward_upper, most_shootward_lower):
    #        assign_LRC_end(PIN2, d[6], (2,7))
    #        assign_LRC_end(PIN2, d[7], (2,8)) # 8?
    #        assign_LRC_end(PIN2, d[8], (2,9)) # 8?
    #        if 9 in d:
    #            assign_LRC_end(PIN2, d[9], (2,))
    #        if 19 in d:
    #                assign_LRC_end(PIN2, d[19], (2,))


    # Find x-position of the elongation zone # this is what Nathan was discussing that may need changing

    #xEZ=max([min([centroid[cid][0] for cid in most_shootward_upper.values()]),min([centroid[cid][0] for cid in most_shootward_lower.values()])])

    #xEZ=QC_pos[0]-210.0 # *** approx mean of previously calculated values using LRC from untreated wt, pin2 and aux1
    
    db,zones = def_property(db, "zones", 0.0, "CELL", "config", "")
    
    #for cid,val in centroid.items():
    #    if val[0]<=QC_pos[0]:
    #        zones[cid]=1.0
    #    if val[0]<=x_distalmeri:
    #        zones[cid]=2.0
    #    if val[0]<=(QC_pos[0]+xEZ)/2:
    #        zones[cid]=3.0
    #    if val[0]<=xEZ+75.0:
    #        zones[cid]=4.0
    #    if val[0]<=xEZ:
    #        zones[cid]=5.0
    db.get_property('species_desc')['zones']='cell'
    """
    Summary of the rules
        1. Shootward PINs on epidermal cells where the source cell is rootward of x_distalmeri, otherwise rootward.
        Note that reference implementation isn't symmetric -> distance criterion is on cid1, rather than cid2
        2. PINs epidermis -> (columella, QC)    2->(10, 11, 12, 3, 17, 18) 
        3. Shootward PINs between LRC cells (LRC1, 2, 3, 4, 5) == (6, 7, 8, 9, 19)
        4. Shootward PINs between any two cortical cells in the EZ, rootward PINs between any two cortical cells in the meristem
        (cid1[0]<xEZ)
        5. Rootward PINs between any two endodermal cells:
        6. Add PINs to the rootward of the vasculature cells - version 2.
        7. PINs on the most shootwards vascular cell of each LRC file - LRC1->6 


        8. Put PINs on the cortical-endodermal initials, facing the QC:
        Put PINs on the final endodermal and cortical cells facing the QC or columella cells (i.e. if there are no C-E initials):
        18->17, (3 or 4) -> (10 or 17 or 18)
        9. Put PINs on all membranes of the QC and columella cells.
        (17, 10, 11, 12, 13, 14, 15) -> any
        10. Put AUX1 on the epidermal and lateral root cap and S1-S3    (6, 7, 8, 9, 11, 12, 13, 19) -> any
        11. AUX1 elongating epidermis  cell_type[cell]==2 and centroid[cell][0]<xEZ+75: -> any
        12. LAX - (10, 12, 17) -> any
            LAX - 5 if centroid[0]> (QC_pos+xEZ)/2
    """

    def apply_simple(carrier_dict, rule, value=1.0):
            for eid in graph.edges():
                    cid1 = graph.source(eid)
                    cid2 = graph.target(eid)
                    if rule(cid1, cid2):
                            carrier_dict[eid] = value

    def rootwards(cid1, cid2):
            return centroid[cid1][0] < centroid[cid2][0]

    def shootwards(cid1, cid2):
            return centroid[cid1][0] > centroid[cid2][0]

    
    #rule1
    #apply_simple(PIN2, lambda cid1, cid2: ( cell_type[cid1]==cell_type[cid2]==2
                                            #and ( centroid[cid2][0] <= x_distalmeri and shootwards(cid1, cid2)) ))

    #apply_simple(PIN4, lambda cid1, cid2: ( cell_type[cid1]==cell_type[cid2]==2 and 
                                             #( ( centroid[cid1][0] > x_distalmeri and rootwards(cid1, cid2)))))
    #could we recalculate x_distalmeri based on (lower) LRC3 position????
    
    
    #rule 2
    #apply_simple(PIN2, lambda cid1, cid2: cell_type[cid1]==2 and cell_type[cid2] in (3, 10, 11, 12, 17, 18))
    apply_simple(PIN4, lambda cid1, cid2: cell_type[cid1]==2 and cell_type[cid2] in (3, 10, 11, 12, 17, 18))
    
    #rule 3
    # original rule
    apply_simple(PIN2, lambda cid1, cid2: (cell_type[cid1] in (6,7,8,9,19)
                                                    and cell_type[cid1]==cell_type[cid2]
                                                    and shootwards(cid1, cid2) ) )
    '''
    # added rules for more PIN in LRC region (added by Heather)
    #apply_simple(PIN3, lambda cid1, cid2: (cell_type[cid1] in (6,7,8,9,19)
    #                                                and cell_type[cid1]==cell_type[cid2]
    #                                                and shootwards(cid1, cid2) ) )
    #apply_simple(PIN4, lambda cid1, cid2: (cell_type[cid1] in (6,7,8,9,19)
    #                                                and cell_type[cid1]==cell_type[cid2]
    #                                                and shootwards(cid1, cid2) ) )
    '''
    
    #rule 4
    #apply_simple(PIN2, lambda cid1, cid2: ( cell_type[cid1]==cell_type[cid2]==4 and 
    #                                         ( ( centroids[cid1][0] > xEZ and shootwards(cid1, cid2)) 
    #                                           or ( centroids[cid2][0] <= xEZ and rootwards(cid1, cid2)) )))

    #apply_simple(PIN2, lambda cid1, cid2: ( cell_type[cid1]==cell_type[cid2]==4 and 
                                             #( ( centroid[cid1][0] > xEZ and rootwards(cid1, cid2)) 
                                               #or ( centroid[cid2][0] <= xEZ and shootwards(cid1, cid2)) )))

    #apply_simple(PIN4, lambda cid1, cid2: (cell_type[cid1]==cell_type[cid2]==4 and rootwards(cid1, cid2) and centroid[cid1][0] > x_distalmeri))
    
    
    # rule 5
    apply_simple(PIN1, lambda cid1, cid2: (cell_type[cid1]==cell_type[cid2]==3 and rootwards(cid1, cid2) ))

    #apply_simple(PIN4, lambda cid1, cid2: (cell_type[cid1]==cell_type[cid2]==3 and rootwards(cid1, cid2) and centroid[cid1][0] > x_distalmeri))

    apply_simple(PIN3, lambda cid1, cid2: (cell_type[cid1]==cell_type[cid2]==3 and rootwards(cid1, cid2) ))
    
    # rule 6
    for cid1 in cell_type:
            if cell_type[cid1] in [5,16,25]:
                    nb_set = set(cid3 for wid3 in mesh.borders(2, cid1) for cid3 in mesh.regions(1, wid3)  if cid3!=cid1)
                    max_wall_x = max(wall_groups_centre[sort_pair(cid1, cid2)][0] for cid2 in nb_set)
                    for cid2 in nb_set:
                            k = sort_pair(cid1, cid2)
                            if wall_groups_centre[k][0] > max_wall_x - 0.75:
                                    for eid in graph.out_edges(cid1):
                                            if wall[eid] in wall_groups[k]:
                                                    PIN1[eid] = 1.0
                                                    PIN3[eid] = 1.0 # note also endodermis within the EZ
                                                    PIN7[eid] = 1.0 # also in PX but not endodermis
                                                    if centroid[cid1][0] >x_distalmeri:
                                                            PIN4[eid] = 1.0


    
    #rule 7



    #for d in (most_rootward_upper, most_rootward_lower):
    #        for cid in (d[2], d[9]):
    #                for eid in graph.out_edges(cid):
    #                        PIN3[eid]=0.1
                    
    
    # rule 8
    apply_simple(PIN3, lambda cid1, cid2: ( (cell_type[cid1]==18 and cell_type[cid2]==17) 
                                             or (cell_type[cid1] in (3,4) and cell_type[cid2] in (10, 17, 18) )))

    apply_simple(PIN4, lambda cid1, cid2: ( (cell_type[cid1]==18 and cell_type[cid2]==17) 
                                             or (cell_type[cid1] in (3,4) and cell_type[cid2] in (10, 17, 18) )))


    

    # rule 9
    # original based on Leah's data
    apply_simple(PIN3, lambda cid1, cid2: cell_type[cid1] in (10, 11, 12))
    apply_simple(PIN4, lambda cid1, cid2: cell_type[cid1] in (10, 11, 12))
    apply_simple(PIN4, lambda cid1, cid2: cell_type[cid1] in (17,18))

    '''
    # more general in all columella cells (added by Heather)
    #apply_simple(PIN3, lambda cid1, cid2: cell_type[cid1] in (10, 11, 12, 13, 14, 15))
    #apply_simple(PIN4, lambda cid1, cid2: cell_type[cid1] in (10, 11, 12, 13, 14, 15))
    '''

    # rule 10
    #apply_simple(AUX1, lambda cid1, cid2: cell_type[cid1] in (6, 7, 8, 9, 11, 12, 13, 19))
    apply_simple(AUX1, lambda cid1, cid2: cell_type[cid1] in (2,16))
    print(cell_type[cid])

    
    # rule 11
    #apply_simple(AUX1, lambda cid1, cid2: cell_type[cid1]==2 and centroid[cid1][0]<xEZ+75)

    # rule 12
    apply_simple(LAX, lambda cid1, cid2: cell_type[cid1] in (10, 12,17))
    apply_simple(LAX, lambda cid1, cid2: cell_type[cid1] in [5,16,25] and centroid[cid1][0]>0.5*(xEZ+QC_pos[0]))
    
    #rule 13
    apply_simple(PIN3, lambda cid1, cid2: cell_type[cid1]==3 and cell_type[cid2]==16) # changed 5 to 16 (pericycle added as cell type 16)
    

    #rule 14 - added NM 4/9/17
    #apply_simple(AUX1, lambda cid1, cid2: cell_type[cid1]==4 and centroid[cid1][0]<xEZ)
    
    
    # Create combined PIN dictionary (useful for existing code)
    db,PIN = def_property(db, "PIN", 0.0, "EDGE", "config","")
    for eid in graph.edges():


            PIN[eid] = sum([PIN1[eid],PIN2[eid],PIN3[eid],PIN4[eid],PIN7[eid]])

    for n in ['PIN1', 'PIN2', 'PIN3', 'PIN4', 'PIN7', 'PIN', 'AUX1', 'LAX']:
            v = locals()[n]
            db.set_property(n, v)
            db.set_description(n, '')
            db.get_property('species_desc')[n]='edge'

    return db
