#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 16 14:55:59 2022

@author: user
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 16 14:34:36 2021

@author: user
"""

from vroot.db_utilities import get_mesh,get_graph,get_wall_decomp,def_property,CellToCoordinates
from numpy import mean,array,median

def set_carriers(db):
    
    mesh=get_mesh(db)
    graph=get_graph(db)
    wall_decomposition=get_wall_decomp(db)
    cell_type=db.get_property('cell_type')
    pos=db.get_property('position')

    border_type = db.get_property('border')

    db,Expvac = def_property(db, "Expvac", 0.0, "CELL", "config", "")
    #db,PIN1 = def_property(db, "PIN1", 0.0, "EDGE", "config", "")
    #db,PIN2 = def_property(db, "PIN2", 0.0, "EDGE", "config", "")
    #db,PIN3 = def_property(db, "PIN3", 0.0, "EDGE", "config", "")
    #db,PIN4 = def_property(db, "PIN4", 0.0, "EDGE", "config", "")
    #db,PIN7 = def_property(db, "PIN7", 0.0, "EDGE", "config", "")

    db,Inf1 = def_property(db, "Inf1", 0.0, "EDGE", "config", "")
    db,Inf2 = def_property(db, "Inf2", 0.0, "EDGE", "config", "")

    for cid in mesh.wisps(2):
        for eid in graph.out_edges(cid):
            Inf1[eid]=0
            Inf2[eid]=0

    V = db.get_property("V")
    S = db.get_property("S")
    wall = db.get_property("wall")

    # cell -> walls
    cell_to_walls = dict((cid,tuple(mesh.borders(2,cid))) for cid in mesh.wisps(2))
    # border -> coordinates dictionary
    border_to_coordinates = dict((wid,\
                            [[pos[tuple(mesh.borders(1,wid))[0]][0],\
                              pos[tuple(mesh.borders(1,wid))[0]][1]],\
                             [pos[tuple(mesh.borders(1,wid))[1]][0],\
                              pos[tuple(mesh.borders(1,wid))[1]][1]]]\
                            ) for wid in mesh.wisps(1))
    # cell -> coordinates           
    cell_to_coordinates = CellToCoordinates(mesh,cell_to_walls,border_to_coordinates)

    centroid=db.get_property('cell_centres')


    def sort_pair(a, b):
            if a<=b:
                    return (a,b)
            else:
                    return (b,a)

    from collections import defaultdict
    wall_groups = defaultdict(list)
    for wid in mesh.wisps(1):
            if mesh.nb_regions(1, wid) == 2:
                    cid1, cid2 = mesh.regions(1, wid)
                    wall_groups[sort_pair(cid1, cid2)].append(wid)


    wall_groups_centre= {}
    for (cid1, cid2), wl in wall_groups.items():
        pid_set = set()
        for wid in wl:
            pid_set = pid_set ^ set(mesh.borders(1, wid))
        pid0, pid1 = pid_set
        p0 = array(pos[pid0])
        p1 = array(pos[pid1])
        wall_groups_centre[(cid1,cid2)]=0.5*p0+0.5*p1
        
    """
    Summary of the rules
        1. Put Expvac on the tonoplast in Endodermis and/or Pericycle (2 and/or 3)
        2. Put Inf1 on the Endodermis and/or Pericycle (2 and/or 3)
        3. Put Inf2 on the Endodermis and/or Pericycle (2 and/or 3)
    """

    def apply_simple(carrier_dict, rule, value=1.0):
            for eid in graph.edges():
                    cid1 = graph.source(eid)
                    cid2 = graph.target(eid)
                    if rule(cid1, cid2):
                            carrier_dict[eid] = value

    def apply_simple1(carrier_dict, rule, value=1.0):
            for cid in mesh.wisps(2):
                    if rule(cid):
                            carrier_dict[cid] = value
    
    # Rule 1

    apply_simple1(Expvac, lambda cid: (cell_type[cid]==16))
    
    # Rule 2
    
    apply_simple(Inf1, lambda cid1, cid2: (cell_type[cid1]==3)) #in (3,16))) 
    #or (cell_type[cid2] in (2,3)))
    
    # Rule 3
    
    apply_simple(Inf2, lambda cid1, cid2: (cell_type[cid1]==16)) #in (3,16)))
    #or (cell_type[cid2] in (2,3)))
    
    # Create combined Expvac dictionary (useful for existing code)
    #db,Expvac = def_property(db, "Expvac", 0.0, "CELL", "config","")
    #for eid in graph.edges():


    #        Expvac[eid] = sum([Expvac1[eid],Expvac2[eid],Expvac3[eid],expvac4[eid],Expvac7[eid]])

    #for n in ['Expvac1', 'Expvac2', 'Expvac3', 'Expvac4', 'Expvac7', 'Expvac', 'Inf1', 'Inf2']:
    #        v = locals()[n]
    #        db.set_property(n, v)
    #        db.set_description(n, '')
    #        db.get_property('species_desc')[n]='edge'

    #return db