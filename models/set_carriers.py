from vroot.db_utilities import get_mesh,get_graph,get_wall_decomp,def_property,CellToCoordinates
from numpy import mean,array,median
from collections import defaultdict

def set_zones(db,growth_dim):
    # set zones for a tissue in given data base in given dimension
    # i.e. if root oriented left to right (shoot to root) dimension is 0 (x direction)
    # or if root is oriented top to bottom (shoot to root) dimension is 1 (y direction)
    # we assume growth is in positive direction
    # zone 0 : tip (below QC)
    # zone 1 : distal meristem (?). LAX in stele, PIN4 rootward in epidermis
    # zone 2 : 'early' proximal meristem. LAX in stele
    # zone 3 : 'mid' proximal meristem. No influx carriers in stele or outer tissues
    # zone 4 : 'late' proximal meristem. AUX1 expressed in epidermis
    # zone 5 : Elongation zone. AUX1 expressed in cortex and epidermis
    
    #the following parameters could/should vary between species/genotypes
    distal_meristem_length=40.0 #um. Length of distal meristem (Band et. al. 2014)
    MZ_epidermal_AUX1=75.0 # um. Length of MZ with AUX1 expression in epidermis (Band et. al. 2014)
    
    cell_centres=db.get_property('cell_centres')
    
    db,zones = def_property(db, "zones", 0.0, "CELL", "config", "")
    
    assert growth_dim in [0,1]
    if growth_dim==0:
        width_dim=1
    else:
        width_dim=0
    
    QC_pos = find_QC_centre(db,width_dim)
    EZstart = find_EZ_start(db,growth_dim)
    
    for cid,val in cell_centres.items():
        if val[growth_dim]<=QC_pos[growth_dim]:
            zones[cid]=1.0
        if val[growth_dim]<=QC_pos[growth_dim]-distal_meristem_length:
            zones[cid]=2.0
        if val[growth_dim]<=(QC_pos[growth_dim]+EZstart)/2:
            zones[cid]=3.0
        if val[growth_dim]<=EZstart+MZ_epidermal_AUX1:
            zones[cid]=4.0
        if val[growth_dim]<=EZstart:
            zones[cid]=5.0
    db.get_property('species_desc')['zones']='cell'

    return [QC_pos[growth_dim],QC_pos[growth_dim]-distal_meristem_length,(QC_pos[growth_dim]+EZstart)/2,EZstart+MZ_epidermal_AUX1,EZstart]

def find_QC_centre(db,width_dim):
    # find co-ordinates of centre of quiescent centre (assumed to be cell type 17)
    QC = 17
    
    cell_type=db.get_property('cell_type')
    cell_centres=db.get_property('cell_centres')
    wall_groups,wall_groups_centre=get_wall_groups_centres(db)

    QC_cells = [cid for cid,val in cell_type.items() if val == QC ]

    # Find QC position
    # should usually be just 2 QC cells defined but some cellset have 1,3 or 4
    # the following should pick out the middle 1 or 2 cells, then use either cell centre
    # (1 cell case) or the middle of the dividing walls (2 cell case)
    while len(QC_cells)>2:
        ypos={cell_centres[cid][width_dim]:cid for cid in QC_cells}
        QC_cells.remove(ypos[min(ypos.keys())])
        QC_cells.remove(ypos[max(ypos.keys())])
    if len(QC_cells) == 1:
        QC_pos = cell_centres[QC_cells[0]]
    if len(QC_cells) == 2:  
        QC_pos = wall_groups_centre[sort_pair(*QC_cells)]

    return QC_pos

def find_EZ_start(db,growth_dim):
    #find co-ordinate in growth dimension of start of elongation zone.
    #assumed to be at the centre of the last lateral root cap cell, nearer to tip
    #i.e. of the pair of last cells in a shootward direction, we pick the one nearer the tip
    
    LRC_types=[6,7,8,9]    
    
    cell_type=db.get_property('cell_type')
    cell_centres=db.get_property('cell_centres')
    
    assert growth_dim in [0,1]
    if growth_dim==0:
        width_dim=1
    else:
        width_dim=0
        
    centrevalue=find_QC_centre(db,width_dim)[width_dim]
    
    #this find the centroid of the LRC cell on the upper and lower sides of the root furthest from the tip (assuming growth is in positive direction)
    LRC_upper=min([cell_centres[cid][growth_dim] for cid,val in cell_type.items() if val in LRC_types and cell_centres[cid][width_dim]>centrevalue])
    LRC_lower=min([cell_centres[cid][growth_dim] for cid,val in cell_type.items() if val in LRC_types and cell_centres[cid][width_dim]<centrevalue])
    
    #then the EZ starts at the end of the LRC nearest the tip
    return max([LRC_upper,LRC_lower])
    
def set_carriers(db,growth_dim):
    
    mesh=get_mesh(db)
    graph=get_graph(db)
    wall_decomposition=get_wall_decomp(db)
    cell_type=db.get_property('cell_type')
    pos=db.get_property('position')

    border_type = db.get_property('border')
    
    #zone pos will be positions of (in growth dimension)[QC,distal meristem (swith in PIN2/PIN4 expression/direction,end of LAX expression in stele,start of epidermal AUX1,start of elongation zone]
    zonepos=set_zones(db,growth_dim)
    x_distalmeri=zonepos[1]
    xEZ=zonepos[4]

    db,PIN1 = def_property(db, "PIN1", 0.0, "EDGE", "config", "")
    db,PIN2 = def_property(db, "PIN2", 0.0, "EDGE", "config", "")
    db,PIN3 = def_property(db, "PIN3", 0.0, "EDGE", "config", "")
    db,PIN4 = def_property(db, "PIN4", 0.0, "EDGE", "config", "")
    db,PIN7 = def_property(db, "PIN7", 0.0, "EDGE", "config", "")
    db,AUX1 = def_property(db, "AUX1", 0.0, "EDGE", "config", "")
    db,LAX = def_property(db, "LAX", 0.0, "EDGE", "config", "")


    V = db.get_property("V")
    S = db.get_property("S")
    wall = db.get_property("wall")


    cell_centres=db.get_property('cell_centres')

    if growth_dim==0:
        width_dim=1
    else:
        width_dim=0


    """
    Summary of the rules
        1. Shootward PIN2/4 on epidermal cells where the source cell is shootward of x_distalmeri (PIN2), otherwise rootward (PIN4).
        Note that reference implementation isn't symmetric -> distance criterion is on cid1, rather than cid2
        2. PIN4 epidermis -> (columella, QC)    2->(10, 11, 12, 17, 18) 
        3. Shootward PIN2 between LRC cells (LRC1, 2, 3, 4) == (6, 7, 8, 9)
        4. Shootward PIN2 between any two cortical cells in the EZ, rootward PIN2 between any two cortical cells in the meristem
        (cid1[0]<xEZ). Rootward PIN4 in cortical cells in the distal meristem
        5. Rootward PINs between any two endodermal cells: PIN1 and PIN3 throughout, PIN4 in distal meristem only
        6. Add PINs to the rootward of the vasculature cells - version 2.
        7. PINs on the most shootwards vascular cell of each LRC file - LRC1->6 
        8. Put PIN3/4 on the cortical-endodermal initials, facing the QC:
        Put PIN3/4 on the final endodermal and cortical cells facing the QC or columella cells (i.e. if there are no C-E initials):
        18->17, (3 or 4) -> (10 or 17 or 18)
        9. Put PINs on all membranes of the QC and columella cells.
        (17, 10, 11, 12, 13, 14, 15) -> any
        10. Put AUX1 on the epidermal and lateral root cap and S1-S3    (6, 7, 8, 9, 11, 12, 13, 19) -> any
        11. AUX1 elongating epidermis  cell_type[cell]==2 and centroid[cell][0]<xEZ+75: -> any
        12. LAX - (10, 12, 17) -> any
            LAX - 5 if centroid[0]> (QC_pos+xEZ)/2
        13. PIN3 expression from endodermis to pericycle
        14. AUX1 expression in the cortical EZ
    """
    
    def apply_simple(carrier_dict, rule, value=1.0):
        for eid in graph.edges():
            cid1 = graph.source(eid)
            cid2 = graph.target(eid)
            if rule(cid1, cid2):
                carrier_dict[eid] = value

    def rootwards(cid1, cid2):
        return cell_centres[cid1][growth_dim] < cell_centres[cid2][growth_dim]

    def shootwards(cid1, cid2):
        return cell_centres[cid1][growth_dim] > cell_centres[cid2][growth_dim]

    
    #rule1
    apply_simple(PIN2, lambda cid1, cid2: ( cell_type[cid1]==cell_type[cid2]==2 and ( cell_centres[cid2][growth_dim] <= x_distalmeri and shootwards(cid1, cid2)) ))
    apply_simple(PIN4, lambda cid1, cid2: ( cell_type[cid1]==cell_type[cid2]==2 and ( cell_centres[cid1][growth_dim] > x_distalmeri and rootwards(cid1, cid2)) ))
    
    #rule 2
    apply_simple(PIN4, lambda cid1, cid2: cell_type[cid1]==2 and cell_type[cid2] in (10, 11, 12, 17, 18))
    
    
    #rule 3
    apply_simple(PIN2, lambda cid1, cid2: (cell_type[cid1] in (6,7,8,9) and cell_type[cid1]==cell_type[cid2] and shootwards(cid1, cid2) ) )
    
    
    #rule 4
    apply_simple(PIN2, lambda cid1, cid2: ( cell_type[cid1]==cell_type[cid2]==4 and 
                                             ( ( cell_centres[cid1][growth_dim] > xEZ and rootwards(cid1, cid2)) 
                                               or ( cell_centres[cid2][growth_dim] <= xEZ and shootwards(cid1, cid2)) )))

    apply_simple(PIN4, lambda cid1, cid2: (cell_type[cid1]==cell_type[cid2]==4 and rootwards(cid1, cid2) and cell_centres[cid1][growth_dim] > x_distalmeri))

    
    # rule 5
    apply_simple(PIN1, lambda cid1, cid2: (cell_type[cid1]==cell_type[cid2]==3 and rootwards(cid1, cid2) ))

    apply_simple(PIN4, lambda cid1, cid2: (cell_type[cid1]==cell_type[cid2]==3 and rootwards(cid1, cid2) and cell_centres[cid1][growth_dim] > x_distalmeri))

    apply_simple(PIN3, lambda cid1, cid2: (cell_type[cid1]==cell_type[cid2]==3 and rootwards(cid1, cid2) ))
    
    # rule 6
    for cid1 in cell_type:
        if cell_type[cid1] in [5,16]:
            nb_set = set(cid3 for wid3 in mesh.borders(2, cid1) for cid3 in mesh.regions(1, wid3)  if cid3!=cid1)
            max_wall_x = max(wall_groups_centre[sort_pair(cid1, cid2)][growth_dim] for cid2 in nb_set)
            for cid2 in nb_set:
                k = sort_pair(cid1, cid2)
                if wall_groups_centre[k][growth_dim] > max_wall_x - 0.75: # not sure what this '0.75' is???
                    for eid in graph.out_edges(cid1):
                        if wall[eid] in wall_groups[k]:
                            PIN1[eid] = 1.0
                            PIN3[eid] = 1.0 # note also endodermis within the EZ
                            PIN7[eid] = 1.0 # also in PX but not endodermis
                            if centroid[cid1][growth_dim] >x_distalmeri:
                                PIN4[eid] = 1.0

    #rule 7
    QC_pos=find_QC_centre(db,width_dim)
    LRC_types=[6,7,8,9]

    # find most shootward LRC cell of each type
    def assign_LRC_end(carrier_dict, xcid):
       for eid in graph.out_edges(xcid):
            cid2 = graph.target(eid)
            if (cell_type[cid2]>cell_type[xcid] and cell_type[cid2] in LRC_types) or cell_type[cid2]==2:
                wid = wall[eid]
                pid1, pid2 = mesh.borders(1, wid)
                if (0.5*(pos[pid1][growth_dim]+pos[pid2][growth_dim]))<cell_centres[xcid][growth_dim]:
                    carrier_dict[eid] = 1.0

    for ct in LRC_types:
        plus= {cell_centres[cid][growth_dim]:cid for cid,val in cell_type.items() if val==ct and cell_centres[cid][width_dim]>QC_pos[width_dim]}
        minus={cell_centres[cid][growth_dim]:cid for cid,val in cell_type.items() if val==ct and cell_centres[cid][width_dim]<QC_pos[width_dim]}
        if len(plus)>0:
            assign_LRC_end(PIN2, plus[min(plus.keys())])
        if len(minus)>0:
            assign_LRC_end(PIN2, minus[min(minus.keys())])
    
    # rule 8
    apply_simple(PIN3, lambda cid1, cid2: ( (cell_type[cid1]==18 and cell_type[cid2]==17) 
                                             or (cell_type[cid1] in (3,4) and cell_type[cid2] in (10, 17, 18) )))
    apply_simple(PIN4, lambda cid1, cid2: ( (cell_type[cid1]==18 and cell_type[cid2]==17) 
                                             or (cell_type[cid1] in (3,4) and cell_type[cid2] in (10, 17, 18) )))

    # rule 9
    apply_simple(PIN3, lambda cid1, cid2: cell_type[cid1] in (10, 11, 12))
    apply_simple(PIN4, lambda cid1, cid2: cell_type[cid1] in (10, 11, 12))
    apply_simple(PIN4, lambda cid1, cid2: cell_type[cid1] in (17,18))
    
    
    # rule 10
    #apply_simple(AUX1, lambda cid1, cid2: cell_type[cid1] in (6, 7, 8, 9, 11, 12, 13, 19))

    #myrule 10
    #apply_simple(AUX1, lambda cid1, cid2: cell_type[cid1] in (3, 5, 16))
    apply_simple(AUX1, lambda cid1, cid2: cell_type[cid1] in (2, 3))

    # rule 11
    #apply_simple(AUX1, lambda cid1, cid2: cell_type[cid1]==2 and centroid[cid1][growth_dim]<zonepos[3])

    # rule 12
    apply_simple(LAX, lambda cid1, cid2: cell_type[cid1] in (10, 12,17))
    apply_simple(LAX, lambda cid1, cid2: cell_type[cid1] in [5,16,25] and centroid[cid1][growth_dim]>zonepos[2])
    
    #rule 13
    apply_simple(PIN3, lambda cid1, cid2: cell_type[cid1]==3 and cell_type[cid2]==16) # changed 5 to 16 (pericycle added as cell type 16)
    

    #rule 14
    #apply_simple(AUX1, lambda cid1, cid2: cell_type[cid1]==4 and centroid[cid1][0]<xEZ)
    
    
    # Create combined PIN dictionary (useful for existing code)
    db,PIN = def_property(db, "PIN", 0.0, "EDGE", "config","")
    for eid in graph.edges():


            PIN[eid] = sum([PIN1[eid],PIN2[eid],PIN3[eid],PIN4[eid],PIN7[eid]])

    for n in ['PIN1', 'PIN2', 'PIN3', 'PIN4', 'PIN7', 'PIN', 'AUX1', 'LAX']:
            v = locals()[n]
            db.set_property(n, v)
            db.set_description(n, '')
            db.get_property('species_desc')[n]='edge'
    
    return db

def get_wall_groups_centres(db):
    
    mesh=get_mesh(db)
    pos=db.get_property('position')
    
    # cell -> walls
    cell_to_walls = dict((cid,tuple(mesh.borders(2,cid))) for cid in mesh.wisps(2))
    # border -> coordinates dictionary
    border_to_coordinates = dict((wid,\
                            [[pos[tuple(mesh.borders(1,wid))[0]][0],\
                              pos[tuple(mesh.borders(1,wid))[0]][1]],\
                             [pos[tuple(mesh.borders(1,wid))[1]][0],\
                              pos[tuple(mesh.borders(1,wid))[1]][1]]]\
                            ) for wid in mesh.wisps(1))
    # cell -> coordinates           
    cell_to_coordinates = CellToCoordinates(mesh,cell_to_walls,border_to_coordinates)


    wall_groups = defaultdict(list)
    for wid in mesh.wisps(1):
        if mesh.nb_regions(1, wid) == 2:
            cid1, cid2 = mesh.regions(1, wid)
            wall_groups[sort_pair(cid1, cid2)].append(wid)

    wall_groups_centre= {}
    for (cid1, cid2), wl in wall_groups.items():
        pid_set = set()
        for wid in wl:
            pid_set = pid_set ^ set(mesh.borders(1, wid))
        pid0, pid1 = pid_set
        p0 = array(pos[pid0])
        p1 = array(pos[pid1])
        wall_groups_centre[(cid1,cid2)]=0.5*p0+0.5*p1

    return wall_groups,wall_groups_centre

def sort_pair(a, b):
        if a<=b:
                return (a,b)
        else:
                return (b,a)
