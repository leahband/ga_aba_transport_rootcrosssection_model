#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 25 00:02:14 2022

@author: user
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 17 03:08:25 2022

@author: user
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 13 21:23:45 2021

@author: user
"""

from numpy import zeros
from math import exp
from vroot.db_utilities import get_mesh, get_graph, get_wall_decomp, get_tissue_maps, set_cell_dependent_parameter
from numpy import size, sqrt,pi,mean
from vroot.biochemicalgeometry1 import model_setup
from models.set_plasmodesmataradial1geometry import set_plasmodesmata
from scipy.sparse.linalg import spsolve

from scipy.sparse import lil_matrix

class GAtransport_ss(object):
    """

    Implements AbstractModel
    """

    def get_species(self):
        return [('GA_vac','cell'),('GA', 'cell'),('GA_wall','wall')]

    def get_steady_species(self):
        return [('Expvac','cell'),('Inf1','edge'),('Inf2','edge'),('GA', 'cell'),('GA_wall','wall'),('GA_vac','cell'),
                ('GA_point','point'),('plas_flux','wall'),('carrier_flux','wall'),('combined_flux','wall'),
                ('Inf1Inf2_flux','wall'),('Expvac_flux','wall'),('passive_flux','wall')]

    def set_default_parameters(self, db):
        p={}

        # Parameters governing GA transport
        p['pHapo']=5.3
        p['pHcyt']=7.0
        p['pK']=4.2#6.4
        p['pHvac']=5.5
        p['Voltage']=-0.120
        p['Voltagevac']=-0.03
        p['Temp']=300
        p['Rconst']=8.31
        p['Fdconst']=96500
        p['phi']=p['Fdconst']*p['Voltage']/(p['Rconst']*p['Temp'])
        p['phivac']=p['Fdconst']*p['Voltagevac']/(p['Rconst']*p['Temp'])
        # Parameters governing GA transport
        p['A1']=1/(1+pow(10,p['pHapo']-p['pK'])) # A1, A2, A3, B1, B2, B3 are dimensionless
        p['A2']=(1-p['A1'])*p['phi']/(exp(p['phi'])-1)
        p['A3']=-(1-p['A1'])*p['phi']/(exp(-p['phi'])-1)

        p['B1']=1/(1+pow(10,p['pHcyt']-p['pK']))
        p['B2']=-(1-p['B1'])*p['phi']/(exp(-p['phi'])-1)
        p['B3']=(1-p['B1'])*p['phivac']/(exp(p['phivac'])-1)
        p['B31']=(1-p['B1'])*p['phi']/(exp(p['phi'])-1)
        p['B32']=-(1-p['B1'])*p['phivac']/(exp(-p['phivac'])-1)
        
        p['C1']=1/(1+pow(10,p['pHvac']-p['pK']))
        p['C3']=-(1-p['C1'])*p['phivac']/(exp(-p['phivac'])-1)
        p['C31']=(1-p['C1'])*p['phivac']/(exp(p['phivac'])-1)
        print(p['A1'])
        print(p['A2'])
        print(p['B1'])
        print(p['B2'])
        print(p['C1'])
        print(p['C3'])
        print(p['B3'])
        print(p['B32'])
        print(p['C31'])
        
        p['vac_percentage']=0.9


        p['D_cw']= 32.0    # Diffusion rate in cell wall (microns^2 sec^(-1)) from Kramer 2007.


        # The permeability coefficients 
        p['PIAAH']=  0.333#0.0222 # Diffusion coefficient of IAAH (micrometers.sec-1)
        p['PInf1']=0.139#0.017 #0.02 # Permeability of anionic GA due to Inf1 active influx carriers (for an Inf1 concentration equal to 1). (micrometers.sec-1)
        p['PInf2']= 0.0097 # Permeability of anionic GA due to Inf2 active influx carriers (for an Inf2 concentration equal to 1). (micrometers.sec-1)
        p['PExpvac']=  0.556  # Permeability of anionic GA due to active efflux carriers on vacuole (for a Expvac concentration equal to 1).   (micrometers.sec-1)    
        p['Pback']= 0 # Background efflux permeability
        p['Pplas'] = 0*8.0/9.92 # Permeability of Plasmodesmata (micrometers.sec-1) 8 in stele (Rustchow et al, 2011) / Plasmodesmata density in transvere wall of Stele
        p['PImpvac'] = 0
        
        '''
        # Expvac type specific permeabilities
        p['PExpvac1'] = 0.304
        p['PExpvac2'] = 0.123
        p['PExpvac3'] = 2.006
        p['PExpvac4'] = 2.006
        p['PExpvac7'] = 2.006
        '''
        
        p['scalefactor']=50/470 # original mesh in pixels, 1 pixel = 0.04 microns.

        GA_vacinit= 0#  Initially set GA vacuolar concentrations to be small.

        # GA production and degradation: 
        p['dGA']=0.000003#0.00000278
        p['pGA']=('cell_type',{5:0.001,'else':0})#('cell_type',{1:0.00000009,0:0,'else':0.00000018})
        
        #exogenous GA
        p['outer_concentration']=0.0
        
        print('Steady state passive diffusion = {}'.format(p['A1']*p['outer_concentration']/p['B1']))
        print('Steady state passive diffusion in vacuole = {}'.format(p['A1']*p['outer_concentration']/p['C1']))
        print('Steady state passive diffusion + importer = {}'.format((p['PIAAH']*p['A1']+p['PInf1']*p['A2'])*p['outer_concentration']/(p['PIAAH']*p['B1']+p['PInf1']*p['B2'])))
        print('Steady state passive diffusion + importer + exporter in vacuole = {}'.format((p['PIAAH']*p['B1']+p['PExpvac']*p['B3'])*(p['PIAAH']*p['A1']+p['PInf1']*p['A2'])*p['outer_concentration']/((p['PIAAH']*p['B1']+p['PInf1']*p['B2'])*(p['PIAAH']*p['C1']+p['PExpvac']*p['C3']))))
        

        self.fixed={'GA':[('cid in border.keys() and cell_type[cid] in [2,3,4,16]',0.0)]}
        #self.fixed={}
        set_plasmodesmata(db)
        self.p=p
        model_setup(self,db)



    def deriv(self, t, y, db, tissue_maps, ssl, wall_decomposition, fixed_idx):

        ret = zeros((len(y),))

        return ret

    def set_ss(self, db, tissue_maps, fixed_idx,ssl):

        p=self.p
        
        
        A1PI=p['A1']*p['PIAAH']
        A2PI1=p['A2']*p['PInf1']
        A2PI2=p['A2']*p['PInf2']
        A3Pb=p['A3']*p['Pback']
        B1PI=p['B1']*p['PIAAH']
        B2PI1=p['B2']*p['PInf1']
        B2PI2=p['B2']*p['PInf2']
        B3Pb=p['B31']*p['Pback']
        Ppla=p['Pplas']
        C1PI=p['C1']*p['PIAAH']
        B3PEv=p['B3']*p['PExpvac']
        B3Piv=p['B32']*p['PImpvac']
        C3PEv=p['C3']*p['PExpvac']
        C3Piv=p['C31']*p['PImpvac']
        
        pGA=db.get_property('pGA')

        Sp = db.get_property('S')
        Vp = db.get_property('V')
        wall_width=db.get_property('wall_width')
        graph=get_graph(db)
        mesh = get_mesh(db)
        
        S={}
        V={}
        Sv={}
        Vv={}
        
        cell_to_walls = dict((cid,tuple(mesh.borders(2,cid))) for cid in mesh.wisps(2))
        
        for cid in mesh.wisps(2):
            V[cid]=p['scalefactor']*p['scalefactor']*Vp[cid]
            Vv[cid]=p['vac_percentage']*p['scalefactor']*p['scalefactor']*Vp[cid]
            Sv[cid]=0
            for a in cell_to_walls[cid]:
                Sv[cid] += pow(p['vac_percentage'],2/3)*p['scalefactor']*Sp[a]

        for wid in mesh.wisps(1):
	        S[wid]=p['scalefactor']*Sp[wid]

        GA=db.get_property('GA')
        GA_wall=db.get_property('GA_wall')
        GA_vac=db.get_property('GA_vac')
        


        Expvac = db.get_property("Expvac")
        Inf1 = db.get_property("Inf1")
        Inf2 = db.get_property("Inf2")
        plasmod=db.get_property("plasmodesmata")
        cell_type=db.get_property('cell_type')

            

        cell_map = tissue_maps['cell']
        wall_map = tissue_maps['wall']
        point_map = tissue_maps['point']
        wall_decomposition=get_wall_decomp(db)
        
        
        c_off = len(list(cell_map.keys()))
        w_off = len(list(wall_map.keys()))
        v_off = len(list(point_map.keys()))

        if p['D_cw']>0.0:
            N = 2*c_off+w_off+v_off
        else:
            N = 2*c_off+w_off
            
        J = lil_matrix((N,N))
        r = zeros((N,))
        
        #set fluxes  
          
        for wid in self.inner_walls:
            widx = ssl['GA_wall'].start + wall_map[wid]

            eid1 = wall_decomposition[wid][0]
            eid2 = wall_decomposition[wid][1]
            sid = graph.source(eid1)
            tid = graph.target(eid1)
            sidx = ssl['GA'].start+cell_map[sid]
            tidx = ssl['GA'].start+cell_map[tid]
            
            #carrier mediated transport and diffusion

            
            Jsw = B1PI+B2PI1*Inf1[eid1]+B2PI2*Inf2[eid1]+ B3Pb
            Jtw = B1PI+B2PI1*Inf1[eid2]+B2PI2*Inf2[eid2]+ B3Pb
            Jws = A1PI+A2PI1*Inf1[eid1]+A2PI2*Inf2[eid1]+ A3Pb
            Jwt = A1PI+A2PI1*Inf1[eid2]+A2PI2*Inf2[eid2]+ A3Pb
            
            J[sidx, sidx] += -(S[wid]/V[sid])*Jsw
            J[sidx, widx] += (S[wid]/V[sid])*Jws
            J[widx, sidx] += (1.0/wall_width[wid])*Jsw
            J[widx, widx] += -(1.0/wall_width[wid])*Jws

            J[tidx, tidx] += -(S[wid]/V[tid])*Jtw
            J[tidx, widx] += (S[wid]/V[tid])*Jwt
            J[widx, tidx] += (1.0/wall_width[wid])*Jtw
            J[widx, widx] += -(1.0/wall_width[wid])*Jwt
            
            #inter-cellular diffusion via Plasmodesmata
            Jpla = Ppla*plasmod[wid]

            J[sidx, sidx] += -(S[wid]/V[sid])*Jpla
            J[sidx, tidx] += (S[wid]/V[sid])*Jpla
            J[tidx, sidx] += (S[wid]/V[tid])*Jpla
            J[tidx, tidx] += -(S[wid]/V[tid])*Jpla
            
        for wid,cid in self.outer_walls.items():
        
            widx = ssl['GA_wall'].start + wall_map[wid]
            sidx = ssl['GA'].start + cell_map[cid]
            
            #carrier mediated transport and diffusion
            Jsw= B1PI#+B2PI1+B3Pb
            Jws= A1PI#+A2PI1+A3Pb
            
            J[sidx, sidx] += -(S[wid]/V[cid])*Jsw
            J[sidx, widx] += (S[wid]/V[cid])*Jws
            J[widx, sidx] += (1.0/wall_width[wid])*Jsw
            J[widx, widx] += -(1.0/wall_width[wid])*Jws
        
        #production / degradation
        for cid in mesh.wisps(mesh.degree()):
            cidx = ssl['GA'].start + cell_map[cid]
            #J[cidx, cidx] -= p['dGA']#/V[cid]
            r[cidx] += pGA[cid]#/V[cid]
            cidv=ssl['GA_vac'].start+cell_map[cid]
            J[cidx,cidx]+=-p['dGA']-Sv[cid]/V[cid] * (B1PI+B3PEv*Expvac[cid]+B3Piv*Expvac[cid])#/V[cid]
            J[cidx,cidv]+=Sv[cid]/V[cid] * (C1PI+C3PEv*Expvac[cid]+C3Piv*Expvac[cid])
            J[cidv,cidv]+=-Sv[cid]/Vv[cid] * (C1PI+C3PEv*Expvac[cid]+C3Piv*Expvac[cid])
            J[cidv,cidx]+=Sv[cid]/Vv[cid] * (B1PI+B3PEv*Expvac[cid]+B3Piv*Expvac[cid])
        
        # prescribe fluxes between adjoining cell wall compartments
        if p['D_cw']>0.0:
            for pid,wids in self.points_to_walls.items():
                pidx=2*c_off+w_off+point_map[pid]
                for wid in wids:
                    widx=ssl['GA_wall'].start+wall_map[wid]
                    eid1 = wall_decomposition[wid][0]
                    eid2 = wall_decomposition[wid][1]
                    sid = graph.source(eid1)
                    tid = graph.target(eid1)
                    if (cell_type[sid]==3 or cell_type[tid]==3):
                        J[pidx,pidx] += -0.001*(1.0/wall_width[wid])*(2*p['D_cw']/S[wid])
                        J[pidx,widx] += 0.001*(1.0/wall_width[wid])*(2*p['D_cw']/S[wid])
                        J[widx,widx] += -0.001*(1.0/S[wid])*(2*p['D_cw']/S[wid])
                        J[widx,pidx] += 0.001*(1.0/S[wid])*(2*p['D_cw']/S[wid])
                    else:
                        J[pidx,pidx] += -(1.0/wall_width[wid])*(2*p['D_cw']/S[wid])
                        J[pidx,widx] += (1.0/wall_width[wid])*(2*p['D_cw']/S[wid])
                        J[widx,widx] += -(1.0/S[wid])*(2*p['D_cw']/S[wid])
                        J[widx,pidx] += (1.0/S[wid])*(2*p['D_cw']/S[wid])


        # fixed properties    
        reverse_map={}
        reverse_ids={}
        for cid,val in GA.items():
            reverse_map[ssl['GA'].start+cell_map[cid]]=val
            reverse_ids[ssl['GA'].start+cell_map[cid]]=cid
        for wid,val in GA_wall.items():
            reverse_map[ssl['GA_wall'].start+wall_map[wid]]=val
            reverse_ids[ssl['GA_wall'].start+wall_map[wid]]=wid
        for cid,val in GA_vac.items():
            reverse_map[ssl['GA_vac'].start+cell_map[cid]]=val
            reverse_ids[ssl['GA_vac'].start+cell_map[cid]]=cid
            
        
        for i in fixed_idx:
            J[i, :] *=0
            J[i, i] = 1
            r[i]=0
            
            #r[i] = -reverse_map[i] ### only needed if section below is commented out
            
            ########## Possibly delete here to not have the graident to zero in top row of cells
            
            if cell_type[reverse_ids[i]] in [2,3,4,16]:
                ncids=list(mesh.border_neighbors(2,reverse_ids[i]))
                found=False
                for xcid in ncids:
                    if cell_type[reverse_ids[i]]==cell_type[xcid]:
                        nid=xcid
                        found=True
                if found==False:
                    print('!!!boundary neighbour not found!!!')

                J[i, ssl['GA'].start+cell_map[nid]] = -1
            else:
                r[i] = -reverse_map[i]
                
			###########
			
        #fixed outer boundary
        if p['outer_concentration']>0.0:
            for wid in self.outer_walls.keys():
                widx = ssl['GA_wall'].start + wall_map[wid]
                J[widx, :] *=0
                J[widx,widx]=1
                r[widx]=-p['outer_concentration']
        
        #solve
        y = -spsolve(J.tocsr(), r)

        #assign values
        for cid in GA.keys():
            GA[cid]=y[ssl['GA'].start+cell_map[cid]]        

        for wid in GA_wall.keys():
            GA_wall[wid]=y[ssl['GA_wall'].start + wall_map[wid]]
        
        for cid in GA_vac.keys():
            GA_vac[cid]=y[ssl['GA_vac'].start+cell_map[cid]]
            
        #print(GA)
        #print(GA_vac)
        #print(GA_wall)
            
        GA_point=db.get_property('GA_point')
        if p['D_cw']>0.0:
            for pid in GA_point.keys():
                GA_point[pid]=y[2*c_off+w_off + point_map[pid]]

        #assign fluxes for output
        plas_flux=db.get_property('plas_flux')
        carrier_flux=db.get_property('carrier_flux')
        combined_flux=db.get_property('combined_flux')
        Inf1Inf2_flux=db.get_property('Inf1Inf2_flux')
        Expvac_flux=db.get_property('Expvac_flux')
        passive_flux=db.get_property('passive_flux')
        for wid in self.inner_walls:
            
            eid1 = wall_decomposition[wid][0]
            if graph.source(eid1)>graph.target(eid1):
                eid2 = eid1
                eid1 = wall_decomposition[wid][1]
            else:
                eid2 = wall_decomposition[wid][1]
            

            
            sid = graph.source(eid1)
            tid = graph.target(eid1)

            Jpla = Ppla*plasmod[wid]
            J1PLA = Ppla*plasmod[wid]*(GA[sid]-GA[tid]) 
            plas_flux[wid]=J1PLA*S[wid]
            
            J1 = (B1PI+B2PI2*Inf1[eid1]+B2PI2*Inf2[eid1]+ B3Pb)*GA[sid]-(A1PI+A2PI1*Inf1[eid1]+A2PI2*Inf2[eid1]+ A3Pb)*GA_wall[wid]
            J2 = (B1PI+B2PI2*Inf1[eid2]+B2PI2*Inf2[eid2]+ B3Pb)*GA[tid]-(A1PI+A2PI1*Inf1[eid2]+A2PI2*Inf2[eid2]+ A3Pb)*GA_wall[wid]
            carrier_flux[wid] = (J1-J2)/2*S[wid]

            J1 = (B2PI1*Inf1[eid1]+B2PI2*Inf2[eid1])*GA[sid]-(A2PI1*Inf1[eid1]+A2PI2*Inf2[eid1])*GA_wall[wid]
            J2 = (B2PI1*Inf1[eid2]+B2PI2*Inf2[eid2])*GA[tid]-(A2PI1*Inf1[eid2]+A2PI2*Inf2[eid2])*GA_wall[wid]
            Inf1Inf2_flux[wid] = (J1-J2)/2*S[wid]

            J1 = 0
            J2 = 0
            Expvac_flux[wid] = (J1-J2)/2*S[wid]
            
            J1 = (B1PI)*GA[sid]-(B1PI)*GA_wall[wid]
            J2 = (B1PI)*GA[tid]-(B1PI)*GA_wall[wid]
            passive_flux[wid] = (J1-J2)/2*S[wid]
            
            combined_flux[wid]=carrier_flux[wid]+plas_flux[wid]