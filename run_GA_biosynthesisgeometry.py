#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  3 18:10:47 2022

@author: user
"""

# -*- coding: utf-8 -*-
"""
Created on Sat Jul 10 21:36:29 2021

@author: Kristian Kiradjiev
"""
from vroot.import_tissue import import_xml, import_tissue
from models.set_carriers_wt_GAgeometry import set_carriers
from vroot.simulationgeometry import Simulation
from models.GAtransport_ssgeometry1 import GAtransport_ss
from models.GAtransportgeometry1 import GAtransport
from vroot.db_utilities import get_mesh, set_cell_dependent_parameter, get_graph, set_parameters, get_parameters
from vroot.pylab_plotgeometry import plotCellIds, plot_pylab_figure, plot_relative_property, plot_data_vs_model, plot_celltypes_by_category
from vroot.plot_dynamicgeometry import Plot_Dynamic
from numpy import mean, arange, dot
from numpy.linalg import norm
from matplotlib import pyplot as plt

def fitFunction(datavals,modelvals):
    dmin=min(list(datavals.values()))
    mmin=min(list(modelvals.values()))
    
    nselected=len([val for cid,val in modelvals.items()])
    return sum([abs((val/mmin)-(datavals[cid]/dmin)) for cid,val in modelvals.items()])/nselected
	
fit = []

### Import tissue database from .xml file and set up simulation

#Change the name of the files when runing simulations for different mutants

sim=Simulation('Tissues/crosssection_withcelltypes_nodespacing20.zip', 'GAVacuole09NoPlasAllTransportersDiffusionSyn0001Deg0000003DynamicPaper5days', biochemical=GAtransport) #reads in the tissue database, gives a folder name for the this initialisation, specifies that we will be using the GAtransport model

db = sim.get_db()
db = set_carriers(db)

### Set-up simulation parameters and knockouts

# set specific GA production rates
#set_cell_dependent_parameter(sim.get_db(),'pGA',0.000,[('cell_type[cid] in [1,2]',0.00),('cid in border.keys() and cell_type[cid] in [1,2]',0.00)])

#sim.set_param_values({'Pplas':0, 'Pback':0, 'PExpvac':0, 'PInf2':0, 'PInf1':0, 'PIAAH':0}) 

# set-up knockouts (excluding Exporter knockouts)
'''
Inf1=db.get_property('Inf1') 
for eid in Inf1.keys(): 
	Inf1[eid]=0 
'''
# set-up Exporter knockouts
'''
Expvac=db.get_property('Expvac')
Expvac2=db.get_property('Expvac2')

for eid in Expvac.keys():
	Expvac[eid]-=Expvac2[eid]
	
	if Expvac2[eid] > 0:
		Expvac2[eid]=0
'''
dt = 0.01
sim.set_timestep(dt)
sim.run_simulation(130)
db=sim.get_db()
db.write('Tissues/Geometry_steady1.zip')

###This is needed to create the dynamic plots when GA synthesis is switched off

tissuename = 'Tissues/Geometry_steady1.zip'
sim=Simulation(tissuename,'GAVac09NoPlasAllTransportersDiffusionSyn0001Deg0000003Paper5daysSynthesis0',biochemical=GAtransport)
sim.configure_output(zips=False,vtus=False,steps=12,timecourses=['GA','GA_vac'])
sim.set_param_values({'pGA':('cell_type',{5:0,'else':0})})

sim.reset_time()

### Run simulation

# set-up vacuolar volume fraction, if it is 0, there is no vacuole
#sim.set_param_values({'vac_percentage':0})
dt = 0.1#
sim.set_timestep(dt)
sim.run_simulation(42)
N=42
#N=sim.run_to_steady_state({'GA','GA_vac','GA_wall'},0.0002)
db = sim.get_db()

###


#Introduce scale-factor for time that scales with the inverse of the degradation rate just for convenience of simulation
T=1/0.000003
C=1
#print (db.get_property('pGA')) 

### Create output figures

#plot some figures

sim.plot_figure('GA.png',{'GA':[0, sim.get_95pc('GA')]},[1,1],True)
sim.plot_figure('GA_wall.png',{'GA_wall':[0, sim.get_95pc('GA_wall')]},[1,1],True)
sim.plot_figure('GA_vac.png',{'GA_vac':[0, sim.get_95pc('GA_vac')]},[1,1],True)
Plot_Dynamic(db,N,dt,'%s' % (sim.dir_name),T,C)
plot_celltypes_by_category(db,'%s/%s' % (sim.dir_name,'CellTypes.png'),rotate=False)
plotCellIds(db,'%s/%s' % (sim.dir_name,'CellIds.png'),rotate=False)

#To plot the dynamic figures, create a folder Plots in the GAtransport folder within the Geometry_steady1 folder, run PlotEndodermisSynthesisPaper.py, and then run Plot_Dynamic1(db,N,dt,T,C)



# C:\Users\sbznm\AppData\Local\Continuum\anaconda3\lib\site-packages\scipy\stats\s
#tats.py:1713: FutureWarning: Using a non-tuple sequence for multidimensional ind
#exing is deprecated; use `arr[tuple(seq)]` instead of `arr[seq]`. In the future
#this will be interpreted as an array index, `arr[np.array(seq)]`, which will res
#ult either in an error or a different result.
#  return np.add.reduce(sorted[indexer] * weights, axis=axis) / sumval

